FROM aantonw/alpine-wkhtmltopdf-patched-qt:latest as wkhtmltopdf
FROM alpine:3.9

RUN apk --update --no-cache add \
    libgcc \
    libstdc++ \
    musl \
    qt5-qtbase \
    qt5-qtbase-x11 \
    qt5-qtsvg \
    qt5-qtwebkit \
    ttf-freefont \
    ttf-dejavu \
    ttf-droid \
    ttf-liberation \
    ttf-ubuntu-font-family \
    fontconfig

### install unpatched wkhtmltopdf
RUN apk add --update wkhtmltopdf

# Add openssl dependencies for wkhtmltopdf
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.8/main' >> /etc/apk/repositories && \
    apk add --no-cache libcrypto1.0 libssl1.0

# Add wkhtmltopdf
COPY --from=wkhtmltopdf /bin/wkhtmltopdf /bin/wkhtmltopdf


COPY --from=golang:1.21.4-alpine3.18 /usr/local/go/ /usr/local/go/
 
ENV PATH="/usr/local/go/bin:${PATH}"


WORKDIR /app 

# COPY /src/go.mod ./
# COPY /src/go.sum ./
COPY /src /app/src
#COPY /config /app/config
COPY local.env /app/src

WORKDIR /app/src 

RUN  go mod download

RUN  go build -o main .

EXPOSE 8080

CMD ["./main"]
