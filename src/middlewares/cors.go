package middlewares

import (
	"BE-Epson/configs/app"
	"BE-Epson/shared/helpers"
	"net/http"

	"github.com/gin-gonic/gin"
)

//func LangMiddleware() gin.HandlerFunc {
//	return func(c *gin.Context) {
//		if c.Request.Header.Get("lang") == "" {
//			c.Request.Header.Set("lang", app.GetConfig().App.Locale)
//		}
//		c.Set("lang", c.GetHeader("lang"))
//		utils.OverrideGinRequest(c, "lang", c.GetHeader("lang"))
//		c.Next()
//	}
//}

func NewCors(ctx *gin.Engine) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		ctx.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		ctx.Writer.Header().Set("Access-Control-Allow-Methods", "GET,OPTIONS,POST,PUT,DELETE")
		ctx.Writer.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Access-Control-Allow-Origin , Origin,Accept, X-Requested-With, Content-Type, Authorization, Access-Control-Request-Method, Access-Control-Request-Headers, X-CSRF-Token")
		ctx.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type, Origin")

		// Handle preflight request
		if ctx.Request.Method == "OPTIONS" {
			ctx.Writer.WriteHeader(204)
			return
		}

		verifyToken := helpers.ExtractToken(ctx)
		if verifyToken == "" {
			ctx.AbortWithStatusJSON(
				http.StatusForbidden,
				gin.H{
					"Error": "Token Is Not Valid !",
				},
			)
			return
		}
		client := app.GetConfig().Keycloak.GoCloakObj
		rptResult, err := client.RetrospectToken(ctx, verifyToken, app.GetConfig().Keycloak.ClientId, app.GetConfig().Keycloak.ClientSecret, app.GetConfig().Keycloak.Realm)
		if err != nil {
			ctx.AbortWithStatusJSON(
				http.StatusForbidden,
				gin.H{
					"Error":   err.Error(),
					"message": "Token Is Not Valid ! ",
				},
			)
			return
		}
		if !*rptResult.Active {
			ctx.AbortWithStatusJSON(
				http.StatusUnauthorized,
				gin.H{
					"Error":   "Rpt Result Token Is Not Active !",
					"Message": "Token Is Not Active ! ",
				},
			)
			return
		}
		ctx.Next()
	}
}
