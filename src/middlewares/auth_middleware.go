package middlewares

import (
	"BE-Epson/configs/app"
	"BE-Epson/domains/staging/interfaces"
	UserManagement "BE-Epson/domains/user/entities"
	"BE-Epson/shared/helpers"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func VerifyUserKeycloak(stagingUserRepo interfaces.StagingUserRepository) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var user_email string
		ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		ctx.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		ctx.Writer.Header().Set("Access-Control-Allow-Methods", "GET,OPTIONS,POST,PUT,DELETE")
		ctx.Writer.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Authorization, Access-Control-Request-Method, Access-Control-Request-Headers , X-CSRF-Token")
		ctx.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type, Origin")
		// Handle preflight request
		if ctx.Request.Method == "OPTIONS" {
			ctx.Writer.WriteHeader(204)
			return
		}

		verifyToken := helpers.ExtractToken(ctx)
		if verifyToken == "" {
			ctx.AbortWithStatusJSON(
				http.StatusUnauthorized,
				gin.H{
					"Error": "Token Is Not Valid !",
				},
			)
			return
		}
		client := app.GetConfig().Keycloak.GoCloakObj
		rptResult, err := client.RetrospectToken(ctx, verifyToken, app.GetConfig().Keycloak.ClientId, app.GetConfig().Keycloak.ClientSecret, app.GetConfig().Keycloak.Realm)
		if err != nil {
			ctx.AbortWithStatusJSON(
				http.StatusUnauthorized,
				gin.H{
					"Error":   err.Error(),
					"message": "Token Is Not Valid ! ",
				},
			)
			return
		}
		if !*rptResult.Active {
			ctx.AbortWithStatusJSON(
				http.StatusUnauthorized,
				gin.H{
					"Error":   "Rpt Result Token Is Not Active !",
					"Message": "Token Is Not Active ! ",
				},
			)
			return
		}
		jwt, err2 := client.GetRawUserInfo(ctx, verifyToken, app.GetConfig().Keycloak.Realm)
		if err2 != nil {
			ctx.AbortWithStatusJSON(
				http.StatusUnauthorized,
				gin.H{
					"Error":   err.Error(),
					"message": "Token Is Not Valid ! ",
				},
			)
			return
		}

		if jwt["employee_id"] != "" {
			jwt["nik"] = jwt["employee_id"]
		} else {
			jwt["nik"] = "22030001"
		}

		if jwt["given_name"] == "Reagent" {
			jwt["nik"] = "10903794" //user
		}

		if jwt["email"] == nil {
			user_email = "miisaladapprover@gmail.com"
		} else {
			user_email = jwt["email"].(string)
		}

		data, err := stagingUserRepo.FindUserByNik(jwt["nik"].(string))
		if err == nil {
			if data.Email != nil {
				if *data.Email != user_email {
					payload := &UserManagement.User{
						Email: helpers.StringPointer(user_email),
					}
					_, err = stagingUserRepo.UpdateUser(payload, jwt["nik"].(string))
					if err != nil {
						fmt.Println("Error Update User : ", err.Error())
					}
				}
			}
		} else {
			payload := &UserManagement.User{
				Name:  jwt["given_name"].(string),
				Nik:   jwt["nik"].(string),
				Email: helpers.StringPointer(user_email),
			}
			_, err = stagingUserRepo.InsertUser(payload)
			if err != nil {
				fmt.Println("Error Create User : ", err.Error())
			}
		}

		ctx.Set("UserData", helpers.CopyValueToken(jwt))

		ctx.Next()
	}
}

func GetUserInfo() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		verifyToken := helpers.ExtractToken(ctx)
		client := app.GetConfig().Keycloak.GoCloakObj
		jwt, err2 := client.GetRawUserInfo(ctx, verifyToken, app.GetConfig().Keycloak.Realm)
		if err2 != nil {
			ctx.AbortWithStatusJSON(
				http.StatusUnauthorized,
				gin.H{
					"Error":   err2.Error(),
					"message": "Token Is Not Valid ! ",
				},
			)
			return
		}
		jwt["name"] = "Dap"

		if jwt["employee_id"] != "" {
			jwt["nik"] = jwt["employee_id"]
		} else {
			jwt["nik"] = "22030001"
		}

		if jwt["given_name"] == "Reagent" {
			jwt["nik"] = "10903794" //user
		} else {
			jwt["name"] = jwt["given_name"]
		}
		ctx.Set("UserData", helpers.CopyValueToken(jwt))
		ctx.Next()
	}
}

func MiddlewareKosong() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		ctx.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		ctx.Writer.Header().Set("Access-Control-Allow-Methods", "GET,OPTIONS,POST,PUT,DELETE")
		ctx.Writer.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Authorization, Access-Control-Request-Method, Access-Control-Request-Headers , X-CSRF-Token")
		ctx.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type, Origin")

		// Handle preflight request
		if ctx.Request.Method == "OPTIONS" {
			ctx.Writer.WriteHeader(204)
			return
		}

		jwt := make(map[string]interface{})
		jwt["name"] = "Dap"
		jwt["nik"] = "22030001"
		ctx.Set("UserData", helpers.CopyValueToken(jwt))
		ctx.Next()
	}
}

func HandleCors() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		ctx.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		ctx.Writer.Header().Set("Access-Control-Allow-Methods", "GET,OPTIONS,POST,PUT,DELETE")
		ctx.Writer.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Authorization, Access-Control-Request-Method, Access-Control-Request-Headers , X-CSRF-Token")
		ctx.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type, Origin")
		// Handle preflight request
		if ctx.Request.Method == "OPTIONS" {
			ctx.Writer.WriteHeader(204)
			return
		}
		ctx.Next()
	}
}
