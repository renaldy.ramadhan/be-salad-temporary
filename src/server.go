package main

import (
	appConfig "BE-Epson/configs/app"
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	"BE-Epson/domains/event/handlers"
	repository2 "BE-Epson/domains/event/interfaces/impl/repository"
	Master "BE-Epson/domains/master/handlers/https"
	"BE-Epson/domains/part/handlers/https"
	"BE-Epson/domains/part/interfaces/impl/repository"
	"BE-Epson/domains/part/presistence"
	Staging "BE-Epson/domains/staging/handlers/http"
	"BE-Epson/domains/staging/interfaces"
	repository3 "BE-Epson/domains/staging/interfaces/impl/repository"
	"BE-Epson/middlewares"
	"log"
	"net/http"

	"github.com/robfig/cron/v3"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func Migrate(db *gorm.DB) {
	err := database.Migrate(db)
	if err != nil {
		panic("Error migrating database : " + err.Error())
	}
}

func RegisterTrustedProxies(router *gin.Engine) {
	appConfig.TrustedProxies(router)
	return
}

type AppRepostiory struct {
	stagingUserRepo interfaces.StagingUserRepository
}

func NewAppRepository(stagingUserRepo interfaces.StagingUserRepository) *AppRepostiory {
	return &AppRepostiory{stagingUserRepo}
}

func RegisterAppRepo() *AppRepostiory {
	handler := &AppRepostiory{
		stagingUserRepo: repository3.NewStagingUserRepository(database.ConnectDatabase(Const.DB_SALAD)),
	}
	return handler
}

func RegisterMiddlewares(router *gin.Engine) {
	//repo := RegisterAppRepo()
	router.Use(middlewares.HandleCors())
	//router.Use(middlewares.NewCors(router))
	//router.Use(middlewares.MiddlewareKosong())

	//router.Use(middlewares.VerifyUserKeycloak(repo.stagingUserRepo))
	// failed Upadate
}

func SetupCronJobs() {
	c := cron.New(cron.WithChain(
		cron.Recover(cron.DefaultLogger), // or use cron.DefaultLogger
	))
	jobNames := appConfig.GetConfig().CronJob.CronJobName
	jobIntervals := appConfig.GetConfig().CronJob.CronJobInterval
	if len(jobNames) != len(jobIntervals) {
		log.Fatal("Cron job configuration error: job names and intervals length mismatch")
	}
	column := appConfig.GetConfig().CronJob.CronJobTargetColumn
	for i, jobName := range jobNames {
		interval := jobIntervals[i]
		task := appConfig.GetConfig().CronJob.CronJobTask[i]
		_, err := c.AddFunc(interval, createJobFunction(column, jobName, interval, task))
		if err != nil {
			log.Printf("Error scheduling Cron Job '%s': %s\n", jobName, err)
		} else {
			log.Printf("Scheduled Cron Job: %s with interval %s\n", jobName, interval)
		}
	}
	c.Start()
}

func createJobFunction(column []string, jobName, interval, task string) func() {
	return func() {
		repo := repository.NewSequenceRepository(database.ConnectDatabase(Const.DB_SALAD))
		for _, value := range column {
			if err := repo.DoResetSequence(presistence.RequestType(value)); err != nil {
				log.Printf("Error running Cron Job '%s': %s\n", jobName, err)
			}
		}
		disposalRepo := repository2.NewDisposalGroupRepository(database.ConnectDatabase(Const.DB_SALAD))
		if err := disposalRepo.DoUpdateStatus(); err != nil {
			log.Printf("Error running Cron Job '%s': %s\n", jobName, err)
		}
		log.Printf("Run Cron Job: %s\n at interval %s\n Target:%s\n", jobName, interval, task)
	}
}

func RegisterFERoute(router *gin.Engine) {
	//init variables
	publicUrl := "/salad"

	//load build fe files
	router.LoadHTMLGlob("./build/*.html")
	router.Static(publicUrl+"/static", "./build/static")
	router.Static(publicUrl+"/images", "./build/images")
	router.StaticFile(publicUrl+"/manifest.json", "./build/manifest.json")
	router.StaticFile(publicUrl+"/favicon.ico", "./build/favicon.ico")
	router.StaticFile(publicUrl+"/robots.txt", "./build/robots.txt")
	router.StaticFile(publicUrl+"/apple-touch-icon.png", "./build/apple-touch-icon.png")
	router.GET("/salad/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})
	router.NoRoute(func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})
}

func RegisterRoutes(router *gin.Engine) {
	appRepo := RegisterAppRepo()
	router.GET("/salad/api", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Hello Backend !",
		})
	})

	Staging.NewStagingUserHttp(router, appRepo.stagingUserRepo)
	OrganizationApprovalRouteHttp := Staging.NewOrganizationApprovalRouteHttp(router, appRepo.stagingUserRepo)
	Staging.NewStagingPlantHttp(router, appRepo.stagingUserRepo)
	Master.NewDepartHttp(router, appRepo.stagingUserRepo)
	Staging.NewStagingReasonHttp(router, appRepo.stagingUserRepo)
	Staging.NewStagingPurchasingGroupHttp(router, appRepo.stagingUserRepo)
	Staging.NewStagingItemHttp(router, appRepo.stagingUserRepo)
	WorkFlow := Master.NewWorkflowHttps(router, appRepo.stagingUserRepo)
	FlowStep := Master.NewFlowStepHttp(router, appRepo.stagingUserRepo)
	https.NewItemPivotHttp(router, appRepo.stagingUserRepo)
	DocumentDetailHttp := https.NewDocumentDetailHttp(router, appRepo.stagingUserRepo)
	Master.NewApproveGroupHttp(router, appRepo.stagingUserRepo)
	ApproverPivot := Master.NewApproverPivotHttp(router, appRepo.stagingUserRepo)
	Master.NewApproverHttp(router, OrganizationApprovalRouteHttp, ApproverPivot, appRepo.stagingUserRepo)
	ActiveRequestList := https.NewActiveRequestApprovalListHttp(router, appRepo.stagingUserRepo)
	https.NewRequestHttp(router, WorkFlow, FlowStep, OrganizationApprovalRouteHttp, ActiveRequestList, ApproverPivot, DocumentDetailHttp, appRepo.stagingUserRepo)
	https.NewDocumentPrintHttp(router, appRepo.stagingUserRepo)
	https.NewRequestApprovalHistoryHttp(router, appRepo.stagingUserRepo)
	handlers.NewActiveEventApproverListHttp(router, appRepo.stagingUserRepo)
	handlers.NewDisposalGroupHttp(router, appRepo.stagingUserRepo)
}
