package helpers

import (
	"BE-Epson/configs/app"
	"BE-Epson/domains/part/presistence"
	"BE-Epson/shared/models/messages"
	"github.com/wneessen/go-mail"
	"log"
	"strings"
)

func SendMail(payload *messages.MailPayload) {
	nextApprovalTemplate := "<!DOCTYPE html>\n<html lang=\"en\" >\n\n<head>\n\t<meta charset=\"UTF-8\">\n\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\t<title>Email</title>\n</head>\n\n<body style=\"font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; margin: 0;\">\n\t<div>\n\t\t<table style=\"width: 100%; background-color: #EEF0F8; padding: 0; border-spacing: 0; \">\n\t\t\t<tr>\n\t\t\t\t<td style=\"padding: 0\">\n\t\t\t\t\t<table id=\"header\"\n\t\t\t\t\t\tstyle=\"width: 600px; background-color: white; padding: 0px 30px; border-top: 5px solid #005395; border-spacing: 0; margin: auto;\">\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<th colspan=\"2\" style=\"text-align: left; padding-left: 0; padding-right: 0;\">\n\t\t\t\t\t\t\t\t<h1 style=\"margin-bottom: 5px;\">Attention!</h1>\n\t\t\t\t\t\t\t</th>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td colspan=\"2\" style=\"padding: 0px 0px 30px 0px;\">SALAD Approval Notification</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td colspan=\"2\" style=\"padding: 0px 0px 10px 0px;\">Dear&nbsp;<span>[APPROVER_NAME]<span>,</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td colspan=\"2\" style=\"padding: 0px 0px 20px 0px;\">There are request [SUBJECT_TYPE] : </td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</table>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t\t<td style=\"padding: 0;\">\n\t\t\t\t\t<table\n\t\t\t\t\t\tstyle=\"width: 600px; background-color: white; padding: 0px 30px; border-spacing: 0; margin: auto;\">\n\t\t\t\t\t\t<tbody id=\"dualcolumn\">\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td\n\t\t\t\t\t\t\t\t\tstyle=\"width: 50%; color: #615a5a; padding: 15px 0px 15px 0px; border-bottom: 1px solid #EEF0F8;\">\n\t\t\t\t\t\t\t\t\tDocument No</td>\n\t\t\t\t\t\t\t\t<td style=\"width: 50%; color: #615a5a; padding: 15px 0px 15px 0px; border-bottom: 1px solid #EEF0F8;\">[REQUEST_NUMBER]</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td\n\t\t\t\t\t\t\t\t\tstyle=\"width: 50%; color: #615a5a; padding: 15px 0px 15px 0px; border-bottom: 1px solid #EEF0F8;\">\n\t\t\t\t\t\t\t\t\tRequest Type</td>\n\t\t\t\t\t\t\t\t<td style=\"width: 50%; color: #615a5a; padding: 15px 0px 15px 0px; border-bottom: 1px solid #EEF0F8;\">[REQUEST_TYPE]</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td\n\t\t\t\t\t\t\t\t\tstyle=\"width: 50%; color: #615a5a; padding: 15px 0px 15px 0px; border-bottom: 1px solid #EEF0F8;\">\n\t\t\t\t\t\t\t\t\tRequestor Name</td>\n\t\t\t\t\t\t\t\t<td style=\"width: 50%; color: #615a5a; padding: 15px 0px 15px 0px; border-bottom: 1px solid #EEF0F8;\">[REQUESTOR_NAME]</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td colspan=\"2\" style=\"padding: 30px 0px 15px 0px;\">\n\t\t\t\t\t\t\t\t\tPlease login to SALAD application to take further action. Thank you for your attention.\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t<tfoot>\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td colspan=\"2\" style=\"color: #615a5a; padding: 30px 0px 0px 0px;\">\n\t\t\t\t\t\t\t\t\tIf you don't recognize this activity, please contact our help desk\n\t\t\t\t\t\t\t\t\timmediately\n\t\t\t\t\t\t\t\t\tat:\n\t\t\t\t\t\t\t\t\t<a style=\"color: #005395; text-decoration: none;\"\n\t\t\t\t\t\t\t\t\t\thref=\"mailto:mii@metrodata.co.id\">mii@metrodata.co.id</a>\n\t\t\t\t\t\t\t\t\tand cc to <a style=\"color: #005395; text-decoration: none;\"\n\t\t\t\t\t\t\t\t\t\thref=\"mailto:mii@metrodata.co.id\">mii@metrodata.co.id</a>\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td colspan=\"2\" style=\"color: #615a5a; padding: 50px 0px 30px 0px;\">\n\t\t\t\t\t\t\t\t\tThis is an automated message by SALAD system, please do not reply this email.\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t</tfoot>\n\t\t\t\t\t</table>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t\t<td style=\"padding: 0;\">\n\t\t\t\t\t<table id=\"footer\"\n\t\t\t\t\t\tstyle=\"width: 600px; background-color: #005395; padding: 0px 30px; border-spacing: 0; margin: auto;\">\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td colspan=\"2\" style=\"text-align: center; color: white; padding: 30px 0px 10px 0px;\">\n\t\t\t\t\t\t\t\t<img style=\"height: 50px;\"\n\t\t\t\t\t\t\t\t\tsrc=\"/media/logos/logo-kominfo-bg-white.png\" alt=\"\" />\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td colspan=\"2\" style=\"text-align: center; color: white; padding: 0px 0px 30px 0px;\">\n\t\t\t\t\t\t\t\t2024© PT EPSON INDONESIA\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</table>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</table>\n\t</div>\n</body>\n\n</html>\n"
	m := mail.NewMsg()
	fromMail := app.GetConfig().Email.Address

	log.Printf("sending email to: %s\n", payload.ToAddress)

	if err := m.From(fromMail); err != nil {
		log.Fatalf("failed to set From address: %s", err)
	}

	if err := m.To(payload.ToAddress); err != nil {
		log.Fatalf("failed to set To address: %s", err)
	}
	result := strings.Replace(nextApprovalTemplate, "[APPROVER_NAME]", *payload.ApproverName, -1)
	result = strings.Replace(result, "[REQUEST_NUMBER]", *payload.DocumentNumber, -1)
	result = strings.Replace(result, "[REQUEST_TYPE]", string(payload.RequestType), -1)
	result = strings.Replace(result, "[REQUESTOR_NAME]", *payload.RequestorName, -1)

	if payload.MailType == presistence.NeedApproval {
		result = strings.Replace(result, "[SUBJECT_TYPE]", presistence.MessageNeedApproval, -1)
	} else {
		result = strings.Replace(result, "[SUBJECT_TYPE]", presistence.MessageFinishApproval, -1)
	}

	nextApprovalTemplate = result
	m.Subject("This is my first mail with go-mail!")
	m.SetBodyString(mail.TypeTextHTML, nextApprovalTemplate)
	c, err := mail.NewClient(app.GetConfig().Email.Host, mail.WithPort(app.GetConfig().Email.Port), mail.WithSMTPAuth(mail.SMTPAuthPlain),
		mail.WithUsername(fromMail), mail.WithPassword(app.GetConfig().Email.Password))
	if err != nil {
		log.Fatalf("failed to create mail client: %s", err)
	}
	if err := c.DialAndSend(m); err != nil {
		log.Fatalf("failed to send mail: %s", err)
	}
}
