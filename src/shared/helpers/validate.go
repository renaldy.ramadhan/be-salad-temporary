package helpers

import (
	Sequence "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/interfaces"
	"BE-Epson/domains/part/presistence"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/models/requests/Part"
	"errors"
	"fmt"
	"strings"
	"time"
)

func CheckDisposalGroupType(Req *Part.CreateRequest) (bool, error) {
	if Req.DisposalType == "" {
		return false, errors.New(Const.DISPOSAL_TYPE_EMPTY)
	}
	if Req.DisposalType != "System" && Req.DisposalType != "Non - System" {
		return false, errors.New(Const.DISPOSAL_TYPE_INVALID)
	}
	if Req.IdDisposalGroupEvent == nil {
		return false, errors.New(Const.ID_DISPOSAL_GROUP_EVENT_EMPTY)
	}
	if *Req.IdDisposalGroupEvent < 1 {
		return false, errors.New(Const.ID_DISPOSAL_GROUP_EVENT_INVALID)
	}
	if Req.DisposalType == "Non - System" {
		if Req.IdParent == nil {
			return false, errors.New(Const.ID_PARENT_EMPTY)
		}
		if *Req.IdParent < 1 {
			return false, errors.New(Const.ID_PARENT_INVALID)
		}
	}
	return true, nil
}

func CheckIdDisposalGroupEvent(Req *Part.CreateRequest, repo interfaces.RequestRepository) (bool, error) {
	if Req.DisposalType == "Non-System" {
		if flag, err := repo.CheckDataIdDisposalGroupEvent(*Req.IdDisposalGroupEvent); flag != true || err != nil {
			return false, errors.New(err.Error())
		}
		if flag, err := repo.CheckDataIdParentGroupEvent(*Req.IdParent); flag != true || err != nil {
			return false, errors.New(err.Error())
		}
	}
	if Req.DisposalType == "System" {

	}
	return true, nil
}

func GetFrontCodeByType(Type presistence.RequestType) string {
	if Type == "Disposal" {
		return "DI"
	}
	if Type == "Request" {
		return "PR"
	}
	if Type == "Ringi" {
		return "RG"
	}
	if Type == "Discrepancy" {
		return "DS"
	}
	return "NA"
}

func CalculatePartRequestEndCode(sequenceLevel uint) string {
	return fmt.Sprintf("%03d", sequenceLevel)
}

func GeneratePartRequestCode(Res Sequence.Sequence) string {
	currentTime := time.Now().Format("2006#01#02")
	splitDate := strings.Split(currentTime, "#")
	return GetFrontCodeByType(Res.Type) + splitDate[0][2:] + splitDate[1] + CalculatePartRequestEndCode(Res.SequenceLevel)
}
