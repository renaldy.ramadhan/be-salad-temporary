package helpers

import (
	UserPresistence "BE-Epson/domains/master/presistence"
	PartPresistence "BE-Epson/domains/part/presistence"
)

func IsValidColumn(column string) bool {
	_, validColumns := UserPresistence.UserValidColumns[column]
	return validColumns
}

func IsRequestValidColumn(column string) bool {
	_, validColumns := PartPresistence.RequestValidColumns[column]
	return validColumns
}

func IsValidColumnRole(colum string) bool {
	_, validColumns := UserPresistence.RoleValidColums[colum]
	return validColumns
}
func IsValidColumnDivision(colum string) bool {
	_, validColumns := UserPresistence.DepartmentValidColums[colum]
	return validColumns
}

func IsValidColumnPart(column string) bool {
	_, validColumns := PartPresistence.ValidColumnsPart[column]
	return validColumns
}

func IsStatusRequest(column string) PartPresistence.StatusRequest {
	switch column {
	case string(PartPresistence.InProgress), string(PartPresistence.Done), string(PartPresistence.Rejected):
		return PartPresistence.StatusRequest(column)
	}
	return ""
}

func IsRequestType(column string) PartPresistence.RequestType {
	switch column {
	case string(PartPresistence.Request), string(PartPresistence.Ringi), string(PartPresistence.Disposal), string(PartPresistence.Discrepancy):
		return PartPresistence.RequestType(column)
	}
	return ""
}

func IsDisposalType(column string) PartPresistence.DisposalType {
	switch column {
	case string(PartPresistence.System), string(PartPresistence.NonSystem):
		return PartPresistence.DisposalType(column)
	}
	return ""
}

func IsStatusActiveApprovalList(column string) PartPresistence.StatusRequestApprovalListAndHistory {
	switch column {
	case string(PartPresistence.InProgress), string(PartPresistence.Done), string(PartPresistence.Rejected), string(PartPresistence.Queued):
		return PartPresistence.StatusRequestApprovalListAndHistory(column)
	}
	return ""
}

func IsReasonTypes(column string) bool {
	if column == string(PartPresistence.Request) || column == string(PartPresistence.Disposal) || column == string(PartPresistence.Discrepancy) || column == string(PartPresistence.Ringi) {
		return true
	}
	return false
}

func CheckIfStatusNotInProgressOrQueued(column PartPresistence.StatusRequestApprovalListAndHistory) bool {
	switch column {
	case PartPresistence.InProgress, PartPresistence.Queued:
		return false
	}
	return true
}

func CheckActiveStatusIfRejected(column PartPresistence.StatusRequestApprovalListAndHistory) bool {
	switch column {
	case PartPresistence.Rejected:
		return true
	}
	return false
}

func TagToMessage(tag string) string {
	switch tag {
	case "required":
		return "This field is required"
	case "email":
		return "Invalid email format"
	case "oneof":
		return "Option is not allowed"
	}
	return ""
}

func StringPointer(s string) *string {
	return &s
}

//func SendMail(send *request.MailSend) error {
//	cc := []string{}
//	bcc := []string{}
//	if send.Cc != "" {
//		cc = []string{send.Cc}
//	}
//	if send.Bcc != "" {
//		bcc = []string{send.Bcc}
//	}
//	sender := Mail.GetEmailSender()
//	err := sender.SendEmail(
//		send.Subject,
//		send.Content,
//		[]string{send.To},
//		cc,
//		bcc,
//		[]string{send.Attach})
//	if err != nil {
//		return err
//	}
//	return nil
//}
