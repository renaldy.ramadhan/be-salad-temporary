package entities

import (
	"time"

	"gorm.io/gorm"
)

// Timestamp this function only extends to current entity, not stuct or create its own database
type Timestamp struct {
	CreatedAt time.Time      `json:"created_at"`
	UpdateAt  time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at"`
}
