package logger

import (
	"gopkg.in/natefinch/lumberjack.v2"
	"io"
	"log"
	"os"
)

var (
	Debug   *log.Logger
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
	Logs    *lumberjack.Logger
)

func init() {
	Logs = &lumberjack.Logger{
		Filename:   "./logs/app.log",
		MaxSize:    10, // megabytes
		MaxBackups: 5,
		MaxAge:     1, // days
		Compress:   true,
	}

	multi := io.MultiWriter(Logs, os.Stdout)

	Debug = log.New(multi,
		"DEBUG: ",
		log.Ldate|log.Ltime)

	Info = log.New(multi,
		"INFO: ",
		log.Ldate|log.Ltime)

	Warning = log.New(multi,
		"WARNING: ",
		log.Ldate|log.Ltime)

	Error = log.New(multi,
		"ERROR: ",
		log.Ldate|log.Ltime)
}
