package Part

import (
	Department "BE-Epson/domains/master/entities"
	Request "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/presistence"
	"gorm.io/gorm"
	"time"
)

type GetOneActiveRequestApprovalListResponse struct {
	gorm.Model
	IdFlowStep       uint                                            `json:"IdFlowStep"`
	IdDepartment     uint                                            `json:"IdDepartment"`
	IdRequest        *uint                                           `json:"IdRequest"`
	IdEvent          *uint                                           `json:"IdEvent"`
	DueDateApproval  *time.Time                                      `json:"DueDateApproval"`
	DateApproval     *time.Time                                      `json:"DateApproval"`
	ApprovalSequence uint                                            `json:"ApprovalSequence"`
	Notes            string                                          `json:"Notes"`
	ApproverNik      string                                          `json:"ApproverNik"`
	Status           presistence.StatusRequestApprovalListAndHistory `json:"Status,omitempty" binding:"oneof='Done','In Progress','Queued','Rejected'"`
	FlowStep         FlowStep                                        `json:"-"`
	Department       Department.Department                           `json:"Department"`
	Request          Request.Request                                 `json:"Request"`
	ApproverName     string                                          `json:"ApproverName"`
}
