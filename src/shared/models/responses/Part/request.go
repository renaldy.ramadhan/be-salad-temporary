package Part

import (
	ItemPivot "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/presistence"
	"time"
)

type RequestResponse struct {
	Id                     uint                        `json:"Id"`
	UserName               string                      `json:"UserName"`
	IdReason               uint                        `json:"IdReason"`
	LocationCode           string                      `json:"LocationCode"`
	IdDisposalGroupEventFk *uint                       `json:"IdDisposalGroupEvent"`
	PurchasingGroupCodeFk  *string                     `json:"PurchasingGroupCodeFk"`
	NikRequest             string                      `json:"NikRequest"`
	LineCode               string                      `json:"LineCode"`
	IdParent               *uint                       `json:"IdParent,omitempty"`
	Type                   presistence.RequestType     `json:"Type"`
	RequestDate            time.Time                   `json:"RequestDate"`
	DueDate                time.Time                   `json:"DueDate"`
	PlantName              string                      `json:"PlantName"`
	Notes                  string                      `json:"Notes"`
	PartRequestCode        string                      `json:"PartRequestCode"`
	RequestLocation        string                      `json:"RequestLocation"`
	DestroyLocation        string                      `json:"DestroyLocation"`
	DisposalType           string                      `json:"DisposalType,omitempty"`
	WarehouseLocation      *string                     `json:"WarehouseLocation"`
	Items                  []ItemPivot.ItemPivotCreate `json:"Items"`
}

type GenerateRequestList struct {
	WorkFlow uint       `json:"WorkFlow"`
	FlowStep []FlowStep `json:"FlowStep"`
}

type FlowStep struct {
	Id           uint   `json:"Id"`
	IdWorkFlow   uint   `json:"IdWorkFlow"`
	IdDepartment uint   `json:"IdDepartment"`
	FlowSequence uint   `json:"FlowSequence"`
	StepName     string `json:"StepName"`
}

type GetOneRequestResponse struct {
	Request   *ItemPivot.Request           `json:"Request"`
	Items     []ItemPivot.ItemStagingPivot `json:"ItemsData"`
	PrintData bool                         `json:"PrintData"`
	Message   string                       `json:"Message"`
}
