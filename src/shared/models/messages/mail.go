package messages

import "BE-Epson/domains/part/presistence"

type MailPayload struct {
	ToAddress      string
	Subject        string
	ApproverName   *string
	DocumentNumber *string
	RequestType    presistence.RequestType
	RequestorName  *string
	MailType       presistence.MailType
}
