package messages

type APIBadRequest struct {
	Field string
	Msg   string
}
