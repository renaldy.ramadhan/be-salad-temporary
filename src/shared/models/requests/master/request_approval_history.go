package master

type RequestApprovalHistoryGet struct {
	IdRequestApprovalHistory uint `json:"IdRequestApprovalHistory"`
	IdRequest                uint `json:"IdRequest"`
	IdApprover               uint `json:"IdApprover"`
	IdFlowStep               uint `json:"IdFlowStep"`
	LastApproverSequence     uint `json:"LastApproverSequence"`
	ApprovedQty              uint `json:"ApprovedQty"`
	Limit                    uint `json:"Limit"`
	Page                     uint `json:"Page"`
	Offset                   uint `json:"Offset"`
}
