package master

type ApprovalSearchByDepartment struct {
	DepartmentId        uint   `json:"DepartmentId"`
	PurchasingGroupCode string `json:"PurchasingGroupCode"`
	GroupType           string `json:"GroupType"`
	Limit               int    `json:"Limit"`
	Page                int    `json:"Page"`
	Search              string `json:"Search"`
}
