package master

type DepartmentQuery struct {
	Limit  int    `json:"Limit"`
	Page   int    `json:"Page"`
	Search string `json:"Search"`
}
