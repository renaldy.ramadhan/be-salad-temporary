package Part

import (
	"BE-Epson/domains/part/presistence"
	"time"
)

type DisposalEventPdf struct {
	ReferenceCode        *string             `json:"ReferenceCode"`
	ExecutionDate        *time.Time          `json:"ExecutionDate"`
	StartDate            *time.Time          `json:"StartDate"`
	EndDate              *time.Time          `json:"EndDate"`
	TotalSystemQty       int64               `json:"TotalSystemQty"`
	TotalNonSystemQty    int64               `json:"TotalNonSystemQty"`
	TotalAmountSystem    float64             `json:"TotalAmountSystem"`
	TotalAmountNonSystem float64             `json:"TotalAmountNonSystem"`
	TotalWeightSystem    float64             `json:"TotalWeightSystem"`
	TotalWeightNonSystem float64             `json:"TotalWeightNonSystem"`
	ApprovalList         []EventApprovalList `json:"ApprovalList"`
}

type EventApprovalList struct {
	DateApproval     *time.Time                                      `json:"DateApproval"`
	ApprovalSequence uint                                            `json:"ApprovalSequence"`
	Notes            string                                          `json:"Notes"`
	ApproverNik      string                                          `json:"ApproverNik"`
	Status           presistence.StatusRequestApprovalListAndHistory `json:"Status"`
	ApproverName     string                                          `json:"ApproverName"`
	DepartmentName   string                                          `json:"DepartmentName"`
}
