package Part

type FlowStepGet struct {
	IdFlowStep   uint `json:"IdFlowStep"`
	IdDepartment uint `json:"IdDepartment"`
	IdWorkflow   uint `json:"IdWorkflow"`
	Limit        int  `json:"Limit"`
	Page         int  `json:"Page"`
}
