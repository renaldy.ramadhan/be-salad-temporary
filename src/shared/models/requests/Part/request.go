package Part

import (
	Approver "BE-Epson/domains/master/entities"
	ItemPivot "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/presistence"
	OrganizationApprovalRoute "BE-Epson/domains/staging/entities"
	"mime/multipart"
	"time"
)

// Request
type CreateRequest struct {
	Id                    uint                        `json:"Id"`
	UserName              string                      `json:"UserName"`
	IdReason              uint                        `json:"IdReason" binding:"required"`
	Nik                   string                      `json:"Nik" binding:"required"`
	PurchasingGroupCodeFk *string                     `json:"PurchasingGroupCodeFk"`
	IdParent              *uint                       `json:"IdParent,omitempty" binding:"omitempty,numeric,gte=1"`
	IdDisposalGroupEvent  *uint                       `json:"IdDisposalGroupEvent,omitempty" binding:"omitempty,numeric,gte=1"`
	LocationCodeFk        string                      `json:"LocationCode" binding:"required"`
	PartRequestCode       string                      `json:"PartRequestCode"`
	Type                  presistence.RequestType     `json:"Type" binding:"required,oneof=Request Ringi Disposal Discrepancy"`
	RequestDate           time.Time                   `json:"RequestDate"`
	DueDate               time.Time                   `json:"DueDate"`
	PlantName             string                      `json:"PlantName"`
	Notes                 string                      `json:"Notes"`
	RequestLocation       string                      `json:"RequestLocation" binding:"required"`
	DestroyLocation       string                      `json:"DestroyLocation"`
	WarehouseLocation     *string                     `json:"WarehouseLocation"`
	DisposalType          string                      `json:"DisposalType,omitempty" binding:"omitempty,oneof=System 'Non-System'"`
	Items                 []ItemPivot.ItemPivotCreate `json:"Items" binding:"required"`
	Files                 []*multipart.FileHeader     `json:"-"` // Tidak diikat dari JSON, hanya dari form-data
}

type UpdateRequest struct {
	RequestId             uint                    `json:"RequestId" binding:"required,gte=1,numeric"`
	IdReason              *uint                   `json:"IdReason,omitempty" binding:"omitempty,numeric,gte=1"`
	PurchasingGroupCodeFk *string                 `json:"PurchasingGroupCodeFk,omitempty" binding:"omitempty"`
	IdParent              *uint                   `json:"IdParent,omitempty" binding:"omitempty,numeric,gte=1"`
	Type                  presistence.RequestType `json:"Type,omitempty" binding:"omitempty,oneof='Request','Ringi','Disposal','Discrepancy'"`
	RequestDate           *time.Time              `json:"RequestDate,omitempty" binding:"omitempty"`
	DueDate               *time.Time              `json:"DueDate,omitempty" binding:"omitempty"`
	PlantName             string                  `json:"PlantName,omitempty" binding:"omitempty"`
	Notes                 string                  `json:"Notes,omitempty"`
	RequestLocation       string                  `json:"RequestLocation,omitempty" binding:"omitempty"`
	DestroyLocation       string                  `json:"DestroyLocation,omitempty" binding:"omitempty"`
	Status                string                  `json:"Status,omitempty" binding:"omitempty,oneof='In Progress','Done','Rejected'"`
	Items                 []ItemPivot.ItemPivot   `json:"Items" binding:"required"`
}

type SearchRequest struct {
	Id        uint                    `json:"Id"`
	IdEvent   uint                    `json:"IdEvent"`
	OrderBy   string                  `json:"OrderBy"`
	Type      presistence.RequestType `json:"Type"`
	AscOrDesc string                  `json:"AscOrDesc"`
	Search    string                  `json:"Search"`
	Page      int                     `json:"Page"`
	PageSize  int                     `json:"PageSize"`
	Nik       string                  `json:"Nik"`
	LineCode  string                  `json:"LineCode"`
}

type Request struct {
	PartId   uint   `json:"PartId" binding:"required"`
	PartName string `json:"PartName"`
	ReqQty   uint   `json:"ReqQty" binding:"required,min=1,numeric"`
	Remark   string `json:"Remark"`
}

type GetApproveData struct {
	Approver Approver.Approver `json:"Approver"`
	FlowStep Approver.FlowStep `json:"FlowStep"`
}

type GetOrganizationApprovalRoute struct {
	OrganizationApprovalData OrganizationApprovalRoute.OrganizationApprovalRoute `json:"OrganizationApprovalData"`
	FlowStep                 Approver.FlowStep                                   `json:"FlowStep"`
}

type UpdateStatusActiveRequestList struct {
	IdActiveRequestList uint              `json:"IdActiveRequestList" binding:"required"`
	Status              string            `json:"Status" binding:"required,oneof='Done' 'Rejected'"`
	Notes               string            `json:"Notes" binding:"omitempty"`
	Nik                 string            `json:"Nik"`
	ItemPivot           []UpdateItemPivot `json:"ItemPivot,omitempty" binding:"omitempty"`
}

type UpdateStatusEventApproval struct {
	IdEvent uint   `json:"IdEvent" binding:"required"`
	Status  string `json:"Status" binding:"required,oneof='Done' 'Rejected'"`
	Notes   string `json:"Notes" binding:"omitempty"`
	Nik     string `json:"Nik"`
}

// Item Pivot
type UpdateItemPivot struct {
	IdItemPivot        uint    `json:"IdItemPivot" binding:"required"`
	ItemCode           string  `json:"ItemCode" binding:"required,omitempty"`
	RequestQuantity    int     `json:"RequestQuantity" binding:"omitempty,required,min=1,numeric"`
	ApprovedQuantity   int     `json:"ApprovedQuantity" binding:"omitempty,required,min=1,numeric"`
	AdjustmentQuantity int     `json:"AdjustmentQuantity" binding:"omitempty,required,min=1,numeric"`
	Notes              string  `json:"Notes" binding:"omitempty"`
	Amount             float64 `json:"Amount"`
}
