package main

import (
	appConfig "BE-Epson/configs/app"
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	helpers "BE-Epson/shared/helpers"
	"BE-Epson/shared/logger"
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"os"
	"path/filepath"
)

// func init() {
// 	err := godotenv.Load("local.env")
// 	if err != nil {
// 		log.Fatal("Error loading local.env file")
// 	}
// }

func main() {
	app := gin.Default()

	if err := os.Mkdir("./logs/", 0777); err != nil {
		logger.Error.Println("Failed to create log directory:", err)
	} else {
		path, err := filepath.Abs("./logs/")
		if err != nil {
			logger.Error.Println("Failed to get absolute path:", err)
		} else {
			logger.Info.Println("Log directory created at:", path)
		}
	}
	writable, err := IsWritable("./logs/")
	if !writable {
		logger.Error.Println("Failed to write file into directory:", err)
	} else {
		logger.Info.Println("Directory is writable")
	}
	log.SetOutput(logger.Logs)

	app.Use(gin.LoggerWithWriter(io.MultiWriter(logger.Logs, os.Stderr)))

	appPort := os.Getenv("ASPNETCORE_PORT")
	if appPort == "" {
		appPort = helpers.ParseUintString(appConfig.GetConfig().App.Port)
	}
	if appConfig.GetConfig().App.FERouting {
		RegisterFERoute(app)
	}
	if appConfig.GetConfig().Database.DatabaseMigrate {
		Migrate(database.ConnectDatabase(Const.DB_SALAD))
	} else {
		database.ConnectDatabase(Const.DB_SALAD)
	}
	database.ConnectDatabase(Const.EXTERNAL_STK_ADJ)
	if appConfig.GetConfig().Database.DatabaseSeed {
		database.SeedData()
	}
	RegisterTrustedProxies(app)
	RegisterRoutes(app)
	RegisterMiddlewares(app)
	SetupCronJobs()
	RegisterAppRepo()
	logger.Info.Println("Application is running at port " + appPort)
	err = app.Run(":" + appPort)
	if err != nil {
		logger.Error.Fatalf("Failed to run the app: %v", err)
	}
}

func IsWritable(path string) (bool, error) {
	tmpFile := "tmpfile"

	file, err := os.CreateTemp(path, tmpFile)
	if err != nil {
		return false, err
	}

	defer os.Remove(file.Name())
	defer file.Close()

	return true, nil
}
