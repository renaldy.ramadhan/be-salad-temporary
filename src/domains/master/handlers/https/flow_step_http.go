package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	FlowStep "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	"BE-Epson/domains/master/interfaces/impl/repository"
	"BE-Epson/domains/master/interfaces/impl/usecase"
	interfaces2 "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/messages"
	"BE-Epson/shared/models/requests/Part"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
)

type FlowStepHttp struct {
	flowStepUsecase interfaces.FlowStepUseCase
}

func NewFlowStepHttp(router *gin.Engine, stagingUserRepo interfaces2.StagingUserRepository) *FlowStepHttp {
	handler := &FlowStepHttp{
		flowStepUsecase: usecase.NewFlowStepUsecase(repository.NewFlowStepRepository(
			database.ConnectDatabase(Const.DB_SALAD))),
	}
	flowStep := router.Group("/salad/api/flow-step")
	{
		flowStep.GET("", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetAll)
		flowStep.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetOne)
		flowStep.GET("/workflow/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetAllFlowStepByWorkflow)
		flowStep.POST("/create", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.CreateFlowStep)
		flowStep.PUT("/update/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.UpdateFlowStep)
		flowStep.DELETE("/delete/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.DeleteFlowStep)
	}
	return handler
}

func (FlowStepHttp *FlowStepHttp) GetAll(c *gin.Context) {
	req := &Part.FlowStepGet{
		IdFlowStep:   helpers.ParseUint(c.Query("id_flow_step")),
		IdWorkflow:   helpers.ParseUint(c.Query("id_workflow")),
		IdDepartment: helpers.ParseUint(c.Query("id_department")),
		Limit:        helpers.ParseInt(c.Query("limit")),
		Page:         helpers.ParseInt(c.Query("page")),
	}
	result, err := FlowStepHttp.flowStepUsecase.GetAllFlowStep(req)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusOK, result)
}

func (FlowStepHttp *FlowStepHttp) GetOne(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	result, err := FlowStepHttp.flowStepUsecase.GetOneFlowStep(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusOK, result)
}

func (FlowStepHttp *FlowStepHttp) GetAllFlowStepByWorkflow(c *gin.Context) {
	id := c.MustGet("WorkflowId").(uint)
	result, err := FlowStepHttp.flowStepUsecase.GetAllFlowStepByWorkflowById(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	c.Set("FlowStep", result)
}

func (FlowStepHttp *FlowStepHttp) CreateFlowStep(c *gin.Context) {
	body := &FlowStep.FlowStepPayload{}
	if err := c.ShouldBindJSON(body); err != nil {
		var ve validator.ValidationErrors
		if errors.As(err, &ve) {
			out := make([]messages.APIBadRequest, len(ve))
			fmt.Print(ve)
			for i, fe := range ve {
				out[i] = messages.APIBadRequest{Field: fe.Field(), Msg: helpers.TagToMessage(fe.Tag())}
			}
			c.JSON(http.StatusBadRequest, gin.H{
				"message": "Bad Request",
				"error":   out,
			})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": "Bad Request",
				"error":   err,
			})
		}
		return
	}

	result, err := FlowStepHttp.flowStepUsecase.StoreFlowStep(body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"message": "Created Successfully!",
		"data":    result,
	})
}

func (FlowStepHttp *FlowStepHttp) UpdateFlowStep(c *gin.Context) {
	id := c.Param("id")
	body := &FlowStep.FlowStepPayload{}
	if err := c.ShouldBindJSON(body); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	result, err := FlowStepHttp.flowStepUsecase.UpdateFlowStep(body, id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Update Successfully!",
		"data":    result,
	})
}

func (FlowStepHttp *FlowStepHttp) DeleteFlowStep(c *gin.Context) {
	id := c.Param("id")
	result, err := FlowStepHttp.flowStepUsecase.DeleteFlowStep(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Delete Failed!",
			"status":  result,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "Delete Successfully!",
		"status":  result,
	})
}
