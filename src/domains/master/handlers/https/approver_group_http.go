package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	ApproverGroup "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	"BE-Epson/domains/master/interfaces/impl/repository"
	"BE-Epson/domains/master/interfaces/impl/usecase"
	"BE-Epson/domains/part/presistence"
	interfaces2 "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/requests/master"
	"BE-Epson/shared/models/responses/Part"
	"net/http"

	"github.com/gin-gonic/gin"
)

type ApproverGroupHttp struct {
	approveGroupUsecase interfaces.ApproverGroupUseCase
}

func NewApproveGroupHttp(router *gin.Engine, stagingUserRepo interfaces2.StagingUserRepository) *ApproverGroupHttp {
	handler := &ApproverGroupHttp{
		approveGroupUsecase: usecase.NewApproverGroupUseCase(repository.NewApproverGroupRepo(database.ConnectDatabase(Const.DB_SALAD))),
	}
	approverGroup := router.Group("/salad/api/approver-group")
	{
		approverGroup.GET("/", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetAllApproverGroup)
		approverGroup.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetApproveGroupById)
		approverGroup.POST("/create", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.CreateApprovalGroup)
		approverGroup.PUT("/update/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.UpdateApproverGroup)
		approverGroup.DELETE("/delete/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.DeleteApprover)
	}
	return handler
}

func (ApproverGroupHttp *ApproverGroupHttp) GetAllApproverGroup(c *gin.Context) {
	data := &master.ApprovalSearchByDepartment{
		DepartmentId:        helpers.ParseUint(c.Query("department_id")),
		PurchasingGroupCode: c.Query("purchasing_group_code"),
		GroupType:           c.Query("group_type"),
		Limit:               helpers.ParseInt(c.Query("limit")),
		Page:                helpers.ParseInt(c.Query("page")),
		Search:              c.Query("search"),
	}
	approverGroup, err := ApproverGroupHttp.approveGroupUsecase.GetAllApproverGroup(data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, approverGroup)
}
func (ApproverGroupHttp *ApproverGroupHttp) GetApproveGroupById(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	approverGroup, err := ApproverGroupHttp.approveGroupUsecase.GetApproverGroupById(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, approverGroup)
}

func (ApproverGroupHttp *ApproverGroupHttp) GenerateApprovalGroup(c *gin.Context) {
	var (
		approvalGroupData *ApproverGroup.ApproverGroup
		err               error
	)
	//userData := c.MustGet("UserData").(*SessionUser.SessionUser)
	responseRequest := c.MustGet("ResponseCreateRequest").(*Part.RequestResponse)
	flowStepData := c.MustGet("FlowStep").([]ApproverGroup.FlowStep)
	for _, value := range flowStepData {
		err = nil
		if value.Type == presistence.Location && responseRequest.PurchasingGroupCodeFk != nil {
			approvalGroupData, err = ApproverGroupHttp.approveGroupUsecase.GetAllApproverGroupByDeptIdAndPurchasingGroup(*value.IdDepartment, *responseRequest.PurchasingGroupCodeFk, helpers.ParseToString(responseRequest.Type))
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"message": err.Error(),
				})
				return
			}
		}
	}
	c.Set("ApprovalGroupData", approvalGroupData)
}

func (ApproverGroupHttp *ApproverGroupHttp) CreateApprovalGroup(c *gin.Context) {
	data := &ApproverGroup.ApproverGroup{}
	if err := c.ShouldBindJSON(data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	approvalGroup, err := ApproverGroupHttp.approveGroupUsecase.CreateApprovalGroup(data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, approvalGroup)
}

func (ApproverGroupHttp *ApproverGroupHttp) UpdateApproverGroup(c *gin.Context) {
	id := c.Param("id")
	data := &ApproverGroup.ApproverGroup{}
	if err := c.ShouldBindJSON(data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	result, err := ApproverGroupHttp.approveGroupUsecase.UpdateApproverGroup(data, id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Update Successfully!",
		"data":    result,
	})
}

func (ApproverGroupHttp *ApproverGroupHttp) DeleteApprover(c *gin.Context) {
	id := c.Param("id")
	result, err := ApproverGroupHttp.approveGroupUsecase.DeleteApproverGroup(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"status":  result,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Delete Successfully",
		"status":  result,
	})
}
