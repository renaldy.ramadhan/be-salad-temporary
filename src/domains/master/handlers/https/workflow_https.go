package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	Workflow "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	"BE-Epson/domains/master/interfaces/impl/repository"
	"BE-Epson/domains/master/interfaces/impl/usecase"
	interfaces2 "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/responses/Part"
	"net/http"

	"github.com/gin-gonic/gin"
)

type WorkflowHttps struct {
	WorkflowUseCase interfaces.WorkflowUseCase
}

func NewWorkflowHttps(router *gin.Engine, stagingUserRepo interfaces2.StagingUserRepository) *WorkflowHttps {
	handler := &WorkflowHttps{
		WorkflowUseCase: usecase.NewWorkflowUseCase(repository.NewWorkflowRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}
	Workflow := router.Group("/salad/api/workflow")
	{
		Workflow.GET("/", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetAllWorkflow)
		Workflow.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetWorkflowById)
		Workflow.GET("/find/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.FindSingleWorkflow)
		Workflow.GET("/type/:type", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetWorkFlowId)
		Workflow.POST("/create", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.CreateWorkFlow)
		Workflow.PUT("/update/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.UpdateWorkFlow)
		Workflow.DELETE("/delete/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.DeleteWorkflow)

	}
	return handler
}

func (WorkflowHttps *WorkflowHttps) GetAllWorkflow(c *gin.Context) {
	data, err := WorkflowHttps.WorkflowUseCase.GetAllWorkflow()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (WorkflowHttps *WorkflowHttps) FindSingleWorkflow(c *gin.Context) {
	id := helpers.ParseToUint(c.Param("id"))
	data, err := WorkflowHttps.WorkflowUseCase.FindSingleWorkflow(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Something went wrong!",
		})
		return
	}
	c.JSON(http.StatusOK, data)

}

func (WorkflowHttps *WorkflowHttps) GetWorkflowById(c *gin.Context) {
	Req := c.MustGet("ResponseCreateRequest").(*Part.RequestResponse)
	data, err := WorkflowHttps.WorkflowUseCase.GetWorkflowByType(Req.Type)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Set("WorkflowId", data)
}

func (WorkflowHttps *WorkflowHttps) GetWorkFlowId(c *gin.Context) {
	types := c.Param("type")
	dataId, err := WorkflowHttps.WorkflowUseCase.GetWorkFlowId(types)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, dataId)
}

func (WorkflowHttps *WorkflowHttps) CreateWorkFlow(c *gin.Context) {
	req := &Workflow.Workflow{}
	if err := c.ShouldBindJSON(req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	data, err := WorkflowHttps.WorkflowUseCase.CreateWorkflow(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"message": "Created!",
		"payload": data,
	})
}

func (WorkflowHttps *WorkflowHttps) UpdateWorkFlow(c *gin.Context) {
	id := c.Param("id")
	req := &Workflow.Workflow{}
	if err := c.ShouldBindJSON(req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	data, err := WorkflowHttps.WorkflowUseCase.UpdateWorkflow(req, id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Updated!",
		"payload": data,
	})
}

func (WorkflowHttps *WorkflowHttps) DeleteWorkflow(c *gin.Context) {
	id := c.Param("id")
	result, err := WorkflowHttps.WorkflowUseCase.DeleteWorkflow(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Deleted!",
		"status":  result,
	})
}
