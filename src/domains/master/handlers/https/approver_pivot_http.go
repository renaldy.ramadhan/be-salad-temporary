package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	ApproverGroup "BE-Epson/domains/master/entities"
	ApproverPivot "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	"BE-Epson/domains/master/interfaces/impl/repository"
	"BE-Epson/domains/master/interfaces/impl/usecase"
	"BE-Epson/domains/part/presistence"
	interfaces2 "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/requests/Part"
	PartRes "BE-Epson/shared/models/responses/Part"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type ApproverPivotHttp struct {
	approverPivotUsecase interfaces.ApproverPivotUsecase
	approveGroupUsecase  interfaces.ApproverGroupUseCase
}

func NewApproverPivotHttp(router *gin.Engine, stagingUserRepo interfaces2.StagingUserRepository) *ApproverPivotHttp {
	handler := &ApproverPivotHttp{
		approverPivotUsecase: usecase.NewApproverPivotUsecase(repository.NewApproverPivotRepository(database.ConnectDatabase(Const.DB_SALAD))),
		approveGroupUsecase:  usecase.NewApproverGroupUseCase(repository.NewApproverGroupRepo(database.ConnectDatabase(Const.DB_SALAD))),
	}

	approverPivot := router.Group("/salad/api/approver-pivot")
	{
		approverPivot.GET("/", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetAll)
		approverPivot.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetById)
		approverPivot.POST("/create", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.Create)
		approverPivot.PUT("/updates", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.UpdateApproverPivot)
		approverPivot.DELETE("/deletes", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.DeleteApproverPivot)
	}
	return handler
}

func (ApproverPivotHttp *ApproverPivotHttp) GetAll(c *gin.Context) {
	data, err := ApproverPivotHttp.approverPivotUsecase.GetAll()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internal Server Error",
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (ApproverPivotHttp *ApproverPivotHttp) GetById(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	data, err := ApproverPivotHttp.approverPivotUsecase.GetById(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internal Server Error",
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (ApproverPivotHttp *ApproverPivotHttp) GetAllLocationApprover(c *gin.Context) {
	var (
		approvalGroupData *ApproverGroup.ApproverGroup
		err               error
		dataRes           []Part.GetApproveData
		approverData      []ApproverPivot.ApproverPivot
	)
	responseRequest := c.MustGet("ResponseCreateRequest").(*PartRes.RequestResponse)
	flowStepData := c.MustGet("FlowStep").([]ApproverPivot.FlowStep)
	for _, value := range flowStepData {
		err = nil
		if value.Type != presistence.Organization {
			if value.Type == presistence.PurchasingGroup && responseRequest.Type != "Discrepancy" {
				approvalGroupData, err = ApproverPivotHttp.approveGroupUsecase.GetAllApproverGroupByDeptIdAndPurchasingGroup(*value.IdDepartment, *responseRequest.PurchasingGroupCodeFk, helpers.ParseToString(responseRequest.Type))
				if err != nil {
					c.JSON(http.StatusInternalServerError, gin.H{
						"message": err.Error(),
					})
					return
				}
				approverData, err = ApproverPivotHttp.approverPivotUsecase.GetAllApproverByGroupId(approvalGroupData.ID)
				if err != nil {
					c.JSON(http.StatusInternalServerError, gin.H{
						"message": err.Error(),
					})
					return
				}
				for _, valueAprovalGroup := range approverData {
					newData := Part.GetApproveData{
						Approver: valueAprovalGroup.Approver,
						FlowStep: value,
					}
					dataRes = append(dataRes, newData)
				}
			}
			if value.Type == presistence.Location || responseRequest.Type == "Discrepancy" {
				approvalGroupData, err = ApproverPivotHttp.approveGroupUsecase.GetAllApproverGroupByDeptIdAndLocationId(*value.IdDepartment, responseRequest.LocationCode, helpers.ParseToString(responseRequest.Type))
				if err != nil {
					c.JSON(http.StatusInternalServerError, gin.H{
						"message": err.Error(),
					})
					return
				}
				approverData, err = ApproverPivotHttp.approverPivotUsecase.GetAllApproverByGroupId(approvalGroupData.ID)
				if err != nil {
					c.JSON(http.StatusInternalServerError, gin.H{
						"message": err.Error(),
					})
					return
				}
				for _, valueAprovalGroup := range approverData {
					newData := Part.GetApproveData{
						Approver: valueAprovalGroup.Approver,
						FlowStep: value,
					}
					dataRes = append(dataRes, newData)
				}
			} else {
				fmt.Println("Executed")
			}
		}
	}
	c.Set("ApproverData", dataRes)
}

func (ApproverPivotHttp *ApproverPivotHttp) Create(c *gin.Context) {
	idApproverGroup := helpers.ParseToUint(c.MustGet("ApproverGroup_id"))
	idApprover := helpers.ParseToUint(c.MustGet("Approver_id"))
	req := &ApproverPivot.ApproverPivotCreate{
		IdApproverGroup: idApproverGroup,
		IdApprover:      idApprover,
	}

	data, err := ApproverPivotHttp.approverPivotUsecase.CreateApproverPivot(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internal Server Error",
			"error":   err.Error(), // Optionally log the error details
		})
		return
	}
	c.JSON(http.StatusCreated, data)
}

func (ApproverPivotHttp *ApproverPivotHttp) UpdateApproverPivot(c *gin.Context) {
	idApprover := helpers.ParseToUint(c.MustGet("Approver_id"))
	idApproverGroup := c.MustGet("ApproverGroup_id").([]uint)
	req := &ApproverGroup.ApproverPivotUpdate{
		IdApprover:      idApprover,
		IdApproverGroup: idApproverGroup,
	}
	result, err := ApproverPivotHttp.approverPivotUsecase.UpdateManyApproverPivot(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internal Server Error",
			"error":   err.Error(), // Optionally log the error details
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Update successfully",
		"result":  result,
	})
}

func (ApproverPivotHttp *ApproverPivotHttp) DeleteApproverPivot(c *gin.Context) {
	req := []ApproverGroup.ApproverPivotDelete{}
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	result, err := ApproverPivotHttp.approverPivotUsecase.DeleteManyApproverPivot(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internal Server Error",
			"error":   err.Error(), // Optionally log the error details
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "Update successfully",
		"result":  result,
	})
}
