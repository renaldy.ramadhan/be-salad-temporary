package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	ApproverGroup "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	"BE-Epson/domains/master/interfaces/impl/repository"
	"BE-Epson/domains/master/interfaces/impl/usecase"
	OrganizationApprovalRouteHttp "BE-Epson/domains/staging/handlers/http"
	interfaces2 "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	SessionUser "BE-Epson/shared/entities"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/requests/master"
	"github.com/gin-gonic/gin"
	"net/http"
)

type ApproverHttp struct {
	approveUsecase                interfaces.ApproverUseCase
	organizationApprovalRouteHttp *OrganizationApprovalRouteHttp.OrganizationApprovalRouteHttp
	approverPivot                 *ApproverPivotHttp
}

func NewApproverHttp(router *gin.Engine, OrganizationApprovalRouteHttp *OrganizationApprovalRouteHttp.OrganizationApprovalRouteHttp, pivotHttp *ApproverPivotHttp, stagingUserRepo interfaces2.StagingUserRepository) *ApproverHttp {
	handler := &ApproverHttp{
		approveUsecase:                usecase.NewApproverUseCase(repository.NewApproverRepo(database.ConnectDatabase(Const.DB_SALAD))),
		organizationApprovalRouteHttp: OrganizationApprovalRouteHttp,
		approverPivot:                 pivotHttp,
	}
	guest := router.Group("/salad/api/approver")
	{
		guest.GET("/is_approver",
			middlewares.VerifyUserKeycloak(stagingUserRepo),
			middlewares.GetUserInfo(),
			//middlewares.MiddlewareKosong(),
			handler.CheckIsApprover)
		guest.GET("/", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetAllApprover)
		guest.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetApproverById)
		guest.POST("/create", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.CreateApprover)
		guest.PUT("/update/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.UpdateApprover, pivotHttp.UpdateApproverPivot)
		guest.DELETE("/delete/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.DeleteApprover)
	}
	return handler
}

func (ApproverHttp *ApproverHttp) CheckIsApprover(c *gin.Context) {
	data := c.MustGet("UserData").(*SessionUser.SessionUser).Nik
	var flag = false
	flag, _ = ApproverHttp.approveUsecase.CheckDataIsApprover(data)
	if flag {
		c.JSON(http.StatusOK, gin.H{
			"message": flag,
		})
		return
	}
	flag, _ = ApproverHttp.organizationApprovalRouteHttp.CheckDataIsApprover(data)
	if flag {
		c.JSON(http.StatusOK, gin.H{
			"message": flag,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": flag,
	})
}

func (ApproverHttp *ApproverHttp) GetAllApprover(c *gin.Context) {
	dataReq := &master.ApprovalSearchByDepartment{
		Limit:  helpers.ParseInt(c.Query("limit")),
		Page:   helpers.ParseInt(c.Query("page")),
		Search: c.Query("search"),
	}
	approver, err := ApproverHttp.approveUsecase.GetAllApprover(dataReq)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, approver)
}

func (ApproverHttp *ApproverHttp) GetApproverById(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	approver, err := ApproverHttp.approveUsecase.GetApproverById(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, approver)
}

func (ApproverHttp *ApproverHttp) CreateApprover(c *gin.Context) {
	req := &ApproverGroup.ApproverPayload{}
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := ApproverHttp.approveUsecase.CreateOne(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	//c.Set("Approver_id", data.ID)
	//c.Set("ApproverGroup_id", *req.IdApproverGroup)
	c.JSON(http.StatusCreated, data)
}

func (ApproverHttp *ApproverHttp) UpdateApprover(c *gin.Context) {
	id := c.Param("id")
	body := &ApproverGroup.ApproverPayload{}
	if err := c.ShouldBindJSON(body); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	_, err := ApproverHttp.approveUsecase.UpdateApprover(body, id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Set("Approver_id", id)
	c.Set("ApproverGroup_id", body.IdApproverGroup)
}
func (ApproverHttp *ApproverHttp) DeleteApprover(c *gin.Context) {
	id := c.Param("id")
	result, err := ApproverHttp.approveUsecase.DeleteApprover(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "Delete successfully!",
		"status":  result,
	})
}
