package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	Department "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	"BE-Epson/domains/master/interfaces/impl/repository"
	"BE-Epson/domains/master/interfaces/impl/usecase"
	interfaces2 "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	Master "BE-Epson/shared/models/requests/master"
	"github.com/gin-gonic/gin"
	"net/http"
)

type DepartmentHttp struct {
	departmentUsecase interfaces.DepartmentUseCase
}

func NewDepartHttp(router *gin.Engine, stagingUserRepo interfaces2.StagingUserRepository) *DepartmentHttp {
	handler := &DepartmentHttp{
		departmentUsecase: usecase.NewDepartmentUsecase(repository.NewDepartmentRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}
	department := router.Group("/salad/api/department")
	{
		department.GET("", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetAll)
		department.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.GetOne)
		department.POST("/create", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.Create)
		department.PUT("/update/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.Update)
		department.DELETE("/delete/:id", middlewares.VerifyUserKeycloak(stagingUserRepo), handler.Delete)
	}
	return handler
}

func (DepartmentHttp *DepartmentHttp) GetAll(c *gin.Context) {
	query := &Master.DepartmentQuery{
		Page:   helpers.ParseInt(c.Query("page")),
		Limit:  helpers.ParseInt(c.Query("limit")),
		Search: c.Query("search"),
	}
	result, err := DepartmentHttp.departmentUsecase.GetAllDepartment(query)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, result)
}

func (DepartmentHttp *DepartmentHttp) GetOne(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	result, err := DepartmentHttp.departmentUsecase.GetOneDepartment(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, result)
}

func (DepartmentHttp *DepartmentHttp) Create(c *gin.Context) {
	body := &Department.DepartmentCreate{}
	if err := c.ShouldBindJSON(body); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	result, err := DepartmentHttp.departmentUsecase.CreateDepartment(body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"message": "Created Successfully!",
		"data":    result,
	})
}

func (DepartmentHttp *DepartmentHttp) Update(c *gin.Context) {
	param := c.Param("id")
	body := &Department.DepartmentCreate{}
	if err := c.ShouldBindJSON(body); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	result, err := DepartmentHttp.departmentUsecase.UpdateDepartment(body, param)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "Update Successfully!",
		"data":    result,
	})
}

func (DepartmentHttp *DepartmentHttp) Delete(c *gin.Context) {
	id := c.Param("id")
	result, err := DepartmentHttp.departmentUsecase.DeleteDepartment(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Deleted Successfully!",
		"status":  result,
	})
}
