package interfaces

import (
	Workflow "BE-Epson/domains/master/entities"
	"BE-Epson/domains/part/presistence"
)

type WorkflowRepository interface {
	FindAll() ([]Workflow.Workflow, error)
	FindOne(id uint) (*Workflow.Workflow, error)
	FindWorkflowIdByType(types string) (uint, error)
	Create(data *Workflow.Workflow) (*Workflow.Workflow, error)
	Update(data *Workflow.Workflow, param string) (*Workflow.Workflow, error)
	Delete(param string) (bool, error)
}

type WorkflowUseCase interface {
	GetAllWorkflow() ([]Workflow.Workflow, error)
	GetWorkflowByType(types presistence.RequestType) (uint, error)
	GetWorkFlowId(types string) (uint, error)
	FindSingleWorkflow(id uint) (*Workflow.Workflow, error)
	CreateWorkflow(data *Workflow.Workflow) (*Workflow.Workflow, error)
	UpdateWorkflow(data *Workflow.Workflow, param string) (*Workflow.Workflow, error)
	DeleteWorkflow(param string) (bool, error)
}
