package interfaces

import (
	Department "BE-Epson/domains/master/entities"
	Master "BE-Epson/shared/models/requests/master"
)

type DepartmentRepository interface {
	FindAll(req *Master.DepartmentQuery) (data []Department.Department, totalData int64, err error)
	FindOne(id uint) (data *Department.Department, err error)
	Store(data *Department.DepartmentCreate) (*Department.DepartmentCreate, error)
	Update(data *Department.DepartmentCreate, param string) (*Department.DepartmentCreate, error)
	Destroy(param string) (bool, error)
}

type DepartmentUseCase interface {
	GetAllDepartment(req *Master.DepartmentQuery) ([]Department.Department, error)
	GetOneDepartment(id uint) (*Department.Department, error)
	CreateDepartment(data *Department.DepartmentCreate) (*Department.DepartmentCreate, error)
	UpdateDepartment(data *Department.DepartmentCreate, param string) (*Department.DepartmentCreate, error)
	DeleteDepartment(param string) (bool, error)
}
