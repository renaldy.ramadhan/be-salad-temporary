package usecase

import (
	Workflow "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	"BE-Epson/domains/part/presistence"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/helpers"
	"errors"
)

type workflowUseCase struct {
	workflowRepo interfaces.WorkflowRepository
}

func (workflowUseCase workflowUseCase) GetWorkFlowId(types string) (uint, error) {
	//TODO implement me
	panic("implement me")
}

func NewWorkflowUseCase(workflowRepo interfaces.WorkflowRepository) *workflowUseCase {
	return &workflowUseCase{workflowRepo}
}

func (workflowUseCase *workflowUseCase) GetAllWorkflow() ([]Workflow.Workflow, error) {
	data, err := workflowUseCase.workflowRepo.FindAll()
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (workflowUseCase *workflowUseCase) GetWorkflowById(Id uint) (*Workflow.Workflow, error) {
	if Id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	data, err := workflowUseCase.workflowRepo.FindOne(Id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (workflowUseCase *workflowUseCase) GetWorkflowByType(types presistence.RequestType) (uint, error) {
	if types == "" {
		return 0, errors.New(Const.TYPES_EMPTY)
	}
	data, err := workflowUseCase.workflowRepo.FindWorkflowIdByType(helpers.ParseToString(types))
	if err != nil {
		return 0, err
	}
	return data, nil
}

func (workflowUseCase *workflowUseCase) FindSingleWorkflow(id uint) (*Workflow.Workflow, error) {
	data, err := workflowUseCase.workflowRepo.FindOne(id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (workflowUseCase *workflowUseCase) CreateWorkflow(data *Workflow.Workflow) (*Workflow.Workflow, error) {
	data, err := workflowUseCase.workflowRepo.Create(data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (workflowUseCase *workflowUseCase) UpdateWorkflow(data *Workflow.Workflow, param string) (*Workflow.Workflow, error) {
	data, err := workflowUseCase.workflowRepo.Update(data, param)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (workflowUseCase *workflowUseCase) DeleteWorkflow(param string) (bool, error) {
	result, err := workflowUseCase.workflowRepo.Delete(param)
	if err != nil {
		return result, err
	}

	return result, nil
}
