package usecase

import (
	ApproverGroup "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	Const "BE-Epson/shared/errors"
	ApprovalRequest "BE-Epson/shared/models/requests/master"
	"errors"
)

type approverUseCase struct {
	approverRepo interfaces.ApproverRepository
}

func NewApproverUseCase(approverRepo interfaces.ApproverRepository) *approverUseCase {
	return &approverUseCase{approverRepo}
}
func (approverUseCase *approverUseCase) GetAllApprover(Req *ApprovalRequest.ApprovalSearchByDepartment) ([]ApproverGroup.Approver, error) {
	data, err := approverUseCase.approverRepo.FindAll(Req)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverUseCase *approverUseCase) GetApproverById(Id uint) (*ApproverGroup.Approver, error) {
	if Id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	data, err := approverUseCase.approverRepo.FindOne(Id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverUseCase *approverUseCase) GetApproverByGroupId(groupId uint) ([]ApproverGroup.Approver, error) {
	data, err := approverUseCase.approverRepo.FindApproverByGroupId(groupId)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverUseCase *approverUseCase) CreateOne(Req *ApproverGroup.ApproverPayload) (data *ApproverGroup.Approver, err error) {
	data, err = approverUseCase.approverRepo.StoreOne(Req)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverUseCase *approverUseCase) CheckDataIsApprover(Nik string) (bool, error) {
	if Nik == "" {
		return false, errors.New(Const.NIK_EMPTY)
	}
	flag, err := approverUseCase.approverRepo.CheckDataIsApprover(Nik)
	if err != nil {
		return false, err
	}
	return flag, nil
}

func (approverUseCase *approverUseCase) UpdateApprover(data *ApproverGroup.ApproverPayload, param string) (*ApproverGroup.Approver, error) {
	result, err := approverUseCase.approverRepo.UpdateOne(data, param)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (approverUseCase *approverUseCase) DeleteApprover(param string) (bool, error) {
	result, err := approverUseCase.approverRepo.Destroy(param)
	return result, err
}
