package usecase

import (
	Department "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	Const "BE-Epson/shared/errors"
	Master "BE-Epson/shared/models/requests/master"
	"errors"
)

type departmentUsecase struct {
	departmentRepo interfaces.DepartmentRepository
}

func NewDepartmentUsecase(departmentRepo interfaces.DepartmentRepository) *departmentUsecase {
	return &departmentUsecase{departmentRepo}
}

func (departmentUc *departmentUsecase) GetAllDepartment(req *Master.DepartmentQuery) ([]Department.Department, error) {
	departments, _, err := departmentUc.departmentRepo.FindAll(req)
	if err != nil {
		return nil, err
	}
	return departments, nil
}

func (departmentUc *departmentUsecase) GetOneDepartment(id uint) (*Department.Department, error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	department, err := departmentUc.departmentRepo.FindOne(id)
	if err != nil {
		return nil, err
	}
	return department, nil
}

func (departmentUsecase *departmentUsecase) CreateDepartment(data *Department.DepartmentCreate) (*Department.DepartmentCreate, error) {
	result, err := departmentUsecase.departmentRepo.Store(data)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (departmentUsecase *departmentUsecase) UpdateDepartment(data *Department.DepartmentCreate, param string) (*Department.DepartmentCreate, error) {
	result, err := departmentUsecase.departmentRepo.Update(data, param)
	if err != nil {
		return nil, err
	}

	return result, err
}

func (departmentUsecase *departmentUsecase) DeleteDepartment(param string) (bool, error) {
	result, err := departmentUsecase.departmentRepo.Destroy(param)
	if err != nil {
		return result, err
	}

	return result, nil
}
