package usecase

import (
	ApproverGroup "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/models/requests/master"
	"errors"
)

type approverGroupUseCases struct {
	approverGroupRepo interfaces.ApproverGroupRepository
}

func NewApproverGroupUseCase(approverGroupRepo interfaces.ApproverGroupRepository) *approverGroupUseCases {
	return &approverGroupUseCases{approverGroupRepo}
}
func (approverGroupUseCase *approverGroupUseCases) GetAllApproverGroup(Req *master.ApprovalSearchByDepartment) ([]ApproverGroup.ApproverGroup, error) {
	data, err := approverGroupUseCase.approverGroupRepo.FindAll(Req)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupUseCase *approverGroupUseCases) GetListApproverGroupByType(groupType string) ([]ApproverGroup.ApproverGroup, error) {
	data, err := approverGroupUseCase.approverGroupRepo.FindListApproverGroupByType(groupType)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupUseCase *approverGroupUseCases) GetAllApproverGroupDataApprover(Req *master.ApprovalSearchByDepartment) ([]ApproverGroup.Approver, error) {
	data, err := approverGroupUseCase.approverGroupRepo.FindAll(Req)
	if err != nil {
		return nil, err
	}
	res, err := approverGroupUseCase.approverGroupRepo.FindAllApproverPivotFromGroup(data)
	if err != nil {
		return nil, err
	}
	dataApprover, err := approverGroupUseCase.approverGroupRepo.FindAllApproverFromPivot(res)
	if err != nil {
		return nil, err
	}
	return dataApprover, nil
}

func (approverGroupUseCase *approverGroupUseCases) GetApproverGroupById(Id uint) (*ApproverGroup.ApproverGroup, error) {
	if Id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	data, err := approverGroupUseCase.approverGroupRepo.FindOne(Id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupUseCase *approverGroupUseCases) GenerateApprovalGroupData(step *ApproverGroup.FlowStep) ([]ApproverGroup.ApproverGroup, error) {
	data, err := approverGroupUseCase.approverGroupRepo.GenerateApprovalGroupData(step.ID)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupUseCase *approverGroupUseCases) CreateApprovalGroup(step *ApproverGroup.ApproverGroup) (data *ApproverGroup.ApproverGroup, err error) {
	data, err = approverGroupUseCase.approverGroupRepo.StoreOne(step)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupUseCase *approverGroupUseCases) GetAllApproverGroupByDeptIdAndPurchasingGroup(id uint, pg string, groupType string) (*ApproverGroup.ApproverGroup, error) {
	data, err := approverGroupUseCase.approverGroupRepo.FindAllApproverGroupByDeptIdAndPurchasingGroup(id, pg, groupType)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupUseCase *approverGroupUseCases) GetAllApproverGroupByDeptIdAndLocationId(id uint, locationId string, groupType string) (data *ApproverGroup.ApproverGroup, err error) {
	data, err = approverGroupUseCase.approverGroupRepo.FindAllApproverGroupByDeptIdAndLocationId(id, locationId, groupType)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupUseCase *approverGroupUseCases) UpdateApproverGroup(data *ApproverGroup.ApproverGroup, param string) (*ApproverGroup.ApproverGroup, error) {
	result, err := approverGroupUseCase.approverGroupRepo.UpdateOne(data, param)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (approverGroupUseCase *approverGroupUseCases) DeleteApproverGroup(param string) (bool, error) {
	result, err := approverGroupUseCase.approverGroupRepo.DestroyOne(param)
	return result, err
}
