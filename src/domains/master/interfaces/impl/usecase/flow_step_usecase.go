package usecase

import (
	FlowStep "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/models/requests/Part"
	"errors"
)

type flowStepUsecase struct {
	flowStepRepo interfaces.FlowStepRepository
}

func NewFlowStepUsecase(flowStepRepo interfaces.FlowStepRepository) *flowStepUsecase {
	return &flowStepUsecase{flowStepRepo}
}

func (flowStepUc *flowStepUsecase) GetAllFlowStep(req *Part.FlowStepGet) ([]FlowStep.FlowStep, error) {
	flowSteps, _, err := flowStepUc.flowStepRepo.FindAll(req)
	if err != nil {
		return nil, err
	}
	return flowSteps, nil
}

func (flowStepUc *flowStepUsecase) GetOneFlowStep(id uint) (*FlowStep.FlowStep, error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	flowStep, err := flowStepUc.flowStepRepo.FindOne(id)
	if err != nil {
		return nil, err
	}
	return flowStep, nil
}

func (flowStepUc *flowStepUsecase) GetAllFlowStepByWorkflowById(id uint) ([]FlowStep.FlowStep, error) {
	if id <= 0 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	flowSteps, _, err := flowStepUc.flowStepRepo.FindAllByWorkFlow(id)
	if err != nil {
		return nil, err
	}
	return flowSteps, nil
}

func (flowStepUsecase *flowStepUsecase) StoreFlowStep(data *FlowStep.FlowStepPayload) (*FlowStep.FlowStepPayload, error) {
	result, err := flowStepUsecase.flowStepRepo.Create(data)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (flowStepUsecase *flowStepUsecase) UpdateFlowStep(data *FlowStep.FlowStepPayload, param string) (*FlowStep.FlowStep, error) {
	result, err := flowStepUsecase.flowStepRepo.Update(data, param)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (flowStepUsecase *flowStepUsecase) DeleteFlowStep(param string) (bool, error) {
	result, err := flowStepUsecase.flowStepRepo.Destroy(param)
	return result, err
}
