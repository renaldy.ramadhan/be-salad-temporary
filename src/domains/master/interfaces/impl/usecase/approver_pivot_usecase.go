package usecase

import (
	ApproverPivot "BE-Epson/domains/master/entities"
	"BE-Epson/domains/master/interfaces"
	Const "BE-Epson/shared/errors"
	"errors"
)

type approverPivotUsecase struct {
	approverRepo interfaces.ApproverPivotRepository
}

func NewApproverPivotUsecase(approverRepo interfaces.ApproverPivotRepository) *approverPivotUsecase {
	return &approverPivotUsecase{approverRepo}
}

func (ApproverPivotUsecase *approverPivotUsecase) GetAll() (data []ApproverPivot.ApproverPivot, err error) {
	data, err = ApproverPivotUsecase.approverRepo.FindAll()
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ApproverPivotUsecase *approverPivotUsecase) GetById(id uint) (data *ApproverPivot.ApproverPivot, err error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	data, err = ApproverPivotUsecase.approverRepo.FindById(id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ApproverPivotUsecase *approverPivotUsecase) GetAllApproverByGroupId(groupId uint) (data []ApproverPivot.ApproverPivot, err error) {
	if groupId < 1 {
		return nil, errors.New(Const.ID_GET_APPROVAL_BY_GROUP_ID)
	}
	data, err = ApproverPivotUsecase.approverRepo.FindAllApproverByGroupId(groupId)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ApproverPivotUsecase *approverPivotUsecase) CreateApproverPivot(Req *ApproverPivot.ApproverPivotCreate) (data *ApproverPivot.ApproverPivotCreate, err error) {
	data, err = ApproverPivotUsecase.approverRepo.StoreOne(Req)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ApproverPivotUsecase *approverPivotUsecase) UpdateManyApproverPivot(data *ApproverPivot.ApproverPivotUpdate) (result *ApproverPivot.ApproverPivotUpdate, err error) {
	result, err = ApproverPivotUsecase.approverRepo.UpdateMany(data)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (ApproverPivotUseCase *approverPivotUsecase) DeleteManyApproverPivot(data []ApproverPivot.ApproverPivotDelete) (bool, error) {
	result, err := ApproverPivotUseCase.approverRepo.DestroyMany(data)
	return result, err
}
