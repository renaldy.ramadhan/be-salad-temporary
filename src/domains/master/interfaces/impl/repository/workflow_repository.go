package repository

import (
	Workflow "BE-Epson/domains/master/entities"
	"strconv"

	"gorm.io/gorm"
)

type workflowRepository struct {
	DB *gorm.DB
}

func NewWorkflowRepository(DB *gorm.DB) *workflowRepository {
	return &workflowRepository{DB}
}

func (workflowRepo *workflowRepository) FindAll() (data []Workflow.Workflow, err error) {
	err = workflowRepo.DB.Find(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (workflowRepo *workflowRepository) FindOne(id uint) (*Workflow.Workflow, error) {
	data := &Workflow.Workflow{}
	err := workflowRepo.DB.Find(&data, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (workflowRepo *workflowRepository) FindWorkflowIdByType(types string) (uint, error) {
	data := &Workflow.Workflow{}
	err := workflowRepo.DB.Find(&data, "type = ?", types).Error
	if err != nil {
		return 0, err
	}
	return data.Model.ID, nil
}

func (workflowRepo *workflowRepository) Create(data *Workflow.Workflow) (*Workflow.Workflow, error) {
	err := workflowRepo.DB.Create(&data).Error
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (workflowRepo *workflowRepository) Update(data *Workflow.Workflow, param string) (*Workflow.Workflow, error) {
	id, _ := strconv.ParseInt(param, 10, 32)
	body := &Workflow.Workflow{
		Model: gorm.Model{ID: uint(id)},
		Type:  data.Type,
		Notes: data.Notes,
	}

	err := workflowRepo.DB.Where("id = ?", id).Updates(body).Error
	if err != nil {
		return nil, err
	}

	return body, nil
}

func (workflowRepo *workflowRepository) Delete(param string) (bool, error) {
	model := &Workflow.Workflow{}
	err := workflowRepo.DB.Where("id = ?", param).Delete(model).Error
	if err != nil {
		return false, err
	}

	return true, nil
}
