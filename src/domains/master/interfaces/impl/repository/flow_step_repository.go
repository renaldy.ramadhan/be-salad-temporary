package repository

import (
	FlowStep "BE-Epson/domains/master/entities"
	"BE-Epson/shared/models/requests/Part"
	"gorm.io/gorm"
)

type flowStepRepository struct {
	DB *gorm.DB
}

func NewFlowStepRepository(DB *gorm.DB) *flowStepRepository {
	return &flowStepRepository{DB}
}

func (flowStepRepository *flowStepRepository) buildQuery(query *gorm.DB, req *Part.FlowStepGet) *gorm.DB {
	if req.IdWorkflow != 0 {
		query = query.Where("id_workflow = ?", req.IdWorkflow)
	}

	if req.IdDepartment != 0 {
		query = query.Where("id_department = ?", req.IdDepartment)
	}

	if req.Page > 0 && req.Limit > 0 {
		query = query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit)
	}

	return query
}

func (flowStepRepo *flowStepRepository) FindAll(req *Part.FlowStepGet) (data []FlowStep.FlowStep, totalData int64, err error) {
	query := flowStepRepo.buildQuery(flowStepRepo.DB, req)
	err = query.Preload("Department").Preload("Workflow").Find(&data).Error
	//que := query.Preload("Department").Find(&data)
	//fmt.Println(que)
	if err != nil {
		return nil, totalData, err
	}
	return data, totalData, nil
}

func (flowStepRepo *flowStepRepository) FindOne(id uint) (data *FlowStep.FlowStep, err error) {
	err = flowStepRepo.DB.Find(&data, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (flowStepRepo *flowStepRepository) FindAllByWorkFlow(id uint) (data []FlowStep.FlowStep, totalData int64, err error) {
	err = flowStepRepo.DB.Find(&data, "id_workflow = ?", id).Count(&totalData).Error
	if err != nil {
		return nil, totalData, err
	}
	return data, totalData, nil
}

func (flowStepRepository *flowStepRepository) Create(data *FlowStep.FlowStepPayload) (*FlowStep.FlowStepPayload, error) {
	body := &FlowStep.FlowStep{
		IdWorkflow:   data.IdWorkflow,
		IdDepartment: data.IdDepartment,
		FlowSequence: data.FlowSequence,
		StepName:     data.StepName,
		Type:         data.Type,
	}
	err := flowStepRepository.DB.Create(body).Error
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (flowStepRepository *flowStepRepository) Update(data *FlowStep.FlowStepPayload, param string) (*FlowStep.FlowStep, error) {
	body := &FlowStep.FlowStep{
		IdWorkflow:   data.IdWorkflow,
		IdDepartment: data.IdDepartment,
		FlowSequence: data.FlowSequence,
		StepName:     data.StepName,
		Type:         data.Type,
	}
	err := flowStepRepository.DB.Where("id = ?", param).Updates(body).Error
	if err != nil {
		return nil, err
	}
	return body, nil
}

func (flowStepRepository *flowStepRepository) Destroy(param string) (bool, error) {
	model := &FlowStep.FlowStep{}
	err := flowStepRepository.DB.Where("id = ?", param).Delete(model).Error
	if err != nil {
		return false, err
	}

	return true, nil
}
