package repository

import (
	ApproverGroup "BE-Epson/domains/master/entities"
	"BE-Epson/shared/models/requests/master"
	"gorm.io/gorm"
)

type approverGroupRepo struct {
	DB *gorm.DB
}

func NewApproverGroupRepo(DB *gorm.DB) *approverGroupRepo {
	return &approverGroupRepo{DB}
}

func (approverGroupRepo *approverGroupRepo) buildQuery(query *gorm.DB, req *master.ApprovalSearchByDepartment) *gorm.DB {
	if req.DepartmentId >= 1 {
		query = query.Where("id_department = ?", req.DepartmentId)
	}
	if req.PurchasingGroupCode != "" {
		query = query.Where("purchasing_group_code_fk = ?", req.PurchasingGroupCode)
	}
	if req.GroupType != "" {
		query = query.Where("approver_group_type = ?", req.GroupType)
	}

	if req.Page > 0 && req.Limit > 0 {
		query = query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit)
	}

	if req.Search != "" {
		query = query.Where("approver_group_name = ?", req.Search)
	}

	return query
}

func (approverGroupRepo *approverGroupRepo) FindAll(req *master.ApprovalSearchByDepartment) (data []ApproverGroup.ApproverGroup, err error) {
	query := approverGroupRepo.buildQuery(approverGroupRepo.DB, req)

	err = query.Preload("Department").Find(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupRepo *approverGroupRepo) FindListApproverGroupByType(groupType string) (data []ApproverGroup.ApproverGroup, err error) {
	err = approverGroupRepo.DB.Find(&data, "group_type = ? AND id_department != ?", groupType, 1).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupRepo *approverGroupRepo) FindAllApproverFromPivot(Req []ApproverGroup.ApproverPivot) (data []ApproverGroup.Approver, err error) {
	err = approverGroupRepo.DB.Find(&data, Req).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupRepo *approverGroupRepo) FindAllApproverPivotFromGroup(Req []ApproverGroup.ApproverGroup) (data []ApproverGroup.ApproverPivot, err error) {
	err = approverGroupRepo.DB.Find(&data, Req).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupRepo *approverGroupRepo) FindOne(id uint) (*ApproverGroup.ApproverGroup, error) {
	data := &ApproverGroup.ApproverGroup{}
	err := approverGroupRepo.DB.Find(&data, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupRepo *approverGroupRepo) GenerateApprovalGroupData(id uint) ([]ApproverGroup.ApproverGroup, error) {
	var data []ApproverGroup.ApproverGroup
	err := approverGroupRepo.DB.Find(&data, id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupRepo *approverGroupRepo) UpdateOne(data *ApproverGroup.ApproverGroup, param string) (*ApproverGroup.ApproverGroup, error) {
	var model *ApproverGroup.ApproverGroup
	body := map[string]interface{}{}

	if data.ApproverGroupType == "Location" {
		body["PurchasingGroupCodeFk"] = gorm.Expr("NULL")
		body["LocationCode"] = data.LocationCode
	} else {
		body["LocationCode"] = gorm.Expr("NULL")
		body["PurchasingGroupCodeFk"] = data.PurchasingGroupCodeFk
	}

	body["IdDepartment"] = data.IdDepartment
	body["ApproverGroupType"] = data.ApproverGroupType
	body["ApproverGroupName"] = data.ApproverGroupName
	body["GroupType"] = data.GroupType

	err := approverGroupRepo.DB.Model(model).Where("id = ?", param).Updates(&body).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupRepo *approverGroupRepo) FindAllApproverGroupByDeptIdAndPurchasingGroup(id uint, pg string, groupType string) (data *ApproverGroup.ApproverGroup, err error) {
	err = approverGroupRepo.DB.Find(&data, "id_department = ? and purchasing_group_code_fk = ? and group_type = ?", id, pg, groupType).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupRepo *approverGroupRepo) StoreOne(data *ApproverGroup.ApproverGroup) (*ApproverGroup.ApproverGroup, error) {
	err := approverGroupRepo.DB.Create(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupRepo *approverGroupRepo) FindAllApproverGroupByDeptIdAndLocationId(id uint, locationId string, groupType string) (data *ApproverGroup.ApproverGroup, err error) {
	err = approverGroupRepo.DB.Find(&data, "id_department = ? and location_code = ? and group_type = ?", id, locationId, groupType).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverGroupRepo *approverGroupRepo) DestroyOne(param string) (bool, error) {
	model := &ApproverGroup.ApproverGroup{}
	err := approverGroupRepo.DB.Where("id = ?", param).Delete(model).Error
	if err != nil {
		return false, err
	}
	return true, nil
}
