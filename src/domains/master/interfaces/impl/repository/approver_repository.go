package repository

import (
	Approver "BE-Epson/domains/master/entities"
	"BE-Epson/shared/models/requests/master"
	"gorm.io/gorm"
)

type approverRepo struct {
	DB *gorm.DB
}

func NewApproverRepo(DB *gorm.DB) *approverRepo {
	return &approverRepo{DB}
}

func (approverRepo *approverRepo) buildQuery(query *gorm.DB, req *master.ApprovalSearchByDepartment) *gorm.DB {
	if req.Search != "" {
		search := "%" + req.Search
		query = query.Where("department_name LIKE ?", search)
	}

	if req.Page > 0 && req.Limit > 0 {
		query = query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit)
	}

	return query
}

func (approverRepo *approverRepo) FindAll(req *master.ApprovalSearchByDepartment) (data []Approver.Approver, err error) {
	query := approverRepo.buildQuery(approverRepo.DB, req)
	err = query.Preload("ApproverGroup").Find(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverRepo *approverRepo) FindOne(id uint) (*Approver.Approver, error) {
	data := &Approver.Approver{}
	err := approverRepo.DB.Preload("ApproverGroup").First(&data, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (approverRepo *approverRepo) FindApproverByGroupId(groupId uint) ([]Approver.Approver, error) {
	data := []Approver.Approver{}
	err := approverRepo.DB.Preload("ApproverGroup").Joins("JOIN approver_pivots ON approvers.id = approver_pivots.id_approver").Where("approver_pivots.id_approver_group = ?", groupId).Find(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil

}

func (approverRepo *approverRepo) StoreOne(data *Approver.ApproverPayload) (*Approver.Approver, error) {
	approver := &Approver.Approver{
		Nik:           data.Nik,
		ApprovalStage: data.ApprovalStage,
		EmployeeName:  data.EmployeeName,
		Sequence:      data.Sequence,
	}

	tx := approverRepo.DB.Begin()

	if err := tx.Create(&approver).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	for _, idAppvGroup := range data.IdApproverGroup {
		pivot := Approver.ApproverPivot{
			IdApprover:      approver.ID,
			IdApproverGroup: idAppvGroup,
		}
		err := tx.Create(&pivot).Error
		if err != nil {
			tx.Rollback()
			return nil, err
		}
	}

	if err := tx.Commit().Error; err != nil {
		return nil, err
	}

	return approver, nil
}

func (approverRepo *approverRepo) CheckDataIsApprover(data string) (bool, error) {
	var count int64
	err := approverRepo.DB.Model(&Approver.Approver{}).Where("nik = ?", data).Count(&count).Error
	if err != nil {
		return false, err
	}
	if count > 0 {
		return true, nil
	}
	return false, nil
}

func (approverRepo *approverRepo) UpdateOne(data *Approver.ApproverPayload, param string) (*Approver.Approver, error) {
	approver := &Approver.Approver{
		Nik:           data.Nik,
		ApprovalStage: data.ApprovalStage,
		EmployeeName:  data.EmployeeName,
		Sequence:      data.Sequence,
	}

	tx := approverRepo.DB.Begin()

	err := tx.Where("id = ?", param).Updates(&approver).Error

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		return nil, err
	}

	return approver, err
}

func (approverRepo *approverRepo) Destroy(param string) (bool, error) {
	approver := &Approver.Approver{}
	pivot := &Approver.ApproverPivot{}
	tx := approverRepo.DB.Begin()

	errPivot := tx.Where("id_approver = ?", param).Delete(&pivot).Error

	if errPivot != nil {
		tx.Rollback()
		return false, errPivot
	}
	err := tx.Where("id = ?", param).Delete(&approver).Error
	if err != nil {
		tx.Rollback()
		return false, err
	}

	if err := tx.Commit().Error; err != nil {
		return false, err
	}

	return true, nil
}
