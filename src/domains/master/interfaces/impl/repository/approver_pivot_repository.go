package repository

import (
	ApproverPivot "BE-Epson/domains/master/entities"
	"errors"
	"gorm.io/gorm"
)

type approverPivotRepository struct {
	DB *gorm.DB
}

func NewApproverPivotRepository(DB *gorm.DB) *approverPivotRepository {
	return &approverPivotRepository{DB}
}

func (ApproverPivotRepo *approverPivotRepository) FindAll() (data []ApproverPivot.ApproverPivot, err error) {
	err = ApproverPivotRepo.DB.Find(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ApproverPivotRepo *approverPivotRepository) FindById(id uint) (data *ApproverPivot.ApproverPivot, err error) {
	err = ApproverPivotRepo.DB.Where("id = ?", id).First(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ApproverPivotRepo *approverPivotRepository) FindAllApproverByGroupId(groupId uint) (data []ApproverPivot.ApproverPivot, err error) {
	err = ApproverPivotRepo.DB.Preload("Approver").Find(&data, "id_approver_group = ?", groupId).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ApproverPivotRepo *approverPivotRepository) StoreOne(req *ApproverPivot.ApproverPivotCreate) (datas *ApproverPivot.ApproverPivotCreate, err error) {
	data := &ApproverPivot.ApproverPivot{
		IdApprover:      req.IdApprover,
		IdApproverGroup: req.IdApproverGroup,
	}
	err = ApproverPivotRepo.DB.Create(&data).Error
	if err != nil {
		return nil, errors.New("failed to commit transaction: " + err.Error())
	}
	return req, nil
}

func (ApproverPivotRepo *approverPivotRepository) UpdateMany(data *ApproverPivot.ApproverPivotUpdate) (result *ApproverPivot.ApproverPivotUpdate, err error) {
	model := &[]ApproverPivot.ApproverPivot{}
	tx := ApproverPivotRepo.DB.Begin()

	if err := tx.Where("id_approver = ?", data.IdApprover).Find(&model).Error; err != nil {
		return nil, err
	}
	currentId := make([]uint, len(*model))
	for i, appvGroup := range *model {
		currentId[i] = appvGroup.IdApproverGroup
	}

	for _, v := range data.IdApproverGroup {
		rowExists := false
		for _, currId := range currentId {
			if v == currId {
				rowExists = true
				break
			}
		}

		if !rowExists {
			addNewApproverGroup := &ApproverPivot.ApproverPivot{
				IdApproverGroup: v,
				IdApprover:      data.IdApprover,
			}
			tx.Model(&model).Create(addNewApproverGroup)
		}
	}

	for _, existsId := range *model {
		rowToRemove := true
		for _, newId := range data.IdApproverGroup {
			if existsId.IdApproverGroup == newId {
				rowToRemove = false
				break
			}
		}

		if rowToRemove {
			tx.Where("id_approver = ? AND id_approver_group = ?", data.IdApprover, existsId.IdApproverGroup).Delete(&model)
		}
	}

	if err = tx.Commit().Error; err != nil {
		return nil, errors.New("failed to commit transaction: " + err.Error())
	}
	return data, nil
}

func (approverPivotRepository *approverPivotRepository) DestroyMany(data []ApproverPivot.ApproverPivotDelete) (bool, error) {
	tx := approverPivotRepository.DB.Begin()
	model := &ApproverPivot.ApproverPivot{}
	for _, v := range data {
		err := tx.Where("id = ?", v.Id).Delete(model).Error
		if err != nil {
			tx.Rollback()
			return false, err
		}
	}
	if err := tx.Commit().Error; err != nil {
		return false, errors.New("failed to commit transaction: " + err.Error())
	}

	return true, nil
}
