package repository

import (
	Department "BE-Epson/domains/master/entities"
	Master "BE-Epson/shared/models/requests/master"
	"gorm.io/gorm"
	"strconv"
)

type departmentRepository struct {
	DB *gorm.DB
}

func NewDepartmentRepository(DB *gorm.DB) *departmentRepository {
	return &departmentRepository{DB}
}

func (departmentRepository *departmentRepository) buildQuery(query *gorm.DB, req *Master.DepartmentQuery) *gorm.DB {
	if req.Page > 0 && req.Limit > 0 {
		query = query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit)
	}

	if req.Search != "" {
		search := "%" + req.Search
		query = query.Where("department_name LIKE ?", search)
	}

	return query
}

func (departmentRepo *departmentRepository) FindAll(req *Master.DepartmentQuery) (data []Department.Department, totalData int64, err error) {
	query := departmentRepo.buildQuery(departmentRepo.DB, req)
	err = query.Preload("StagingPlant").Find(&data).Error
	if err != nil {
		return nil, totalData, err
	}
	return data, totalData, nil
}

func (departmentRepo *departmentRepository) FindOne(id uint) (data *Department.Department, err error) {
	err = departmentRepo.DB.Find(&data, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (departmentRepository *departmentRepository) Store(data *Department.DepartmentCreate) (*Department.DepartmentCreate, error) {
	body := &Department.Department{
		DepartmentName: data.DepartmentName,
		LocationCodeFk: data.LocationCodeFk,
	}
	err := departmentRepository.DB.Create(&body).Error
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (departmentRepository *departmentRepository) Update(data *Department.DepartmentCreate, param string) (*Department.DepartmentCreate, error) {
	id, errParse := strconv.ParseInt(param, 10, 32)
	if errParse != nil {
		return nil, errParse
	}

	body := &Department.Department{
		Model: gorm.Model{
			ID: uint(id),
		},
		DepartmentName: data.DepartmentName,
		LocationCodeFk: data.LocationCodeFk,
	}

	err := departmentRepository.DB.Where("id = ?", id).Updates(body).Error
	if err != nil {
		return nil, err
	}

	return data, err
}

func (departmentRepository *departmentRepository) Destroy(param string) (bool, error) {
	model := &Department.Department{}
	err := departmentRepository.DB.Where("id = ?", param).Delete(model).Error
	if err != nil {
		return false, err
	}

	return true, nil
}
