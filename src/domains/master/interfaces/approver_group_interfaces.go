package interfaces

import (
	ApproverGroup "BE-Epson/domains/master/entities"
	"BE-Epson/shared/models/requests/master"
)

type ApproverGroupRepository interface {
	FindAll(department *master.ApprovalSearchByDepartment) (data []ApproverGroup.ApproverGroup, err error)
	FindListApproverGroupByType(groupType string) (data []ApproverGroup.ApproverGroup, err error)
	FindOne(id uint) (*ApproverGroup.ApproverGroup, error)
	FindAllApproverGroupByDeptIdAndPurchasingGroup(id uint, pg string, groupType string) (data *ApproverGroup.ApproverGroup, err error)
	FindAllApproverGroupByDeptIdAndLocationId(id uint, locationId string, groupType string) (data *ApproverGroup.ApproverGroup, err error)
	GenerateApprovalGroupData(id uint) ([]ApproverGroup.ApproverGroup, error)
	StoreOne(data *ApproverGroup.ApproverGroup) (*ApproverGroup.ApproverGroup, error)
	FindAllApproverFromPivot(Req []ApproverGroup.ApproverPivot) (data []ApproverGroup.Approver, err error)
	FindAllApproverPivotFromGroup(Req []ApproverGroup.ApproverGroup) (data []ApproverGroup.ApproverPivot, err error)
	UpdateOne(data *ApproverGroup.ApproverGroup, param string) (*ApproverGroup.ApproverGroup, error)
	DestroyOne(param string) (bool, error)
}

type ApproverGroupUseCase interface {
	GetAllApproverGroup(department *master.ApprovalSearchByDepartment) ([]ApproverGroup.ApproverGroup, error)
	GetListApproverGroupByType(groupType string) ([]ApproverGroup.ApproverGroup, error)
	GetAllApproverGroupDataApprover(department *master.ApprovalSearchByDepartment) ([]ApproverGroup.Approver, error)
	GetApproverGroupById(Id uint) (*ApproverGroup.ApproverGroup, error)
	GetAllApproverGroupByDeptIdAndPurchasingGroup(id uint, pg string, groupType string) (data *ApproverGroup.ApproverGroup, err error)
	GenerateApprovalGroupData(step *ApproverGroup.FlowStep) ([]ApproverGroup.ApproverGroup, error)
	CreateApprovalGroup(step *ApproverGroup.ApproverGroup) (*ApproverGroup.ApproverGroup, error)
	GetAllApproverGroupByDeptIdAndLocationId(id uint, locationId string, groupType string) (data *ApproverGroup.ApproverGroup, err error)
	UpdateApproverGroup(data *ApproverGroup.ApproverGroup, param string) (*ApproverGroup.ApproverGroup, error)
	DeleteApproverGroup(param string) (bool, error)
}
