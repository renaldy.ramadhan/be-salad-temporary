package interfaces

import (
	Approver "BE-Epson/domains/master/entities"
	ApprovalRequest "BE-Epson/shared/models/requests/master"
)

type ApproverRepository interface {
	FindAll(Req *ApprovalRequest.ApprovalSearchByDepartment) (data []Approver.Approver, err error)
	FindOne(id uint) (*Approver.Approver, error)
	FindApproverByGroupId(groupId uint) ([]Approver.Approver, error)
	CheckDataIsApprover(data string) (bool, error)
	StoreOne(data *Approver.ApproverPayload) (*Approver.Approver, error)
	UpdateOne(data *Approver.ApproverPayload, param string) (*Approver.Approver, error)
	Destroy(param string) (bool, error)
}

type ApproverUseCase interface {
	GetAllApprover(dataReq *ApprovalRequest.ApprovalSearchByDepartment) ([]Approver.Approver, error)
	GetApproverById(Id uint) (*Approver.Approver, error)
	GetApproverByGroupId(groupId uint) ([]Approver.Approver, error)
	CheckDataIsApprover(data string) (bool, error)
	CreateOne(data *Approver.ApproverPayload) (*Approver.Approver, error)
	UpdateApprover(data *Approver.ApproverPayload, param string) (*Approver.Approver, error)
	DeleteApprover(param string) (bool, error)
}
