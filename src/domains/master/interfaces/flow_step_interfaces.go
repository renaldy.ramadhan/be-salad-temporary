package interfaces

import (
	FlowStep "BE-Epson/domains/master/entities"
	"BE-Epson/shared/models/requests/Part"
)

type FlowStepRepository interface {
	FindAll(req *Part.FlowStepGet) (data []FlowStep.FlowStep, totalData int64, err error)
	FindOne(id uint) (data *FlowStep.FlowStep, err error)
	FindAllByWorkFlow(id uint) (data []FlowStep.FlowStep, totalData int64, err error)
	Create(data *FlowStep.FlowStepPayload) (*FlowStep.FlowStepPayload, error)
	Update(data *FlowStep.FlowStepPayload, param string) (*FlowStep.FlowStep, error)
	Destroy(param string) (bool, error)
}

type FlowStepUseCase interface {
	GetAllFlowStep(Req *Part.FlowStepGet) ([]FlowStep.FlowStep, error)
	GetOneFlowStep(id uint) (*FlowStep.FlowStep, error)
	GetAllFlowStepByWorkflowById(id uint) ([]FlowStep.FlowStep, error)
	StoreFlowStep(data *FlowStep.FlowStepPayload) (*FlowStep.FlowStepPayload, error)
	UpdateFlowStep(data *FlowStep.FlowStepPayload, param string) (*FlowStep.FlowStep, error)
	DeleteFlowStep(param string) (bool, error)
}
