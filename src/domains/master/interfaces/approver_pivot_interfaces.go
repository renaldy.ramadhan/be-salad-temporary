package interfaces

import (
	ApproverPivot "BE-Epson/domains/master/entities"
)

type ApproverPivotRepository interface {
	FindAll() (data []ApproverPivot.ApproverPivot, err error)
	FindById(id uint) (data *ApproverPivot.ApproverPivot, err error)
	FindAllApproverByGroupId(groupId uint) (data []ApproverPivot.ApproverPivot, err error)
	StoreOne(data *ApproverPivot.ApproverPivotCreate) (*ApproverPivot.ApproverPivotCreate, error)
	UpdateMany(data *ApproverPivot.ApproverPivotUpdate) (result *ApproverPivot.ApproverPivotUpdate, err error)
	DestroyMany(data []ApproverPivot.ApproverPivotDelete) (bool, error)
}

type ApproverPivotUsecase interface {
	GetAll() (data []ApproverPivot.ApproverPivot, err error)
	GetById(id uint) (data *ApproverPivot.ApproverPivot, err error)
	GetAllApproverByGroupId(groupId uint) (data []ApproverPivot.ApproverPivot, err error)
	CreateApproverPivot(data *ApproverPivot.ApproverPivotCreate) (*ApproverPivot.ApproverPivotCreate, error)
	UpdateManyApproverPivot(data *ApproverPivot.ApproverPivotUpdate) (result *ApproverPivot.ApproverPivotUpdate, err error)
	DeleteManyApproverPivot(data []ApproverPivot.ApproverPivotDelete) (bool, error)
}
