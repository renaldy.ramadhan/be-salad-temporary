package entities

import (
	"time"
)

type Approver struct {
	ID            uint            `gorm:"primaryKey"`
	Nik           string          `gorm:"not null" json:"Nik" binding:"required"`
	EmployeeName  string          `gorm:"nullable" json:"EmployeeName" binding:"required"`
	Sequence      uint            `gorm:"not null" json:"Sequence" binding:"required"`
	DateApproval  *time.Time      `gorm:"" json:"DateApproval"`
	ApprovalStage *uint8          `json:"ApprovalStage"`
	ApproverGroup []ApproverGroup `gorm:"many2many:approver_pivots;joinForeignKey:IdApprover;joinReferences:IdApproverGroup"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
}

type ApproverPayload struct {
	Nik             string     `binding:"required" json:"Nik"`
	EmployeeName    string     `binding:"required" json:"EmployeeName"`
	Sequence        uint       `binding:"required" json:"Sequence"`
	IdApproverGroup []uint     `json:"IdApproverGroup"`
	DateApproval    *time.Time `json:"DateApproval"`
	ApprovalStage   *uint8     `json:"ApprovalStage"`
}
