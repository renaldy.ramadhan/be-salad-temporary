package entities

import "gorm.io/gorm"

type Workflow struct {
	gorm.Model
	Type  string `gorm:"type:varchar(255);not null;unique" json:"Type" binding:"required,oneof='Request' 'Ringi' 'Disposal' 'Discrepancy' 'Disposal Event' 'Disposal Event Ringi'"`
	Notes string `gorm:"type:varchar(255);" json:"Notes"`
}
