package entities

import (
	StagingPurchasingGroup "BE-Epson/domains/staging/entities"
	"gorm.io/gorm"
)

type ApproverGroup struct {
	gorm.Model
	IdDepartment           uint                                          `gorm:"not null" json:"IdDepartment" binding:"required"`
	PurchasingGroupCodeFk  *string                                       `json:"PurchasingGroupCodeFk"`
	LocationCode           *string                                       `gorm:"type:varchar(255);" json:"LocationCode"`
	ApproverGroupType      string                                        `gorm:"type:varchar(255);not null;" json:"ApproverGroupType" binding:"required,oneof='Location' 'Purchasing Group'"`
	ApproverGroupName      string                                        `gorm:"type:varchar(255);not null;" json:"ApproverGroupName" binding:"required"`
	GroupType              string                                        `gorm:"type:varchar(255);not null;" json:"GroupType" binding:"required,oneof='Request' 'Ringi' 'Discrepancy' 'Disposal' 'Disposal Event' 'Disposal Event Ringi'"`
	Department             Department                                    `gorm:"foreignKey:IdDepartment;" json:"Department"`
	StagingPurchasingGroup StagingPurchasingGroup.StagingPurchasingGroup `gorm:"foreignKey:PurchasingGroupCodeFk;references:PurchasingGroupCode;" json:"-"`
}
