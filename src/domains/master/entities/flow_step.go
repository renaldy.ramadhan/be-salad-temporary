package entities

import (
	"BE-Epson/domains/part/presistence"
	"gorm.io/gorm"
)

type FlowStep struct {
	gorm.Model
	IdWorkflow   uint                       `gorm:"not null" json:"IdWorkflow" binding:"required"`
	IdDepartment *uint                      `json:"IdDepartment"`
	FlowSequence uint                       `json:"FlowSequence" binding:"required,numeric,min=1"`
	StepName     string                     `gorm:"type:varchar(255);not null" json:"StepName" binding:"required"`
	Type         presistence.DepartmentType `json:"Type"`
	Department   Department                 `gorm:"foreignKey:IdDepartment;" json:"Department"`
	Workflow     Workflow                   `gorm:"foreignKey:IdWorkflow;" json:"Workflow"`
}

type FlowStepPayload struct {
	IdWorkflow   uint                       `json:"IdWorkflow" binding:"required"`
	IdDepartment *uint                      `json:"IdDepartment" binding:"required"`
	FlowSequence uint                       `json:"FlowSequence" binding:"required,numeric,min=1"`
	StepName     string                     `json:"StepName" binding:"required"`
	Type         presistence.DepartmentType `json:"Type"`
}
