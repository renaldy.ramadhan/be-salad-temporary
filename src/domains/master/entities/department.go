package entities

import (
	StagingPlant "BE-Epson/domains/staging/entities"
	"gorm.io/gorm"
)

type Department struct {
	gorm.Model
	DepartmentName string                    `gorm:"type:varchar(255);not null;" json:"DepartmentName"`
	LocationCodeFk string                    `gorm:"type:varchar(255)" json:"LocationCode"`
	StagingPlant   StagingPlant.StagingPlant `gorm:"foreignKey:LocationCodeFk;references:LocationCode" json:"StagingPlant"`
}

type DepartmentCreate struct {
	DepartmentName string `gorm:"not null" json:"DepartmentName" binding:"required"`
	LocationCodeFk string `gorm:"not null" json:"LocationCodeFk"`
}

type DepartmentQuery struct {
}
