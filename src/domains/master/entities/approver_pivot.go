package entities

type ApproverPivot struct {
	ID              uint          `gorm:"primaryKey"`
	IdApproverGroup uint          `gorm:"not null" json:"IdApproverGroup" binding:"required"`
	IdApprover      uint          `gorm:"not null" json:"IdApprover" binding:"required"`
	ApproverGroup   ApproverGroup `gorm:"foreignKey:IdApproverGroup;" json:"ApproverGroup"`
	Approver        Approver      `gorm:"foreignKey:IdApprover;" json:"Approver"`
}

type ApproverPivotCreate struct {
	IdApproverGroup uint `gorm:"not null" json:"IdApproverGroup" binding:"required"`
	IdApprover      uint `gorm:"not null" json:"IdApprover" binding:"required"`
}

type ApproverPivotUpdate struct {
	IdApproverGroup []uint
	IdApprover      uint
}

type ApproverPivotDelete struct {
	Id uint `json:"Id" binding:"required"`
}
