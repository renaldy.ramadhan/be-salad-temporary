package presistence

// USER
var UserValidColumns = map[string]bool{
	"name":  true,
	"email": true,
}

// ROLE
var RoleValidColums = map[string]bool{
	"role_name": true,
}

const (
	Role1 = "Admin"
	Role2 = "Manager"
	Role3 = "Employee"
)

// DEPARTMENT
var DepartmentValidColums = map[string]bool{
	"department_name": true,
}

const (
	Division1 = "Warehouse"
	Division2 = "Finance"
	Division3 = "HR"
	Division4 = "IT"
)
