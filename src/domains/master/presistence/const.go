package presistence

type LocationType uint8

const (
	LocationRequest LocationType = iota + 1
	LocationDestroy
)
