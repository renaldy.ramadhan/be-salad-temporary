package entities

import (
	"BE-Epson/domains/master/entities"
	"BE-Epson/domains/part/presistence"
	"gorm.io/gorm"
	"time"
)

type RequestApprovalHistory struct {
	gorm.Model
	IdFlowStep           uint                                            `gorm:"not null" json:"IdFlowStep"`
	IdRequest            uint                                            `gorm:"not null" json:"IdRequest"`
	IdApprover           uint                                            `gorm:"not null" json:"IdApprover"`
	DateApproval         time.Time                                       `gorm:"not null" json:"DateApproval"`
	LastApproverSequence uint                                            `gorm:"not null" json:"LastApproverSequence"`
	Action               string                                          `gorm:"not null" json:"Action"`
	Notes                string                                          `gorm:"not null" json:"Notes"`
	Status               presistence.StatusRequestApprovalListAndHistory `gorm:"default:'In Progress'" json:"Status" binding:"oneof:'In Progress','Done','Queued','Rejected'"`
	FlowStep             entities.FlowStep                               `gorm:"foreignKey:IdFlowStep;" json:"FlowStep"`
	Request              Request                                         `gorm:"foreignKey:IdRequest;" json:"Request"`
}
