package entities

import (
	StagingItem "BE-Epson/domains/staging/entities"
	"strconv"
	"strings"

	"gorm.io/gorm"
)

type ItemPivot struct {
	gorm.Model
	ItemCodeFk         string                  `gorm:"not null" json:"ItemCodeFk" binding:"required"`
	IdRequestFk        uint                    `gorm:"not null" json:"IdRequest" binding:"required"`
	RequestedQuantity  int                     `gorm:"not null" json:"RequestQuantity" binding:"required,numeric,min=1"`
	ApprovedQuantity   int                     `gorm:"not null" json:"ApprovedQuantity" binding:"ApprovedQuantity,numeric,min=1"`
	AdjustmentQuantity int                     `gorm:"not null" json:"AdjustmentQuantity" binding:"omitempty,numeric,min=1"`
	Notes              string                  `gorm:"type:varchar(255);" json:"Notes" binding:"omitempty"`
	Amount             float64                 `gorm:"type:float" json:"Amount"`
	ItemCode           StagingItem.StagingItem `gorm:"foreignKey:ItemCodeFk;references:ItemCode;" json:"Items,omitempty"`
	Request            Request                 `gorm:"foreignKey:IdRequestFk;" json:"-"`
}

type ItemPivotCreate struct {
	ItemCodeFk         string `json:"ItemCode"`
	IdRequestFk        uint   `json:"IdRequestFk"`
	RequestedQuantity  int    `json:"RequestQuantity"`
	ApprovedQuantity   int    `json:"ApprovedQuantity"`
	AdjustmentQuantity int    `json:"AdjustmentQuantity"`
	Notes              string `json:"Notes"`
	Amount             float64
	PricePerUnit       string `json:"PricePerUnit"`
}

type ItemStagingPivot struct {
	ItemCodeFk            string
	IdRequestFk           uint
	RequestedQuantity     int
	ApprovedQuantity      int
	AdjustmentQuantity    int
	Notes                 string
	Amount                float64
	ItemCode              string
	ItemName              string
	GrossWeight           string
	WeightUnit            string
	NetWeight             string
	UnitOfMeasure         string
	PricePerUnit          string
	PurchasingGroupCodeFk string
	SystemQuantity        uint
}

//func (ItemPivot *ItemPivot) BeforeCreate(tx *gorm.DB) (err error) {
//	var itemStagingPivot
//	// Load the associated StagingItem
//	if err = tx.Model(ItemPivot).Association("Request").Find(&ItemPivot.Request); err != nil {
//		return err
//	}
//
//	if err = tx.Model(ItemPivot).Where("id_request_fk = ?", ItemPivot.Request.ID).Joins("JOIN staging_items ON staging_items.item_code = item_pivots.item_code_fk").Select("item_pivots.item_code_fk, item_pivots.id_request_fk, item_pivots.requested_quantity, item_pivots.approved_quantity, item_pivots.adjustment_quantity, item_pivots.notes, item_pivots.amount, staging_items.item_name, staging_items.price_per_unit").Find(&ItemPivot.ItemCode).Error; err != nil {
//		return err
//	}
//
//	qty := ItemPivot.RequestedQuantity
//	if ItemPivot.ApprovedQuantity > 0 {
//		qty = ItemPivot.ApprovedQuantity
//	}
//
//	if ItemPivot.Request.PurchasingGroupCodeFk != nil {
//		if *ItemPivot.Request.PurchasingGroupCodeFk != ItemPivot.ItemCode.PurchasingGroupCodeFk {
//			return errors.New("Purchasing Group Item Is Not Match With Request ! ")
//		}
//	}
//	// Calculate the Amount
//
//	priceStr := strings.Replace(strings.TrimSpace(ItemPivot.ItemCode.PricePerUnit), ",", ".", -1)
//	price, err := strconv.ParseFloat(priceStr, 64)
//	if err != nil {
//		// Handle the error if the price is not a valid float
//		return err
//	}
//
//	ItemPivot.Amount = price * float64(qty)
//	return
//}

func (ItemPivot *ItemPivot) BeforeUpdate(tx *gorm.DB) (err error) {
	// Load the associated StagingItem
	if err = tx.Model(ItemPivot).Association("ItemCode").Find(&ItemPivot.ItemCode); err != nil {
		return err
	}
	qty := ItemPivot.RequestedQuantity
	if ItemPivot.ApprovedQuantity > 0 {
		qty = ItemPivot.ApprovedQuantity
	}
	priceStr := strings.Replace(strings.TrimSpace(ItemPivot.ItemCode.PricePerUnit), ",", ".", -1)
	price, err := strconv.ParseFloat(priceStr, 64)
	if err != nil {
		// Handle the error if the price is not a valid float
		return err
	}
	// Calculate the Amount
	ItemPivot.Amount = price * float64(qty)
	return
}

//func (ItemPivot *ItemPivot) BeforeFind (tx *gorm.DB) (err error) {
//	if err = tx.Model(ItemPivot).Association("ItemCode").Find(&ItemPivot.ItemCode); err != nil {
//		return err
//	}
//	qty := ItemPivot.RequestedQuantity
//	if ItemPivot.ApprovedQuantity > 0 {
//		qty = ItemPivot.ApprovedQuantity
//	} else if ItemPivot.AdjustmentQuantity > 0 {
//		qty = ItemPivot.AdjustmentQuantity
//	}
//	// Calculate the Amount
//
//	price := cast.ToFloat64(strings.TrimSpace(ItemPivot.ItemCode.PricePerUnit))
//
//	ItemPivot.Amount = price * float64(qty)
//	return
//}
