package entities

import (
	DisposalGroup "BE-Epson/domains/event/entities"
	"BE-Epson/domains/master/entities"
	"BE-Epson/domains/part/presistence"
	"time"

	"gorm.io/gorm"
)

type ActiveRequestApprovalList struct {
	gorm.Model
	IdEvent           *uint                                           `gorm:"" json:"IdEvent"`
	IdFlowStep        uint                                            `gorm:"not null" json:"IdFlowStep"`
	IdDepartment      uint                                            `gorm:"not null" json:"IdDepartment"`
	IdRequest         *uint                                           `gorm:"" json:"IdRequest"`
	StartDateApproval *time.Time                                      `gorm:"" json:"StartDateApproval"`
	DateApproval      *time.Time                                      `gorm:"" json:"DateApproval"`
	DueDateApproval   *time.Time                                      `gorm:"" json:"DueDateApproval"`
	ApprovalSequence  uint                                            `gorm:"not null" json:"ApprovalSequence"`
	Notes             string                                          `gorm:"type:varchar(255);" json:"Notes"`
	ApproverNik       string                                          `gorm:"type:varchar(255);" json:"ApproverNik"`
	ApproverEmail     *string                                         `gorm:"type:varchar(255);" json:"ApproverEmail"`
	Status            presistence.StatusRequestApprovalListAndHistory `gorm:"default:Queued;type:varchar(255);not null;" json:"Status,omitempty" binding:"oneof='Done','In Progress','Queued','Rejected'"`
	FlowStep          entities.FlowStep                               `gorm:"foreignKey:IdFlowStep;" json:"-"`
	Department        entities.Department                             `gorm:"foreignKey:IdDepartment;" json:"Department"`
	Request           Request                                         `gorm:"foreignKey:IdRequest;" json:"Request"`
	Event             DisposalGroup.DisposalGroup                     `gorm:"foreignKey:IdEvent;" json:"Event"`
}

type ActiveApprovalListParams struct {
	Page     int    `json:"page"`
	PageSize int    `json:"pageSize"`
	Type     string `json:"type"`
}
