package entities

import (
	"BE-Epson/domains/part/presistence"
	"gorm.io/gorm"
	"time"
)

type Sequence struct {
	gorm.Model
	SequenceLevel uint                    `gorm:"column:sequence_level;not null" json:"SequenceLevel"`
	Type          presistence.RequestType `gorm:"column:type;not null" json:"Type" binding:"required,oneof=Disposal Request Ringi Discrepancy"`
	NextResetDate time.Time               `gorm:"column:next_reset_date;not null;format('dd/mm/yyyy')" json:"NextResetDate"`
}

func (Sequence *Sequence) BeforeUpdate(tx *gorm.DB) (err error) {
	Sequence.NextResetDate = time.Now().AddDate(0, 1, 0)
	return
}
