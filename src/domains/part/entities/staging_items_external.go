package entities

type StagingItemExternal struct {
	ItemCode       string
	SystemQuantity uint
	LocationCode   string
}
