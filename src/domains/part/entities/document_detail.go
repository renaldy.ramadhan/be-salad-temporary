package entities

import (
	"gorm.io/gorm"
)

type DocumentDetail struct {
	gorm.Model
	IdRequest          uint    `gorm:"not null" json:"IdRequest" binding:"required,gte=1,numeric"`
	PathDestination    string  `gorm:"not null" json:"PathDestination" binding:"required"`
	DocumentRealName   string  `gorm:"not null" json:"DocumentRealName" binding:"required"`
	DocumentNotes      string  `gorm:"not null" json:"DocumentNotes" binding:"required,omitempty"`
	DocumentExtensions string  `gorm:"not null" json:"DocumentExtensions" binding:"required,oneof='.csv' '.xls' '.xlsx' '.pdf'"`
	Request            Request `gorm:"foreignKey:IdRequest;" json:"Request"`
}
