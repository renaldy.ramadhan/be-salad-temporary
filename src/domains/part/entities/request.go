package entities

import (
	"BE-Epson/domains/event/entities"
	"BE-Epson/domains/part/presistence"
	StagingPurchasingGroup "BE-Epson/domains/staging/entities"
	"gorm.io/gorm"
	"time"
)

type Request struct {
	gorm.Model
	UserName               string                                        `gorm:"not null" json:"UserName" binding:"required"`
	IdReason               uint                                          `gorm:"not null;" json:"IdReason"`
	PurchasingGroupCodeFk  *string                                       `gorm:"type:varchar(255)" json:"PurchasingGroupCodeFk"`
	NikRequest             string                                        `gorm:"not null;default:'10903794'" json:"NikRequest"`
	LineCode               string                                        `gorm:"not null;default:'08AY0101'" json:"LineCode"`
	LocationCodeFk         string                                        `gorm:"type:varchar(255);not null;" json:"LocationCode"`
	PartRequestCode        string                                        `gorm:"not null;default:'PR2501001'" json:"PartRequestCode"`
	IdParent               *uint                                         `json:"IdParent,omitempty"`
	IdDisposalGroupEventFk *uint                                         `json:"IdDisposalGroupEvent,omitempty"`
	Type                   presistence.RequestType                       `gorm:"not null" json:"Type" binding:"required,oneof='Request','Ringi','Disposal','Discrepancy'"`
	RequestDate            time.Time                                     `gorm:"not null" json:"RequestDate"`
	DueDate                time.Time                                     `gorm:"not null" json:"DueDate" `
	PlantName              string                                        `gorm:"not null" json:"PlantName"`
	RequestLocation        string                                        `gorm:"not null" json:"RequestLocation"`
	DestroyLocation        string                                        `json:"DestroyLocation"`
	WarehouseLocation      *string                                       `json:"WarehouseLocation"`
	Status                 presistence.StatusRequest                     `gorm:"default:'New'" json:"Status" binding:"oneof='New' 'In Progress' 'Done' 'Rejected'"`
	Notes                  string                                        `json:"Notes"`
	LastApprovalSequence   uint                                          `gorm:"default:0" json:"LastApprovalSequence"`
	DisposalGroup          *entities.DisposalGroup                       `gorm:"foreignKey:IdDisposalGroupEventFk;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"DisposalGroup,omitempty"`
	Nik                    StagingPurchasingGroup.StagingUser            `gorm:"foreignKey:NikRequest;" json:"User,omitempty"`
	Reason                 StagingPurchasingGroup.StagingReason          `gorm:"foreignKey:IdReason;" json:"Reason,omitempty"`
	PurchasingGroup        StagingPurchasingGroup.StagingPurchasingGroup `gorm:"foreignKey:PurchasingGroupCodeFk;references:PurchasingGroupCode" json:"PurchasingGroup,omitempty"`
	DisposalType           presistence.DisposalType                      `json:"DisposalType"`
	Parent                 *Request                                      `gorm:"foreignKey:IdParent;nullable" json:"Parent"`
	LocationPlant          StagingPurchasingGroup.StagingPlant           `gorm:"foreignKey:LocationCodeFk;references:LocationCode" json:"LocationPlant,omitempty"`
	ItemPivot              []ItemPivot                                   `gorm:"foreignKey:IdRequestFk;references:ID" json:"ItemPivot"`
}

func (Request *Request) AfterFind(tx *gorm.DB) (err error) {
	if err = tx.Model(&Request).Association("Reason").Find(&Request.Reason); err != nil {
		return err
	}
	if err = tx.Model(&Request).Association("PurchasingGroup").Find(&Request.PurchasingGroup); err != nil {
		return err
	}
	if err = tx.Model(&Request).Association("Nik").Find(&Request.Nik); err != nil {
		return err
	}
	if err = tx.Model(&Request).Association("LocationPlant").Find(&Request.LocationPlant); err != nil {
		return err
	}
	return
}

func (Request *Request) BeforeCreate(tx *gorm.DB) (err error) {
	if err = tx.Model(&Request).Association("Reason").Find(&Request.Reason); err != nil {
		return err
	}
	return
}

func (Request *Request) AfterCreate(tx *gorm.DB) (err error) {

	return
}
