package presistence

// PART
var ValidColumnsPart = map[string]bool{
	"id":         true,
	"part_name":  true,
	"part_code":  true,
	"stock":      true,
	"weight":     true,
	"amount":     true,
	"created_at": true,
}

const (
	Page  = 1
	Limit = 100
)
