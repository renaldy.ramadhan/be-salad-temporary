package presistence

var RequestValidColumns = map[string]bool{
	"type":             true,
	"request_date":     true,
	"due_date":         true,
	"user_name":        true,
	"request_location": true,
	"destroy_location": true,
}

type (
	StatusRequest                       string
	FlowSequence                        uint
	StatusRequestApprovalListAndHistory string
	RequestType                         string
	DisposalType                        string
	DepartmentType                      uint8
	MailType                            string
	MailMessageType                     string
)

// Type Department
const (
	Organization DepartmentType = iota + 1
	Location
	PurchasingGroup
)

// Status Request
const (
	Reject StatusRequest = "Rejected"
)

// Status Active Request Approval List
const (
	Queued StatusRequestApprovalListAndHistory = "Queued"
)
const (
	InProgress = "In Progress"
	Rejected   = "Rejected"
	Done       = "Done"
	New        = "New"
)

// Flow Sequence
const (
	IssuePartRequest FlowSequence = iota + 1
	PPC
	WHC
)

// Request Type
const (
	Request     RequestType = "Request"
	Ringi       RequestType = "Ringi"
	Disposal    RequestType = "Disposal"
	Discrepancy RequestType = "Discrepancy"
)

// Disposal Type
const (
	System    DisposalType = "System"
	NonSystem DisposalType = "Non - System"
)

// Mail Type
const (
	NeedApproval   MailType = "Approval"
	FinishApproval MailType = "Finished"
)

const (
	MessageNeedApproval   string = "need you approval"
	MessageFinishApproval string = "need your attention"
)

const (
	SubjectTypeNeedApproval string = "Request Need Your Approval"
	SubjectTypeFinish       string = "Request Approved"
	SubjectTypeReject       string = "Request Rejected"
)
