package interfaces

type DocumentPrintUsecase interface {
	GetById(id uint) ([]byte, error)
}
