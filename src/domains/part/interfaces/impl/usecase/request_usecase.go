package usecase

import (
	FlowStep "BE-Epson/domains/master/entities"
	interfaces2 "BE-Epson/domains/master/interfaces"
	Request "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/interfaces"
	"BE-Epson/domains/part/presistence"
	interfaces3 "BE-Epson/domains/staging/interfaces"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/requests/Part"
	PartResponse "BE-Epson/shared/models/responses/Part"
	"errors"
)

type RequestUseCase struct {
	RequestRepo        interfaces.RequestRepository
	staginItemExternal interfaces.GetStagingItemExternalRepo
	sequenceRepo       interfaces.SequenceRepository
	workflowRepo       interfaces2.WorkflowRepository
	flowstepRepo       interfaces2.FlowStepRepository
	organizationRepo   interfaces3.OrganizationApprovalRouteRepository
}

func NewRequestUseCase(RequestRepo interfaces.RequestRepository, stagingItemExternal interfaces.GetStagingItemExternalRepo, sequenceRepo interfaces.SequenceRepository, workflowRepo interfaces2.WorkflowRepository, flowstepRepo interfaces2.FlowStepRepository, organizationRepo interfaces3.OrganizationApprovalRouteRepository) *RequestUseCase {
	return &RequestUseCase{
		RequestRepo:        RequestRepo,
		staginItemExternal: stagingItemExternal,
		sequenceRepo:       sequenceRepo,
		workflowRepo:       workflowRepo,
		flowstepRepo:       flowstepRepo,
		organizationRepo:   organizationRepo,
	}
}

func (pc *RequestUseCase) GetAllRequest(Req *Part.SearchRequest) (Res []Request.Request, err error, count int64) {
	if Req.Type == "" {
		return nil, errors.New("Error: Type Request Required!"), 0
	} else {
		Res, err, count = pc.RequestRepo.GetAllRequest(Req)
	}
	if err != nil {
		return nil, errors.New("Error While Getting All Part Request !"), 0
	}
	return Res, nil, count
}

func (pc *RequestUseCase) GetAllRequestByNik(Req *Part.SearchRequest) (Res []Request.Request, err error) {
	if Req.Nik == "" {
		return nil, errors.New("Error: NIK Required!")
	} else {
		Res, err = pc.RequestRepo.FindAllRequestByNik(Req)
	}
	if err != nil {
		return nil, errors.New("Error While Getting All Part Request !")
	}
	return Res, nil
}

func (pc *RequestUseCase) CreateOneRequest(Req *Part.CreateRequest) (Res *PartResponse.RequestResponse, err error) {
	if Req.Nik == "" {
		return nil, errors.New(Const.NIK_EMPTY)
	}
	if Req.Type == "" {
		return nil, errors.New(Const.TYPES_EMPTY)
	} else if Req.Type == presistence.Disposal {
		if flag, checkErr := helpers.CheckDisposalGroupType(Req); flag != true || checkErr != nil {
			return nil, errors.New(checkErr.Error())
		}
		if flag, checkErr := helpers.CheckIdDisposalGroupEvent(Req, pc.RequestRepo); flag != true || checkErr != nil {
			return nil, errors.New(checkErr.Error())
		}
	}
	data, errs := pc.sequenceRepo.GetSequenceByType(Req.Type)
	if errs != nil {
		return nil, errors.New(errs.Error())
	}
	Req.PartRequestCode = helpers.GeneratePartRequestCode(data)
	Res, err = pc.RequestRepo.StoreOneRequest(Req)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	if err = pc.sequenceRepo.UpdateSequenceLevelByType(Req.Type); err != nil {
		return nil, errors.New(err.Error())
	}
	return Res, nil
}

func (pc *RequestUseCase) CreateBulkRequest(Req []Part.CreateRequest, nik string, username string) (Res []PartResponse.RequestResponse, Flowstep []FlowStep.FlowStep, err error) {
	var (
		workflowsId uint
	)
	Res, err = pc.RequestRepo.StoreBulkRequest(Req, nik, username)

	for _, responseReq := range Res {

		dataWorkflowId, err := pc.workflowRepo.FindWorkflowIdByType(helpers.ParseToString(responseReq.Type))
		if err != nil {
			return nil, nil, errors.New(err.Error())
		}
		workflowsId = dataWorkflowId
	}

	dataFlowstep, _, err := pc.flowstepRepo.FindAllByWorkFlow(workflowsId)
	if err != nil {
		return nil, nil, errors.New(err.Error())
	}

	if err != nil {
		return nil, nil, errors.New(err.Error())
	}
	return Res, dataFlowstep, nil
}

func (pc *RequestUseCase) GetOneRequest(partReqId uint) (Res *PartResponse.GetOneRequestResponse, err error) {
	if partReqId < 1 {
		return nil, errors.New(Const.ID_REQUEST_LESS_THAN_ONE)
	}
	Res, err = pc.RequestRepo.FindOneRequest(partReqId)
	if err != nil {
		return nil, errors.New("Error getting master request: " + err.Error())
	}
	//for i, item := range Res.Items {
	//	Res.Items[i].ItemCode.SystemQuantity, err = pc.staginItemExternal.FindAllSystemQuantity(item.ItemCodeFk, Res.Request.LocationCodeFk)
	//	if err != nil {
	//		return nil, errors.New("Error getting system quantity: " + err.Error())
	//	}
	//}
	Res.Message = "Success"
	return Res, nil
}

func (pc *RequestUseCase) DeleteRequest(partReqId uint) (err error) {
	err = pc.RequestRepo.DeleteRequest(partReqId)
	if err != nil {
		return errors.New("Error deleting master request: " + err.Error())
	}
	return err
}

func (pc *RequestUseCase) GetRequestByCode(partReqCode string) (Res []Request.Request, err error) {
	Res, err = pc.RequestRepo.FindRequestByCode(partReqCode)
	if err != nil {
		return nil, errors.New("Error getting master request: " + err.Error())
	}
	return Res, nil

}
