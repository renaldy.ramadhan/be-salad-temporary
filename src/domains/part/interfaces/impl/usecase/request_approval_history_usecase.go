package usecase

import (
	RequestApprovalHistory "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/interfaces"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/models/requests/master"
	"errors"
)

type requestApprovalHistoryUsecase struct {
	requestApprovalHistoryRepo interfaces.RequestApprovalHistoryRepository
}

func NewRequestApprovalHistoryUsecase(requestApprovalHistoryRepo interfaces.RequestApprovalHistoryRepository) *requestApprovalHistoryUsecase {
	return &requestApprovalHistoryUsecase{requestApprovalHistoryRepo}
}

func (requestApprovalHistoryUc *requestApprovalHistoryUsecase) GetAllRequestApprovalHistory(Req *master.RequestApprovalHistoryGet) ([]RequestApprovalHistory.RequestApprovalHistory, error) {
	requestApprovalHistorys, _, err := requestApprovalHistoryUc.requestApprovalHistoryRepo.FindAll()
	if err != nil {
		return nil, err
	}
	return requestApprovalHistorys, nil
}

func (requestApprovalHistoryUc *requestApprovalHistoryUsecase) GetOneRequestApprovalHistory(id uint) (*RequestApprovalHistory.RequestApprovalHistory, error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	requestApprovalHistory, err := requestApprovalHistoryUc.requestApprovalHistoryRepo.FindOne(id)
	if err != nil {
		return nil, err
	}
	return requestApprovalHistory, nil
}
