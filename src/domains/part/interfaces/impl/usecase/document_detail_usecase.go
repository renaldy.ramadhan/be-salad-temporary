package usecase

import (
	DocumentDetail "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/interfaces"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/models/requests/Part"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gorm.io/gorm"
)

type documentDetailUsecase struct {
	documentDetailRepository interfaces.DocumentDetailRepository
}

func NewDocumentDetailUsecase(documentDetailRepository interfaces.DocumentDetailRepository) *documentDetailUsecase {
	return &documentDetailUsecase{
		documentDetailRepository: documentDetailRepository,
	}
}

func (usecase *documentDetailUsecase) GetAll(limit int, offset int, order string) (documentDetails []DocumentDetail.DocumentDetail, err error) {
	documentDetails, err = usecase.documentDetailRepository.FindAll(limit, offset, order)
	if err != nil {
		return nil, err
	}
	return documentDetails, nil
}

func (usecase *documentDetailUsecase) GetById(id uint) (documentDetail *DocumentDetail.DocumentDetail, err error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	documentDetail, err = usecase.documentDetailRepository.FindById(id)
	if err != nil {
		return nil, err
	}
	return documentDetail, nil
}

func (usecase *documentDetailUsecase) RemoveById(id uint) (documentDetail *DocumentDetail.DocumentDetail, err error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	documentDetail, err = usecase.documentDetailRepository.DeleteById(id)
	if err != nil {
		return nil, err
	}
	return documentDetail, nil
}

func (usecase *documentDetailUsecase) CreateDocuments(documentDetail *DocumentDetail.DocumentDetail) (err error) {

	err = usecase.documentDetailRepository.StoreDocuments(documentDetail)
	if err != nil {
		return err
	}
	return nil
}

func (usecase *documentDetailUsecase) GetDocumentPDF(id uint) (documentDetail *DocumentDetail.DocumentDetail, err error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	documentDetail, err = usecase.documentDetailRepository.FindById(id)
	if err != nil {
		return nil, err
	}
	return documentDetail, nil
}

func (usecase *documentDetailUsecase) GetAllByRequestId(limit int, offset int, order string, reqId uint) (documentDetails []DocumentDetail.DocumentDetail, err error) {
	if reqId < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	documentDetails, err = usecase.documentDetailRepository.FindAllByRequestId(limit, offset, order, reqId)
	if err != nil {
		return nil, err
	}
	return documentDetails, nil
}

func (usecase *documentDetailUsecase) CreateMultipleFiles(Req *Part.CreateRequest) (err error) {
	if Req.Id < 1 {
		return errors.New(Const.ID_LESS_THAN_ONE)
	}
	documentDetails := []DocumentDetail.DocumentDetail{}
	fmt.Println("len files  >>>> ? ", len(Req.Files))
	for _, file := range Req.Files {
		documentRealname := file.Filename
		documentExtensions := strings.Split(file.Filename, ".")[1]
		pathDestination := filepath.Join("upload", strconv.Itoa(int(Req.Id)), file.Filename)
		pathDestinationDir := filepath.Join("upload", strconv.Itoa(int(Req.Id)))
		errCreateDir := os.MkdirAll(pathDestinationDir, os.ModePerm)
		if errCreateDir != nil {
			return err
		}
		f, err := os.OpenFile(pathDestination, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			return err
		}
		fmt.Println("dir >>>> ? ", pathDestinationDir)
		fileGet, _ := file.Open()
		defer fileGet.Close()
		io.Copy(f, fileGet)
		data := &DocumentDetail.DocumentDetail{
			Model:              gorm.Model{},
			IdRequest:          Req.Id,
			PathDestination:    pathDestination,
			DocumentRealName:   documentRealname,
			DocumentExtensions: documentExtensions,
		}
		documentDetails = append(documentDetails, *data)
	}
	err = usecase.documentDetailRepository.StoreBulkDocumentFiles(documentDetails)
	if err != nil {
		return err
	}
	return nil
}
