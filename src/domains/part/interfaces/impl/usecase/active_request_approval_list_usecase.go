package usecase

import (
	ActiveRequestApprovalList "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/interfaces"
	"BE-Epson/domains/part/presistence"
	interfaces2 "BE-Epson/domains/staging/interfaces"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/messages"
	"BE-Epson/shared/models/requests/Part"
	Part2 "BE-Epson/shared/models/responses/Part"
	"errors"
)

type activeRequestApprovalListUseCase struct {
	activeRequestApprovalListRepo interfaces.ActiveRequestApprovalListRepository
	sapAdjustmentRepo             interfaces2.SAPAdjustmentRepository
	requestRepo                   interfaces.RequestRepository
	stagingEmployeeCompany        interfaces2.StagingEmployeeCompanyRepository
	stagingUser                   interfaces2.StagingUserRepository
}

func NewActiveRequestApprovalListUseCase(activeRequestApprovalListRepo interfaces.ActiveRequestApprovalListRepository, sapAdjustmentRepo interfaces2.SAPAdjustmentRepository, requestRepo interfaces.RequestRepository, stagingEmployeeCompany interfaces2.StagingEmployeeCompanyRepository, stagingUser interfaces2.StagingUserRepository) *activeRequestApprovalListUseCase {
	return &activeRequestApprovalListUseCase{activeRequestApprovalListRepo, sapAdjustmentRepo, requestRepo, stagingEmployeeCompany, stagingUser}
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) GetByRequestId(id uint) ([]Part2.GetOneActiveRequestApprovalListResponse, error) {
	if id == 0 {
		return nil, errors.New(Const.ID_REQUEST_LESS_THAN_ONE)
	}
	data, err := activeRequestApprovalListUseCase.activeRequestApprovalListRepo.FindByRequestId(id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) GetByEventId(id uint) ([]Part2.GetOneActiveRequestApprovalListResponse, error) {
	if id == 0 {
		return nil, errors.New(Const.ID_REQUEST_LESS_THAN_ONE)
	}
	data, err := activeRequestApprovalListUseCase.activeRequestApprovalListRepo.FindByEventId(id)
	if err != nil {
		return nil, err
	}
	return data, nil

}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) GetAllActiveRequestApprovalList(types string) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error) {
	if types == "" {
		return nil, errors.New(Const.TYPE_IS_REQUIRED)
	}
	data, err := activeRequestApprovalListUseCase.activeRequestApprovalListRepo.FindAll(types)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) GetActiveRequestApprovalListById(Id uint) (*ActiveRequestApprovalList.ActiveRequestApprovalList, error) {
	if Id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	data, err := activeRequestApprovalListUseCase.activeRequestApprovalListRepo.FindOne(Id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) CreateOneActiveRequestApprovalList(Req *ActiveRequestApprovalList.ActiveRequestApprovalList) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error) {
	Res, err = activeRequestApprovalListUseCase.activeRequestApprovalListRepo.StoreOne(Req)
	if err != nil {
		return nil, err
	}
	return Res, nil
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) UpdateStatusActiveRequestList(Req *Part.UpdateStatusActiveRequestList) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error) {
	if Req.IdActiveRequestList < 1 {
		return nil, errors.New(Const.ID_ACTIVE_REQUEST_LIST_LESS_THAN_ONE)
	}
	if Req.Nik == "" {
		return nil, errors.New(Const.NIK_IS_REQUIRED)
	}
	Res, err = activeRequestApprovalListUseCase.activeRequestApprovalListRepo.PutStatusActiveRequestList(Req)

	requestor, _ := activeRequestApprovalListUseCase.stagingUser.FindUserByNik(Res.Request.NikRequest)

	if Res.Request.Status == presistence.Done {
		if Res.Request.DisposalType != presistence.NonSystem {
			Request, _ := activeRequestApprovalListUseCase.requestRepo.FindOneRequest(helpers.ParseToUint(Res.IdRequest))
			StagingEmployee, _ := activeRequestApprovalListUseCase.stagingEmployeeCompany.FindCostCenterByNik(Request.Request.NikRequest)
			_, _ = activeRequestApprovalListUseCase.sapAdjustmentRepo.Store(Request, StagingEmployee.CostCenter)
		}
		if requestor.Email != nil {
			var subject string
			if Res.Request.Status == presistence.Done {
				subject = presistence.SubjectTypeFinish
			} else {
				subject = presistence.SubjectTypeReject
			}
			mailPayload := &messages.MailPayload{
				ToAddress:      *requestor.Email,
				Subject:        subject,
				ApproverName:   helpers.StringPointer(requestor.Name),
				DocumentNumber: &Res.Request.PartRequestCode,
				RequestType:    Res.Request.Type,
				RequestorName:  helpers.StringPointer(requestor.Name),
				MailType:       presistence.FinishApproval,
			}
			helpers.SendMail(mailPayload)
		}
	}
	if err != nil {
		return nil, err
	}
	if Res.ApprovalSequence != Res.Request.LastApprovalSequence {
		stgUser, _ := activeRequestApprovalListUseCase.stagingUser.FindUserByNik(Res.ApproverNik)
		if stgUser.Email != nil {
			mailPayload := &messages.MailPayload{
				ToAddress:      *stgUser.Email,
				Subject:        presistence.SubjectTypeNeedApproval,
				ApproverName:   &stgUser.Name,
				DocumentNumber: &Res.Request.PartRequestCode,
				RequestType:    Res.Request.Type,
				RequestorName:  &requestor.Name,
				MailType:       presistence.NeedApproval,
			}
			helpers.SendMail(mailPayload)
		}
	}
	return Res, nil
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) UpdateStatusApprovalEvent(Req *Part.UpdateStatusEventApproval) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error) {
	Res, err = activeRequestApprovalListUseCase.activeRequestApprovalListRepo.PutStatusApprovalEvent(Req)
	if err != nil {
		return nil, err
	}
	return Res, nil

}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) GetApprovaLoginlList(nik string) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error) {
	if nik == "" {
		return nil, errors.New(Const.NIK_IS_REQUIRED)
	}
	data, err := activeRequestApprovalListUseCase.activeRequestApprovalListRepo.FindApprovalList(nik)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) GetApprovaLoginListAssigned(nik string, params *ActiveRequestApprovalList.ActiveApprovalListParams) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error, int64) {
	if nik == "" {
		return nil, errors.New(Const.NIK_IS_REQUIRED), 0
	}
	data, err, total := activeRequestApprovalListUseCase.activeRequestApprovalListRepo.FindApprovalListAssigned(nik, params)
	if err != nil {
		return nil, err, 0
	}
	return data, nil, total
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) GetAllActiveRequestApprovalListById(Id uint) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error) {
	if Id < 1 {
		return nil, errors.New(Const.ID_REQUEST_LESS_THAN_ONE)
	}
	data, err := activeRequestApprovalListUseCase.activeRequestApprovalListRepo.FindAllById(Id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) CheckIsApproverByRequestId(nik string, id uint) (bool, error) {
	if nik == "" {
		return false, errors.New(Const.NIK_IS_REQUIRED)
	}
	if id < 1 {
		return false, errors.New(Const.ID_REQUEST_LESS_THAN_ONE)
	}
	data, err := activeRequestApprovalListUseCase.activeRequestApprovalListRepo.CheckIsApproverByRequestId(nik, id)
	if err != nil {
		return false, err
	}
	return data, nil
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) CheckIsApproverByEventId(nik string, id uint, reqType string) (bool, bool, error) {
	if nik == "" {
		return false, false, errors.New(Const.NIK_IS_REQUIRED)
	}
	if id < 1 {
		return false, false, errors.New(Const.ID_REQUEST_LESS_THAN_ONE)
	}
	statusRequest, statusApprover, err := activeRequestApprovalListUseCase.activeRequestApprovalListRepo.CheckIsApproverByEventId(nik, id, reqType)
	if err != nil {
		return false, false, err
	}
	return statusRequest, statusApprover, nil
}

func (activeRequestApprovalListUseCase *activeRequestApprovalListUseCase) GetPrintData(data *ActiveRequestApprovalList.Request) (flag bool, err error) {
	if data == nil {
		return false, errors.New(Const.DATA_GET_ONE_RESPONSE_REQUET)
	}
	if data.Type == presistence.Request {
		flag, err = activeRequestApprovalListUseCase.activeRequestApprovalListRepo.CheckPrintDataWhcInRequestDivision(data.Model.ID, 3)
	} else if data.Type == presistence.Disposal {

	}
	if err != nil {
		return false, err
	}
	return flag, nil
}
