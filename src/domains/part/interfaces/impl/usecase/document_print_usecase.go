package usecase

import (
	"BE-Epson/domains/part/interfaces"
	StagingInterface "BE-Epson/domains/staging/interfaces"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/helpers"
	"bytes"
	"errors"
	"log"
	"strings"

	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

type documentPrintUsecase struct {
	itemPivotRepository           interfaces.ItemPivotRepository
	activeRequestApprovalListRepo interfaces.ActiveRequestApprovalListRepository
	stagingUserRepository         StagingInterface.StagingUserRepository
}

func NewDocumentPrintUsecase(itemPivotRepository interfaces.ItemPivotRepository, activeRequestApprovalListRepo interfaces.ActiveRequestApprovalListRepository, stagingUserRepository StagingInterface.StagingUserRepository) *documentPrintUsecase {
	return &documentPrintUsecase{
		itemPivotRepository:           itemPivotRepository,
		activeRequestApprovalListRepo: activeRequestApprovalListRepo,
		stagingUserRepository:         stagingUserRepository,
	}
}

func (usecase *documentPrintUsecase) GetById(id uint) ([]byte, error) {

	// TEMPLATE_PART_REQUEST := "<!DOCTYPE html><head><title>PART REQUEST</title><style> /* Styles for A4 landscape */ @page { size: A4 landscape; margin: 0; } body { font-family: Arial, sans-serif; margin: 0; padding: 0; height: 100vh; /* Set body height to full viewport height */ font-size: 11px; /* Reduced font size */ } /* Invoice styles */ .invoice { page-break-after: always; /* Ensure each invoice starts on a new page */ padding: 40px; /* border: 1px solid #ccc; */ box-sizing: border-box; } .company-name { font-weight: bold; margin-bottom: 5px; } .invoice-header, .invoice-table, .approval-row, .approval-table, .invoice-footer { margin-bottom: 5px; } .invoice-header { text-align: center; } .invoice-footer, .invoice-footer-date { text-align: left; } .invoice-footer-date { margin-top: 8px; /* Adjusted margin */ } .invoice-table { width: 100%; border-collapse: collapse; font-size: 9px !important; /* Reduced font size */ } .invoice-table th, .invoice-table td { border: 1px solid #ccc; /* padding: 8px; */ padding: 4px 8px; } .approval-row { display: flex; justify-content: center; align-items: center; page-break-inside:avoid; display: -webkit-box; -webkit-box-orient: horizontal; -webkit-box-pack: center; /* centers in -webkit-box automagically! */ } .approval-table { border-collapse: collapse; } .wrapper-approval{ -webkit-box-align: stretch; -webkit-box-flex: 1; -webkit-flex: 1; flex: 1; } .approval-row > div:last-child { margin-right: 0; } .approval-table th, .approval-table td { border: 1px solid #ccc; padding: 8px; vertical-align: top; } .signature-line { width: 100%; border-bottom: 1px solid #000; height: 50px; /* Adjust height to accommodate signature */ margin-bottom: 8px; } /* Added style for reason div */ .approval-remarks { border: 1px solid #ccc; padding: 8px; margin-top: 20px; margin-bottom: 20px; } .info-section { display: -webkit-box; display: flex; -webkit-box-pack: justify; justify-content: space-between; margin-bottom: 20px; margin-top: 20px; } .location { width: 10%; border: 1px solid #ccc; padding: 8px; } .reason { flex: 1; -webkit-box-flex: 1; -webkit-flex: 1; margin-left: 20px; border: 1px solid #ccc; padding: 8px; } .invoice-table th:first-child, .invoice-table td:first-child { width: 2%; text-align: center; } .invoice-table th:nth-child(2), .invoice-table td:nth-child(2) { width: 13%; text-align: center; } .invoice-table td:nth-child(2) { text-align: left; } .invoice-table th:nth-child(3), .invoice-table td:nth-child(3) { width: 30%; text-align: center; } .invoice-table td:nth-child(3) { text-align: left; } .invoice-table th:nth-child(4), .invoice-table td:nth-child(4), .invoice-table th:nth-child(5), .invoice-table td:nth-child(5) { width: 10%; text-align: center; } .invoice-table td:nth-child(6) { width: 30%; text-align: center; } .invoice-table td:nth-child(6) { text-align: left; } /* Arrow style */ .arrow { font-size: 24px; color: #999; display: -webkit-box; display: flex; -webkit-box-pack: center; justify-content: center; align-items: center; -webkit-box-align: center; } /* Media print rules */ @media print { .invoice { page-break-after: always; /* Ensure each invoice starts on a new page */ } .approval-row{ page-break-inside: avoid; } }</style></head><div class='invoice'> <div class='company-name'>PT EPSON INDONESIA INDUSTRY</div> <div class='invoice-header'> <h1>PART REQUEST</h1> <p>NO #12345</p> </div> <div class='invoice-header-date'> <p>Date: January 1, 2023</p> </div> <table class='invoice-table'> <thead> <tr> <th>NO</th> <th>PART NUMBER</th> <th>PART NAME</th> <th>REQ QTY</th> <th>ACT QTY</th> <th>REMARK</th> </tr> </thead> <tbody> [ITEMS] </tbody> </table> <div class='info-section'> <div class='location'>Location : [LOCATION]</div> <div class='reason'>Reason : [REASON]</div> </div> <div class='approval-remarks'>Approval Remark : [APPROVAL_REMARKS]</div> <div class='approval-row'> <div class='wrapper-approval'> <table class='approval-table'> <thead> <tr> <th colspan='3'>REQUESTOR</th> </tr> <tr> <th>PIC</th> <th>SUPERVISOR</th> <th>MANAGER</th> </tr> </thead> <tbody> <tr> <td> <div class='signature-line'></div> <div>Name :</div> <div>January 5, 2023</div> </td> <td> <div class='signature-line'></div> <div>Name :</div> <div>January 5, 2023</div> </td> <td> <div class='signature-line'></div> <div>Name :</div> <div>January 5, 2023</div> </td> </tr> </tbody> </table> </div> <!-- Arrow icon --> <div class='arrow'>&rarr;</div> <!-- <div class='arrow'></div> --> <div class='wrapper-approval'> <table class='approval-table'> <thead> <tr> <th colspan='3'>PROC DEPT</th> </tr> <tr> <th>SUPERVISOR</th> <th>MANAGER</th> </tr> </thead> <tbody> <tr> <td> <div class='signature-line'></div> <div>Name :</div> <div>January 5, 2023</div> </td> <td> <div class='signature-line'></div> <div>Name :</div> <div>January 5, 2023</div> </td> </tr> </tbody> </table> </div> <!-- Arrow icon --> <div class='arrow'>&rarr;</div> <!-- <div class='arrow'></div> --> <div class='wrapper-approval'> <table class='approval-table'> <thead> <tr> <th colspan='3'>ISSUER</th> </tr> <tr> <th>MANAGER</th> <th>LEADER</th> <th>ADMIN</th> </tr> </thead> <tbody> <tr> <td> <div class='signature-line'></div> <div>Name :</div> <div>January 5, 2023</div> </td> <td> <div class='signature-line'></div> <div>Name :</div> <div>January 5, 2023</div> </td> <td> <div class='signature-line'></div> <div>Name :</div> <div>January 5, 2023</div> </td> </tr> </tbody> </table> </div> </div> </div></html>"
	TEMPLATE_PART_REQUEST := "<!DOCTYPE html><head> <title>PART REQUEST</title> <style> /* Styles for A4 landscape */ @page { /* size: A4 landscape; */ size: A4; margin: 0; } body { font-family: Arial, sans-serif; margin: 0; padding: 0; height: 100vh; /* Set body height to full viewport height */ font-size: 11px; /* Reduced font size */ } /* Invoice styles */ .invoice { page-break-after: always; /* Ensure each invoice starts on a new page */ padding: 40px; /* border: 1px solid #ccc; */ box-sizing: border-box; } .company-name { font-weight: bold; margin-bottom: 5px; } .invoice-header, .invoice-table, .approval-row, .approval-table, .approval-list, .invoice-footer { margin-bottom: 5px; } .approval-row { margin-top :20px; } .invoice-header { text-align: center; } .invoice-footer, .invoice-footer-date { text-align: left; } .invoice-footer-date { margin-top: 8px; /* Adjusted margin */ } .invoice-table { width: 100%; border-collapse: collapse; font-size: 9px !important; /* Reduced font size */ } .invoice-table th, .invoice-table td { border: 1px solid #ccc; /* padding: 8px; */ padding: 4px 8px; } .approval-row { width: 100%; display: flex; justify-content: center; align-items: center; page-break-inside: avoid; display: -webkit-box; -webkit-box-orient: horizontal; -webkit-box-pack: center; /* centers in -webkit-box automagically! */ } .approval-table, .approval-list{ border-collapse: collapse; } .wrapper-approval { width: 100%; -webkit-box-align: stretch; -webkit-box-flex: 1; -webkit-flex: 1; flex: 1; } .approval-row>div:last-child { margin-right: 0; } .approval-table th, .approval-table td { border: 1px solid #ccc; padding: 8px; vertical-align: top; } .signature-line { border-bottom: 1px solid #000; height: 50px; /* Adjust height to accommodate signature */ margin-bottom: 8px; } /* Added style for reason div */ .approval-remarks { border: 1px solid #ccc; padding: 8px; margin-top: 20px; margin-bottom: 20px; } .info-section { display: -webkit-box; display: flex; -webkit-box-pack: justify; justify-content: space-between; margin-bottom: 20px; margin-top: 20px; } /* .location { width: 10%; border: 1px solid #ccc; padding: 8px; } */ .location { flex: 1; -webkit-box-flex: 1; -webkit-flex: 1; margin-left: 20px; /* border: 1px solid #ccc; */ /*padding: 8px;*/ } .dept { flex: 1; -webkit-box-flex: 1; -webkit-flex: 1; /* margin-left: 20px; */ /* border: 1px solid #ccc; */ /*padding: 8px;*/ } .reason { flex: 1; -webkit-box-flex: 1; -webkit-flex: 1; margin-left: 20px; /* border: 1px solid #ccc; */ /*padding: 8px;*/ } .invoice-table th:first-child, .invoice-table td:first-child { width: 2%; text-align: center; } .invoice-table th:nth-child(2), .invoice-table td:nth-child(2) { width: 13%; text-align: center; } .invoice-table td:nth-child(2) { text-align: left; } .invoice-table th:nth-child(3), .invoice-table td:nth-child(3) { width: 30%; text-align: center; } .invoice-table td:nth-child(3) { text-align: left; } .invoice-table th:nth-child(4), .invoice-table td:nth-child(4), .invoice-table th:nth-child(5), .invoice-table td:nth-child(5) { width: 10%; text-align: center; } .invoice-table td:nth-child(6) { width: 30%; text-align: center; } .invoice-table td:nth-child(6) { text-align: left; } /* Arrow style */ .arrow { font-size: 24px; color: #999; display: -webkit-box; display: flex; -webkit-box-pack: center; justify-content: center; align-items: center; -webkit-box-align: center; } /* Media print rules */ @media print { .invoice { page-break-after: always; /* Ensure each invoice starts on a new page */ } .approval-row { page-break-inside: avoid; } } </style></head><div class='invoice'> <div class='company-name'>PT EPSON INDONESIA INDUSTRY</div> <div class='invoice-header'> <h1>PART REQUEST</h1> <p>NO [NO]</p> </div> <div class='invoice-header-date'> <p>Date: [DATE]</p> </div> <div class='info-section'> <div class='dept'>Dept. Requestor : [DEPT]</div> <div class='location'>Location : [LOCATION]</div> <div class='reason'>Reason : [REASON]</div> </div> <table class='invoice-table'> <thead> <tr> <th>NO</th> <th>PART NUMBER</th> <th>PART NAME</th> <th>REQ QTY</th> <th>ACT QTY</th> <th>REMARK</th> </tr> </thead> <tbody> [ITEMS] </tbody> </table> <!-- <div class='approval-remarks'>Approval Remark : [APPROVAL_REMARKS]</div> --> <div class='approval-row'> <div class='wrapper-approval'> <table class='approval-list'> <thead> <tr> <th colspan='3'>Approval List</th> </tr> <tr> <th width='33%' text-align='center'>NIK</th> <th width='33%' text-align='center'>Name</th> <th width='33%' text-align='center'>Approval Date</th> </tr> </thead> <tbody> [APPROVAL_LIST] </tbody> </table> </div> <!-- Arrow icon --> <!-- <div class='arrow'>&rarr;</div> --> <!-- <div class='arrow'></div> --> <!-- Arrow icon --> <!-- <div class='arrow'>&rarr;</div> --> <!-- <div class='arrow'></div> --> <div class='wrapper-approval'> <table class='approval-table'> <thead> <!-- <tr> <th colspan='3'>ISSUER</th> </tr> --> <tr> <th width='33%'>Requestor</th> <th width='33%'>WH DEPT</th> <th width='33%'>WHC DEPT</th> </tr> </thead> <tbody> <tr> <td> <div class='signature-line'></div> <div>NIK :</div> <div>Name :</div> <div>Date :</div> </td> <td> <div class='signature-line'></div> <div>NIK :</div> <div>Name :</div> <div>Date :</div> </td> <td> <div class='signature-line'></div> <div>NIK :</div> <div>Name :</div> <div>Date :</div> </td> </tr> </tbody> </table> </div> </div></div></html>"

	itemsString := ""
	approvalList := ""
	result := ""
	dateString := ""

	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}

	items, err1 := usecase.itemPivotRepository.FindByRequestId(id)

	dataApproval, err2 := usecase.activeRequestApprovalListRepo.FindByRequestId(id)

	// log.Fatal("size items " + helpers.ParseToString(len(items)))
	if items[0].Request.Type == "Request" {
		for idx, item := range items {
			no := helpers.ParseToString(idx + 1)
			reqQty := helpers.ParseToString(item.RequestedQuantity)
			actQty := helpers.ParseToString(item.ApprovedQuantity)

			itemsString = itemsString + "<tr><td>" + no + "</td><td>" + item.ItemCode.ItemCode + "</td><td>" + item.ItemCode.ItemName + "</td><td>" + reqQty + "</td><td>" + actQty + "</td><td>" + item.Notes + "</td></tr>"
		}
		result = strings.Replace(TEMPLATE_PART_REQUEST, "[ITEMS]", itemsString, -1)
		result = strings.Replace(result, "[LOCATION]", items[0].Request.RequestLocation, -1)
		result = strings.Replace(result, "[DEPT]", items[0].Request.Nik.Department, -1)
		for _, approval := range dataApproval {
			dataUsr, err3 := usecase.stagingUserRepository.FindByNik(approval.ApproverNik)
			if err3 != nil {
				return nil, err3
			}

			if approval.DateApproval != nil {
				dateString = approval.DateApproval.Format("2 January 2006")
			} else {
				dateString = "-"
			}
			approvalList = approvalList + "<tr><td>" + approval.ApproverNik + "</td><td>" + dataUsr.Name + "</td><td>" + dateString + "</td></tr>"

		}
		result = strings.Replace(result, "[APPROVAL_LIST]", approvalList, -1)
		result = strings.Replace(result, "[REASON]", items[0].Request.Reason.ReasonName, -1)
		result = strings.Replace(result, "[NO]", items[0].Request.PartRequestCode, -1)
		result = strings.Replace(result, "[DATE]", items[0].Request.RequestDate.Format("2 January 2006"), -1)
	}

	if err1 != nil {
		return nil, err1
	}
	if err2 != nil {
		return nil, err2
	}

	// Client code
	pdfg := wkhtmltopdf.NewPDFPreparer()
	pdfg.AddPage(wkhtmltopdf.NewPageReader(strings.NewReader(result)))
	pdfg.Dpi.Set(600)

	// The html string is also saved as base64 string in the JSON file
	jsonBytes, err4 := pdfg.ToJSON()
	if err4 != nil {
		log.Fatal(err4)
	}

	// Server code, create a new PDF generator from JSON, also looks for the wkhtmltopdf executable
	pdfgFromJSON, err4 := wkhtmltopdf.NewPDFGeneratorFromJSON(bytes.NewReader(jsonBytes))
	if err4 != nil {
		log.Fatal(err4)
	}

	// Create the PDF
	err4 = pdfgFromJSON.Create()
	if err4 != nil {
		log.Fatal(err4)
	}

	return pdfgFromJSON.Bytes(), nil
}
