package usecase

import (
	ItemPivot "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/interfaces"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/models/requests/Part"
	"errors"
)

type itemPivotUsecase struct {
	itemPivotRepository interfaces.ItemPivotRepository
}

func NewItemPivotUsecase(itemPivotRepository interfaces.ItemPivotRepository) *itemPivotUsecase {
	return &itemPivotUsecase{itemPivotRepository: itemPivotRepository}
}

func (ItemPivotUsecase *itemPivotUsecase) GetAll() (data []ItemPivot.ItemPivot, err error) {
	data, err = ItemPivotUsecase.itemPivotRepository.FindAll()
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ItemPivotUsecase *itemPivotUsecase) CreateItemPivot(req []ItemPivot.ItemPivot) (data []ItemPivot.ItemPivot, err error) {
	if len(req) < 1 {
		return nil, errors.New(Const.REQUEST_LESS_THAN_ONE)
	}
	data, err = ItemPivotUsecase.itemPivotRepository.StoreItemPivot(req)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ItemPivotUsecase *itemPivotUsecase) GetById(id uint) (data *ItemPivot.ItemPivot, err error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	data, err = ItemPivotUsecase.itemPivotRepository.FindById(id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ItemPivotUsecase *itemPivotUsecase) GetByRequestId(id uint) (data []ItemPivot.ItemPivot, err error) {
	if id < 1 {
		return nil, errors.New(Const.ID_REQUEST_LESS_THAN_ONE)
	}
	data, err = ItemPivotUsecase.itemPivotRepository.FindByRequestId(id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ItemPivotUsecase *itemPivotUsecase) UpdateRequestData(req []Part.UpdateItemPivot) (data []ItemPivot.ItemPivot, err error) {
	if len(req) < 1 {
		return nil, errors.New(Const.REQUEST_LESS_THAN_ONE)
	}
	data, err = ItemPivotUsecase.itemPivotRepository.PutRequestData(req)
	if err != nil {
		return nil, err
	}
	return data, nil
}
