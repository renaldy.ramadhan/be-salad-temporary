package repository

import (
	ItemPivot "BE-Epson/domains/part/entities"
	"BE-Epson/shared/models/requests/Part"

	"gorm.io/gorm"
)

type itemPivotRepository struct {
	DB *gorm.DB
}

func NewItemPivotRepository(DB *gorm.DB) *itemPivotRepository {
	return &itemPivotRepository{DB: DB}
}

func (ItemPivotRepository *itemPivotRepository) FindAll() (data []ItemPivot.ItemPivot, err error) {
	err = ItemPivotRepository.DB.Find(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ItemPivotRepository *itemPivotRepository) FindById(id uint) (data *ItemPivot.ItemPivot, err error) {
	err = ItemPivotRepository.DB.First(&data, id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ItemPivotRepository *itemPivotRepository) StoreItemPivot(req []ItemPivot.ItemPivot) (data []ItemPivot.ItemPivot, err error) {
	err = ItemPivotRepository.DB.Create(&req).Error
	if err != nil {
		return nil, err
	}
	return req, nil
}

func (ItemPivotRepository *itemPivotRepository) FindByRequestId(id uint) (data []ItemPivot.ItemPivot, err error) {
	err = ItemPivotRepository.DB.Preload("ItemCode").Preload("Request").Preload("Request.Reason").Preload("Request.Nik").Find(&data, "id_request_fk = ? ", id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (ItemPivotRepository *itemPivotRepository) PutRequestData(req []Part.UpdateItemPivot) (datas []ItemPivot.ItemPivot, err error) {
	tx := ItemPivotRepository.DB.Begin()
	for _, item := range req {
		data := ItemPivot.ItemPivot{
			Model:              gorm.Model{ID: item.IdItemPivot},
			ItemCodeFk:         item.ItemCode,
			RequestedQuantity:  item.RequestQuantity,
			ApprovedQuantity:   item.ApprovedQuantity,
			AdjustmentQuantity: item.AdjustmentQuantity,
			Notes:              item.Notes,
		}
		err = tx.Updates(&data).Error
		if err != nil {
			tx.Rollback()
			return nil, err
		}
		datas = append(datas, data)
	}
	if tx.Commit().Error != nil {
		tx.Rollback()
		return nil, err
	}
	return datas, nil
}
