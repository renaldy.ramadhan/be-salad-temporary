package repository

import (
	"BE-Epson/configs/app"
	"BE-Epson/domains/part/entities"
	"gorm.io/gorm"
)

type RequestStagingItemsRepositoryExternal struct {
	DB *gorm.DB
}

func NewRequestStagingItemsRepositoryExternal(DB *gorm.DB) *RequestStagingItemsRepositoryExternal {
	return &RequestStagingItemsRepositoryExternal{DB: DB}
}

func (RequestStagingItemsRepositoryExternal *RequestStagingItemsRepositoryExternal) FindAllSystemQuantity(itemCode, locationCode string) (stagingItem uint, err error) {
	query := RequestStagingItemsRepositoryExternal.DB
	if itemCode != "" {
		query = query.Table(app.GetConfig().Database.ExternalTableName[0]).Select("system_quantity").Where("item_code = ? AND location_code = ?", itemCode, locationCode)
	}
	if err = query.Find(&stagingItem).Error; err != nil {
		return 0, nil
	}
	return stagingItem, nil
}

func (RequestStagingItemRepositoryExternal *RequestStagingItemsRepositoryExternal) FindAll(locationCode string) (res []entities.StagingItemExternal, err error) {
	err = RequestStagingItemRepositoryExternal.DB.Table(app.GetConfig().Database.ExternalTableName[0]).Select("item_code, system_quantity,location_code").Where("location_code = ?", locationCode).Scan(&res).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}
