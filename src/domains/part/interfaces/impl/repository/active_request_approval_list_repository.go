package repository

import (
	ActiveRequestApprovalList "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/presistence"
	OrganizationApprovalRoute "BE-Epson/domains/staging/entities"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/requests/Part"
	Part2 "BE-Epson/shared/models/responses/Part"
	"errors"
	"time"

	"gorm.io/gorm"
)

type activeRequestApprovalListRepo struct {
	DB *gorm.DB
}

func NewActiveRequestApprovalListRepo(DB *gorm.DB) *activeRequestApprovalListRepo {
	return &activeRequestApprovalListRepo{DB}
}

func (repo activeRequestApprovalListRepo) buildQuery(query *gorm.DB, req *ActiveRequestApprovalList.ActiveApprovalListParams, nik string) (*gorm.DB, int64) {
	var count int64
	query.Model(&ActiveRequestApprovalList.ActiveRequestApprovalList{}).Joins("Request").Where("approver_nik = ? AND active_request_approval_lists.status = 'In Progress' AND Request.type = ?", nik, req.Type).Count(&count)

	if req.PageSize > 0 && req.Page > 0 {
		query = query.Limit(req.PageSize).Offset((req.Page - 1) * req.PageSize)
	}

	return query, count
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) FindByRequestId(id uint) (Res []Part2.GetOneActiveRequestApprovalListResponse, err error) {
	data := []ActiveRequestApprovalList.ActiveRequestApprovalList{}
	err = activeRequestApprovalListRepo.DB.Preload("Department").
		Joins("JOIN requests ON requests.id = active_request_approval_lists.id_request").
		Where("requests.id = ?", id).
		Find(&data).Error
	if err != nil {
		return nil, err
	}
	for _, value := range data {
		dataBaru, err2 := activeRequestApprovalListRepo.createResponseActiveRequestApprovalList(&value)
		if err2 != nil {
			return nil, err2
		}
		Res = append(Res, dataBaru)
	}
	return Res, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) FindByEventId(id uint) (Res []Part2.GetOneActiveRequestApprovalListResponse, err error) {
	data := []ActiveRequestApprovalList.ActiveRequestApprovalList{}
	err = activeRequestApprovalListRepo.DB.Preload("Department").
		Where("id_event = ?", id).
		Find(&data).Error
	if err != nil {
		return nil, err
	}
	for _, value := range data {
		dataBaru, err2 := activeRequestApprovalListRepo.createResponseActiveRequestApprovalList(&value)
		if err2 != nil {
			return nil, err2
		}
		Res = append(Res, dataBaru)
	}
	return Res, nil

}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) createResponseActiveRequestApprovalList(data *ActiveRequestApprovalList.ActiveRequestApprovalList) (Res Part2.GetOneActiveRequestApprovalListResponse, err error) {
	var dateApproval *time.Time
	var IdRequest *uint
	if data.DateApproval != nil {
		dateApproval = data.DateApproval
	} else {
		dateApproval = nil
	}
	if data.IdRequest != nil {
		IdRequest = data.IdRequest
	}

	Res = Part2.GetOneActiveRequestApprovalListResponse{
		Model:            data.Model,
		IdFlowStep:       data.IdFlowStep,
		IdDepartment:     data.IdDepartment,
		IdRequest:        IdRequest,
		IdEvent:          data.IdEvent,
		DateApproval:     dateApproval,
		ApprovalSequence: data.ApprovalSequence,
		Notes:            data.Notes,
		ApproverNik:      data.ApproverNik,
		Status:           data.Status,
		Department:       data.Department,
		Request:          data.Request,
		DueDateApproval:  data.DueDateApproval,
		ApproverName:     "",
	}
	getApprover := &OrganizationApprovalRoute.StagingUser{}
	err = activeRequestApprovalListRepo.DB.Select("name").First(&getApprover, "nik = ?", data.ApproverNik).Error
	if getApprover.Name != "" {
		Res.ApproverName = getApprover.Name
		return Res, nil
	}
	getOrganizationApproverName := &OrganizationApprovalRoute.OrganizationApprovalRoute{}
	err = activeRequestApprovalListRepo.DB.Select("name").First(&getOrganizationApproverName, "id = ?", data.IdFlowStep).Error
	if getOrganizationApproverName.Name != "" {
		Res.ApproverName = getOrganizationApproverName.Name
		return Res, nil
	}
	return Res, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) FindAll(types string) (data []ActiveRequestApprovalList.ActiveRequestApprovalList, err error) {
	err = activeRequestApprovalListRepo.DB.
		Joins("JOIN requests ON requests.id = active_request_approval_lists.id_request").
		Where("requests.type = ?", types).
		Find(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) FindOne(id uint) (*ActiveRequestApprovalList.ActiveRequestApprovalList, error) {
	data := &ActiveRequestApprovalList.ActiveRequestApprovalList{}
	err := activeRequestApprovalListRepo.DB.Preload("FlowStep").Preload("Request").Preload("Department").Find(&data, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) FindAllById(id uint) (data []ActiveRequestApprovalList.ActiveRequestApprovalList, err error) {
	err = activeRequestApprovalListRepo.DB.Find(&data, "id_request = ?", id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) StoreOne(Req *ActiveRequestApprovalList.ActiveRequestApprovalList) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error) {
	if err = activeRequestApprovalListRepo.DB.Create(&Req).Error; err != nil {
		return nil, err
	}
	return Req, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) PutStatusActiveRequestList(Req *Part.UpdateStatusActiveRequestList) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error) {
	tx := activeRequestApprovalListRepo.DB.Begin()
	if err = tx.Find(&Res, "id_request = ? AND approver_nik = ? and status = 'In Progress' ", Req.IdActiveRequestList, Req.Nik).Error; err != nil {
		tx.Rollback()
		return nil, errors.New("data not found")
	}
	Res.Status = helpers.IsStatusActiveApprovalList(Req.Status)
	if Req.Notes != "" {
		Res.Notes = Req.Notes
	}
	if err = tx.Model(&Res).Where("id_request = ? and approver_nik = ? and status = 'In Progress' ", Req.IdActiveRequestList, Req.Nik).Updates(&Res).Error; err != nil {
		tx.Rollback()
		return nil, err
	}
	if Req.ItemPivot != nil && len(Req.ItemPivot) > 0 {
		for _, item := range Req.ItemPivot {
			if item.IdItemPivot < 1 {
				tx.Rollback()
				return nil, errors.New("id item pivot less than one")
			}
			data := &ActiveRequestApprovalList.ItemPivot{}
			if err = tx.Model(&data).First(&data, "ID = ? AND id_request_fk = ?", item.IdItemPivot, Req.IdActiveRequestList).Error; err != nil {
				tx.Rollback()
				return nil, err
			}
			data.ApprovedQuantity = item.ApprovedQuantity
			if item.Notes != "" {
				data.Notes = item.Notes
			}
			if err = tx.Where("ID = ? AND id_request_fk = ? ", item.IdItemPivot, Req.IdActiveRequestList).Updates(&data).Error; err != nil {
				tx.Rollback()
				return nil, err
			}
		}
		if err = tx.Commit().Error; err != nil {
			tx.Rollback()
			return nil, err
		}
	} else {
		if err = tx.Commit().Error; err != nil {
			tx.Rollback()
			return nil, err
		}
	}
	Res, err = activeRequestApprovalListRepo.AfterUpdates(Res)
	if err != nil {
		return Res, err
	}
	return Res, nil
}

func (repo *activeRequestApprovalListRepo) PutStatusApprovalEvent(Req *Part.UpdateStatusEventApproval) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error) {
	if err = repo.DB.First(&Res, "id_event = ? and approver_nik = ? and status = 'In Progress' ", Req.IdEvent, Req.Nik).Error; err != nil {
		return nil, errors.New("Data not found")
	}

	currentDate := time.Now()
	Res.Status = helpers.IsStatusActiveApprovalList(Req.Status)
	Res.DateApproval = &currentDate

	if Req.Notes != "" {
		Res.Notes = Req.Notes
	}
	if err = repo.DB.Model(&Res).Where("id_event = ? and approver_nik = ? and status = 'In Progress' ", Req.IdEvent, Req.Nik).Updates(&Res).Error; err != nil {
		return nil, err
	}
	Res, err = repo.AfterUpdateEventApproval(Res)
	if err != nil {
		return nil, err
	}
	return Res, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) AfterUpdateEventApproval(data *ActiveRequestApprovalList.ActiveRequestApprovalList) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error) {
	if data.Status == presistence.Rejected {
		data.Event.Status = helpers.StringPointer("In Active")
		if err = activeRequestApprovalListRepo.DB.Model(&data.Event).Where("id = ?", data.IdEvent).Updates(&data.Event).Error; err != nil {
			return nil, err
		}
		return data, nil
	}

	nextApprovalList := data.ApprovalSequence + 1
	payload := &ActiveRequestApprovalList.ActiveRequestApprovalList{
		Status: presistence.InProgress,
	}
	if err = activeRequestApprovalListRepo.DB.Model(&payload).Where("id_event = ? and approval_sequence = ?", data.IdEvent, nextApprovalList).Updates(&payload).Error; err != nil {
		return data, nil

	}
	return data, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) AfterUpdates(data *ActiveRequestApprovalList.ActiveRequestApprovalList) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error) {
	if err = activeRequestApprovalListRepo.DB.Model(&data).Association("Request").Find(&data.Request); err != nil {
		return nil, err
	}
	var (
		countMax         int64
		date             *time.Time
		requestData      *ActiveRequestApprovalList.Request
		countAllRequest  int64
		countDoneRequest int64
	)
	if err = activeRequestApprovalListRepo.DB.Model(&data).Where("id_request = ?", data.IdRequest).Count(&countMax).Error; err != nil && countMax == 0 {
		return nil, err
	}
	currentDate := time.Now()
	data.DateApproval = &currentDate
	if data.Status == presistence.Rejected {
		data.Request.Status = presistence.Rejected
		if err = activeRequestApprovalListRepo.DB.Model(&data.Request).Updates(&data.Request).Error; err != nil {
			return nil, err
		}
		if err = activeRequestApprovalListRepo.DB.Model(&data).Updates(&data).Error; err != nil {
			return nil, errors.New("failed to update active request list")
		}
		return data, nil
	}
	if data.Status != presistence.Rejected && data.Status != presistence.Queued {
		date = data.DateApproval
		data.Request.LastApprovalSequence = data.ApprovalSequence
		if err = activeRequestApprovalListRepo.DB.Model(&data).Updates(&data).Error; err != nil {
			return nil, errors.New("failed to update active request list")
		}
	}
	if data.Request.Status == presistence.New {
		data.Request.Status = presistence.InProgress
		if err = activeRequestApprovalListRepo.DB.Model(&data.Request).Updates(&data.Request).Error; err != nil {
			return nil, errors.New("failed to update request or request already done")
		}
	}

	if helpers.ParseInt64ToUint(countMax-1) == data.ApprovalSequence && data.Status == presistence.Done {
		data.Request.Status = presistence.Done
		if err = activeRequestApprovalListRepo.DB.Model(&data.Request).Updates(&data.Request).Error; err != nil {
			return nil, errors.New("failed to update request or request already done")
		}

		if data.Request.Type == presistence.Disposal || data.Request.Type == presistence.Ringi {
			if err = activeRequestApprovalListRepo.DB.Model(&requestData).Where("id_disposal_group_event_fk = ?", data.Request.IdDisposalGroupEventFk).Count(&countAllRequest).Error; err != nil {
				return nil, err
			}
			if err = activeRequestApprovalListRepo.DB.Model(&requestData).Where("id_disposal_group_event_fk = ? and status = ?", data.Request.IdDisposalGroupEventFk, "Done").Count(&countDoneRequest).Error; err != nil {
				return nil, err
			}

			if countAllRequest == countDoneRequest {
				payloadUpdate := &ActiveRequestApprovalList.ActiveRequestApprovalList{
					Status: presistence.InProgress,
				}
				if err = activeRequestApprovalListRepo.DB.Model(&ActiveRequestApprovalList.ActiveRequestApprovalList{}).Where("id_event = ? and approval_sequence = ?", data.Request.IdDisposalGroupEventFk, 1).Updates(&payloadUpdate).Error; err != nil {
					return nil, err
				}
			}
		}
		return data, nil
	}

	nextRequestList := &ActiveRequestApprovalList.ActiveRequestApprovalList{
		Model: gorm.Model{
			ID: data.ID,
		},
		Status:  data.Status,
		Request: data.Request,
	}
	nextRequestList.ID = data.ID + 1
	if err = activeRequestApprovalListRepo.DB.Model(&data).
		First(&nextRequestList, "ID = ? AND id_request = ? AND approval_sequence = ?",
			nextRequestList.ID,
			data.IdRequest,
			data.ApprovalSequence+1).Error; err != nil {
		return nil, nil
	}
	if data.Status == presistence.Done {
		nextRequestList.StartDateApproval = date
	}
	nextRequestList.Status = presistence.InProgress
	if nextRequestList == nil {
		return data, nil
	}
	if err = activeRequestApprovalListRepo.DB.Save(&nextRequestList).Error; err != nil {
		return nil, err
	}
	return nextRequestList, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) FindApprovalList(nik string) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error) {
	var data []ActiveRequestApprovalList.ActiveRequestApprovalList
	err := activeRequestApprovalListRepo.DB.Find(&data, "approver_nik = ?", nik).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) FindApprovalListAssigned(nik string, params *ActiveRequestApprovalList.ActiveApprovalListParams) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error, int64) {
	var data []ActiveRequestApprovalList.ActiveRequestApprovalList
	query, total := activeRequestApprovalListRepo.buildQuery(activeRequestApprovalListRepo.DB, params, nik)
	err := query.Preload("Department").Joins("Request").Find(&data, "approver_nik = ? AND active_request_approval_lists.status = 'In Progress' AND Request.type = ?", nik, params.Type).Error
	if err != nil {
		return nil, err, 0
	}
	return data, nil, total
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) CheckIsApproverByRequestId(nik string, id uint) (bool, error) {
	var data ActiveRequestApprovalList.ActiveRequestApprovalList
	err := activeRequestApprovalListRepo.DB.First(&data, "approver_nik = ? and id_request = ? and status = 'In Progress' ", nik, id).Error
	if err != nil {
		return false, err
	}
	return true, nil
}

func (actioveRequestApprovalListRepo *activeRequestApprovalListRepo) CheckIsApproverByEventId(nik string, id uint, reqType string) (bool, bool, error) {
	var (
		requestData      *ActiveRequestApprovalList.Request
		approverActive   *ActiveRequestApprovalList.ActiveRequestApprovalList
		countAllRequest  int64
		countDoneRequest int64
		countApprover    int64
	)

	statusRequest := false
	statusApprover := false

	err := actioveRequestApprovalListRepo.DB.Model(&requestData).Where("id_disposal_group_event_fk = ? and type = ?", id, reqType).Count(&countAllRequest).Error
	if err != nil {
		return false, false, err
	}

	err = actioveRequestApprovalListRepo.DB.Model(&requestData).Where("id_disposal_group_event_fk = ? and status = ? and type = ?", id, "Done", reqType).Count(&countDoneRequest).Error
	if err != nil {
		return false, false, err
	}

	errApprover := actioveRequestApprovalListRepo.DB.Find(&approverActive, "id_event = ? and approver_nik = ? and status = ?", id, nik, "In Progress").Count(&countApprover).Error
	if errApprover != nil {
		return false, false, errApprover
	}

	if countApprover > 0 {
		statusApprover = true
	}

	if countAllRequest == countDoneRequest {
		statusRequest = true
	}

	return statusRequest, statusApprover, nil
}

func (activeRequestApprovalListRepo *activeRequestApprovalListRepo) CheckPrintDataWhcInRequestDivision(IdRequest, DepartmentId uint) (flag bool, err error) {
	data := []ActiveRequestApprovalList.ActiveRequestApprovalList{}
	err = activeRequestApprovalListRepo.DB.Find(&data, "id_request = ? and id_department = ?", IdRequest, DepartmentId).Error
	if err != nil {
		return false, err
	}
	for _, value := range data {
		if value.Status == presistence.Rejected {
			return false, nil
		}
		if value.Status == presistence.InProgress || value.Status == presistence.Done {
			flag = true
		}
	}
	return flag, nil
}
