package repository

import (
	RequestApprovalHistory "BE-Epson/domains/part/entities"
	"gorm.io/gorm"
)

type requestApprovalHistoryRepository struct {
	DB *gorm.DB
}

func NewRequestApprovalHistoryRepository(DB *gorm.DB) *requestApprovalHistoryRepository {
	return &requestApprovalHistoryRepository{DB}
}

func (requestApprovalHistoryRepo *requestApprovalHistoryRepository) FindAll() (data []RequestApprovalHistory.RequestApprovalHistory, totalData int64, err error) {
	err = requestApprovalHistoryRepo.DB.Find(&data).Count(&totalData).Error
	if err != nil {
		return nil, totalData, err
	}
	return data, totalData, nil
}

func (requestApprovalHistoryRepo *requestApprovalHistoryRepository) FindOne(id uint) (data *RequestApprovalHistory.RequestApprovalHistory, err error) {
	err = requestApprovalHistoryRepo.DB.Find(&data, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}
