package repository

import (
	DisposalGroup "BE-Epson/domains/event/entities"
	Request "BE-Epson/domains/part/entities"
	OrganizationApprovalRoute "BE-Epson/domains/staging/entities"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/helpers"
	PartRequest "BE-Epson/shared/models/requests/Part"
	PartResponse "BE-Epson/shared/models/responses/Part"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"strconv"
	"strings"
)

type RequestRepository struct {
	DB *gorm.DB
}

func NewRequestRepository(DB *gorm.DB) *RequestRepository {
	return &RequestRepository{DB: DB}
}

func (repo *RequestRepository) buildQuery(query *gorm.DB, Req *PartRequest.SearchRequest) (*gorm.DB, int64) {
	var count int64
	query.Model(&Request.Request{}).Where("type = ?", Req.Type).Count(&count)

	if Req.Type != "" {
		query = query.Where("type = ?", Req.Type)
	}
	if Req.IdEvent > 0 {
		query = query.Where("id_disposal_group_event_fk = ?", Req.IdEvent)
	}
	if Req.PageSize > 0 && Req.Page > 0 {
		query = query.Limit(Req.PageSize).Offset((Req.Page - 1) * Req.PageSize)
	}

	OrderBy := "id"
	ascOrDsc := "desc"
	if Req.AscOrDesc != "" {
		if Req.OrderBy != "" && helpers.IsRequestValidColumn(Req.OrderBy) {
			OrderBy = Req.OrderBy
		}
		if Req.AscOrDesc == "asc" || Req.AscOrDesc == "desc" {
			ascOrDsc = Req.AscOrDesc
		}
	}
	query = query.Order(OrderBy + " " + ascOrDsc)
	return query, count
}

func (repo *RequestRepository) GetAllRequest(Req *PartRequest.SearchRequest) (requests []Request.Request, err error, count int64) {
	query, total := repo.buildQuery(repo.DB, Req)
	if err = query.Find(&requests).Error; err != nil {
		return nil, fmt.Errorf("error while getting all requests: %v", err), 0
	}
	return requests, nil, total
}

func (repo *RequestRepository) FindAllRequestByNik(Req *PartRequest.SearchRequest) (requests []Request.Request, err error) {
	lineCode := &OrganizationApprovalRoute.OrganizationApprovalRoute{}
	if err := repo.DB.Select("line_code").Where("nik = ?", Req.Nik).First(lineCode).Error; err != nil {
		return nil, errors.New(Const.LINE_IS_NOT_FOUND)
	}
	query, _ := repo.buildQuery(repo.DB, Req)
	if err = query.Find(&requests, "line_code", lineCode.LineCode).Error; err != nil {
		return nil, fmt.Errorf("error while getting all requests: %v", err)
	}
	return requests, nil
}

func (repo *RequestRepository) StoreOneRequest(Req *PartRequest.CreateRequest) (Response *PartResponse.RequestResponse, err error) {
	var (
		approverEvent DisposalGroup.DisposalGroup
		partReqCode   string
	)
	partReqCode = Req.PartRequestCode
	tx := repo.DB.Begin()
	data := &OrganizationApprovalRoute.OrganizationApprovalRoute{}
	if err = tx.Select("line_code").First(&data, "nik = ?", Req.Nik).Error; err != nil {
		tx.Rollback()
		return nil, errors.New(Const.LINE_IS_NOT_FOUND)
	}

	if Req.Type == "Disposal" {
		fmt.Println("IdEvent", Req.IdDisposalGroupEvent)
		if err = tx.First(&approverEvent, Req.IdDisposalGroupEvent).Error; err != nil {
			tx.Rollback()
			return nil, errors.New(Const.ID_DISPOSAL_GROUP_EVENT_NOT_FOUND)
		}
		partReqCode = *approverEvent.DisposalCode
	}
	Res := &Request.Request{
		UserName:               Req.UserName,
		IdReason:               Req.IdReason,
		PurchasingGroupCodeFk:  Req.PurchasingGroupCodeFk,
		IdParent:               Req.IdParent,
		Type:                   Req.Type,
		RequestDate:            Req.RequestDate,
		DueDate:                Req.DueDate,
		PlantName:              Req.PlantName,
		LocationCodeFk:         Req.LocationCodeFk,
		RequestLocation:        Req.RequestLocation,
		DestroyLocation:        Req.DestroyLocation,
		Notes:                  Req.Notes,
		Status:                 "New", // "New" is the default value for "Status
		NikRequest:             Req.Nik,
		LineCode:               data.LineCode,
		DisposalType:           helpers.IsDisposalType(Req.DisposalType),
		PartRequestCode:        partReqCode,
		IdDisposalGroupEventFk: Req.IdDisposalGroupEvent,
		WarehouseLocation:      Req.WarehouseLocation,
	}
	err = tx.Create(&Res).Error
	if err != nil {
		tx.Rollback()
		fmt.Println(err)
		return nil, fmt.Errorf("error while creating request: %v", err)
	}
	for i := range Req.Items {
		priceStr := strings.Replace(strings.TrimSpace(Req.Items[i].PricePerUnit), ",", ".", -1)
		price, errPrice := strconv.ParseFloat(priceStr, 64)
		if errPrice != nil {
			return nil, err
		}
		fmt.Println(float64(Req.Items[i].ApprovedQuantity) * price)

		singleItem := &Request.ItemPivot{
			ItemCodeFk:        Req.Items[i].ItemCodeFk,
			IdRequestFk:       Res.ID,
			RequestedQuantity: Req.Items[i].RequestedQuantity,
			ApprovedQuantity:  Req.Items[i].ApprovedQuantity,
			Notes:             Req.Items[i].Notes,
			Amount:            float64(Req.Items[i].ApprovedQuantity) * price,
		}
		if err = tx.Model(&Request.ItemPivot{}).Create(&singleItem).Error; err != nil {
			tx.Rollback()
			return nil, fmt.Errorf("error while creating request items: %v", err)
		}
	}

	if err = tx.Commit().Error; err != nil {
		tx.Rollback()
		return nil, fmt.Errorf("error while commiting request: %v", err)
	}
	return repo.createRequestToRequestResponse(Req, Res.ID, data.LineCode), nil
}

func (repo *RequestRepository) StoreBulkRequest(req []PartRequest.CreateRequest, nik string, username string) ([]PartResponse.RequestResponse, error) {
	var Res []PartResponse.RequestResponse
	tx := repo.DB.Begin()
	for _, Req := range req {
		Req.Nik = nik
		Req.UserName = username

		data, err := repo.StoreOneRequest(&Req)
		if err != nil {
			tx.Rollback()
			return nil, fmt.Errorf("error while creating request: %v", err)
		}
		Res = append(Res, *data)
	}
	return Res, nil
}

func (repo *RequestRepository) createRequestToRequestResponse(Req *PartRequest.CreateRequest, Id uint, lineCode string) *PartResponse.RequestResponse {
	return &PartResponse.RequestResponse{
		Id:                    Id,
		UserName:              Req.UserName,
		IdReason:              Req.IdReason,
		PurchasingGroupCodeFk: Req.PurchasingGroupCodeFk,
		IdParent:              Req.IdParent,
		Type:                  Req.Type,
		RequestDate:           Req.RequestDate,
		DueDate:               Req.DueDate,
		PlantName:             Req.PlantName,
		Notes:                 Req.Notes,
		NikRequest:            Req.Nik,
		LineCode:              lineCode,
		RequestLocation:       Req.RequestLocation,
		DestroyLocation:       Req.DestroyLocation,
		DisposalType:          Req.DisposalType,
		Items:                 Req.Items,
		PartRequestCode:       Req.PartRequestCode,
		WarehouseLocation:     Req.WarehouseLocation,
	}
}

func (repo *RequestRepository) DeleteRequest(partReqId uint) (err error) {
	err = repo.DB.Table("part_requests").
		Unscoped().Where("id = ?", partReqId).
		Delete(&Request.Request{}).Error
	if err != nil {
		err = errors.New("Error deleting master request")
		return err
	}
	return err
}

func (repo *RequestRepository) CheckDataIdDisposalGroupEvent(IdDisposalGroupEvent uint) (bool, error) {
	if err := repo.DB.First(&Request.Request{}, IdDisposalGroupEvent).Error; err != nil {
		return false, errors.New(Const.ID_DISPOSAL_GROUP_EVENT_NOT_FOUND)
	}
	return true, nil
}

func (repo *RequestRepository) CheckDataIdParentGroupEvent(IdDisposalGroupEvent uint) (bool, error) {
	if err := repo.DB.First(&Request.Request{}, IdDisposalGroupEvent).Error; err != nil {
		return false, errors.New(Const.ID_PARENT_NOT_FOUND)
	}
	return true, nil
}

func (repo *RequestRepository) FindOneRequest(partReqId uint) (Res *PartResponse.GetOneRequestResponse, err error) {
	Res = &PartResponse.GetOneRequestResponse{}
	var itemStagingPivot []Request.ItemStagingPivot
	err = repo.DB.Preload("DisposalGroup").First(&Res.Request, "id = ?", partReqId).Error
	if err != nil {
		return nil, errors.New("Error getting master request: " + err.Error())
	}
	err = repo.DB.Model(&Request.ItemPivot{}).Where("id_request_fk = ?", partReqId).Joins("JOIN staging_items ON staging_items.item_code = item_pivots.item_code_fk").Select("item_pivots.item_code_fk, item_pivots.id_request_fk, item_pivots.requested_quantity, item_pivots.approved_quantity, item_pivots.adjustment_quantity, item_pivots.notes, item_pivots.amount, staging_items.item_name, staging_items.price_per_unit").Scan(&itemStagingPivot).Error
	if err != nil {
		return nil, errors.New("Error getting master request: " + err.Error())
	}
	Res.Items = itemStagingPivot
	return Res, nil
}

func (repo RequestRepository) FindRequestByCode(partReqCode string) (Res []Request.Request, err error) {
	search := "%" + partReqCode + "%" // Assuming partReqCode is a string
	err = repo.DB.Where("part_request_code LIKE ?", search).Find(&Res).Error
	if err != nil {
		return nil, errors.New("Error getting master request: " + err.Error())
	}
	return Res, nil

}
