package repository

import (
	Sequence "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/presistence"
	"gorm.io/gorm"
)

type sequenceRepository struct {
	DB *gorm.DB
}

func NewSequenceRepository(db *gorm.DB) *sequenceRepository {
	return &sequenceRepository{
		DB: db,
	}
}

func (repo *sequenceRepository) GetAllSequence() (res []Sequence.Sequence, err error) {
	err = repo.DB.Find(&res).Error
	return
}

//
//func (repo *sequenceRepository) StoreOneSequence(Req *Sequence.Sequence) (Res *Sequence.Sequence, err error) {
//	err = repo.DB.Create(&Req).Error
//	return
//}

func (repo *sequenceRepository) GetSequenceByType(Type presistence.RequestType) (Res Sequence.Sequence, err error) {
	err = repo.DB.Where("type = ?", Type).First(&Res).Error
	return
}

func (repo *sequenceRepository) DoResetSequence(Type presistence.RequestType) (err error) {
	data := &Sequence.Sequence{}
	err = repo.DB.Where("type = ?", Type).First(&data).Error
	if err != nil {
		return err
	}
	data.NextResetDate = data.NextResetDate.AddDate(0, 1, 0)
	data.SequenceLevel = 1
	err = repo.DB.Where("type = ?", Type).Save(&data).Error
	return err
}

func (repo *sequenceRepository) UpdateSequenceLevelByType(Type presistence.RequestType) (err error) {
	err = repo.DB.Model(&Sequence.Sequence{}).Where("type = ?", Type).Update("sequence_level", gorm.Expr("sequence_level + ?", 1)).Error
	return
}
