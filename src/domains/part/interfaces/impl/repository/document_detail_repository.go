package repository

import (
	DocumentDetail "BE-Epson/domains/part/entities"
	"gorm.io/gorm"
)

type documentDetailRepository struct {
	DB *gorm.DB
}

func NewDocumentDetailRepository(db *gorm.DB) *documentDetailRepository {
	return &documentDetailRepository{
		DB: db,
	}
}

func (repository *documentDetailRepository) FindAll(limit int, offset int, order string) (documentDetails []DocumentDetail.DocumentDetail, err error) {
	err = repository.DB.Limit(limit).Offset(offset).Order(order).Find(&documentDetails).Error
	if err != nil {
		return nil, err
	}
	return documentDetails, nil
}

func (repository *documentDetailRepository) FindById(id uint) (documentDetail *DocumentDetail.DocumentDetail, err error) {
	err = repository.DB.Where("id = ?", id).Find(&documentDetail).Error
	if err != nil {
		return nil, err
	}
	return documentDetail, nil
}

func (repository *documentDetailRepository) Create(documentDetail *DocumentDetail.DocumentDetail) error {
	err := repository.DB.Create(documentDetail).Error
	if err != nil {
		return err
	}
	return nil
}

func (repository *documentDetailRepository) Update(documentDetail *DocumentDetail.DocumentDetail) error {
	err := repository.DB.Save(documentDetail).Error
	if err != nil {
		return err
	}
	return nil
}

func (repository *documentDetailRepository) Delete(documentDetail *DocumentDetail.DocumentDetail) error {
	err := repository.DB.Delete(documentDetail).Error
	if err != nil {
		return err
	}
	return nil
}

func (repository *documentDetailRepository) StoreDocuments(documentDetail *DocumentDetail.DocumentDetail) (err error) {
	err = repository.DB.Create(documentDetail).Error
	if err != nil {
		return err
	}
	return nil
}

func (repository *documentDetailRepository) GetDocumentPDF(id uint) error {
	var documentDetail DocumentDetail.DocumentDetail
	err := repository.DB.Where("id = ?", id).Find(&documentDetail).Error
	if err != nil {
		return err
	}
	return nil
}

func (repository *documentDetailRepository) FindAllByRequestId(limit int, offset int, order string, reqId uint) (documentDetails []DocumentDetail.DocumentDetail, err error) {
	err = repository.DB.Limit(limit).Offset(offset).Order(order).Where("id_request = ?", reqId).Find(&documentDetails).Error
	if err != nil {
		return nil, err
	}
	return documentDetails, nil
}

func (repository *documentDetailRepository) DeleteById(id uint) (documentDetail *DocumentDetail.DocumentDetail, err error) {
	err = repository.DB.Where("id = ?", id).Delete(&documentDetail).Error
	if err != nil {
		return nil, err
	}
	return documentDetail, nil
}

func (repository *documentDetailRepository) StoreBulkDocumentFiles(data []DocumentDetail.DocumentDetail) (err error) {
	err = repository.DB.Create(&data).Error
	if err != nil {
		return err
	}
	return nil
}
