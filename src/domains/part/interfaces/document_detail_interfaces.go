package interfaces

import (
	DocumentDetail "BE-Epson/domains/part/entities"
	"BE-Epson/shared/models/requests/Part"
)

type DocumentDetailRepository interface {
	// Get all data from the database, limit the data, and order it by the latest data
	FindAll(limit int, offset int, order string) (documentDetails []DocumentDetail.DocumentDetail, err error)
	// Get data from the database by id
	FindById(id uint) (documentDetail *DocumentDetail.DocumentDetail, err error)
	DeleteById(id uint) (documentDetail *DocumentDetail.DocumentDetail, err error)
	StoreDocuments(documentDetail *DocumentDetail.DocumentDetail) (err error)
	// get list file by request ID
	FindAllByRequestId(limit int, offset int, order string, reqId uint) (documentDetails []DocumentDetail.DocumentDetail, err error)
	StoreBulkDocumentFiles(documentDetails []DocumentDetail.DocumentDetail) (err error)
}

type DocumentDetailUsecase interface {
	// Get all data from the database, limit the data, and order it by the latest data
	GetAll(limit int, offset int, order string) (documentDetails []DocumentDetail.DocumentDetail, err error)
	// Get data from the database by id
	GetById(id uint) (documentDetail *DocumentDetail.DocumentDetail, err error)
	RemoveById(id uint) (documentDetail *DocumentDetail.DocumentDetail, err error)
	CreateDocuments(documentDetail *DocumentDetail.DocumentDetail) (err error)
	GetDocumentPDF(id uint) (documentDetail *DocumentDetail.DocumentDetail, err error)
	CreateMultipleFiles(*Part.CreateRequest) (err error)
	GetAllByRequestId(limit int, offset int, order string, reqId uint) (documentDetails []DocumentDetail.DocumentDetail, err error)
}
