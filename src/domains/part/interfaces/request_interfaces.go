package interfaces

import (
	FlowStep "BE-Epson/domains/master/entities"
	Request "BE-Epson/domains/part/entities"
	"BE-Epson/shared/models/requests/Part"
	PartResponse "BE-Epson/shared/models/responses/Part"
)

type RequestRepository interface {
	GetAllRequest(Type *Part.SearchRequest) ([]Request.Request, error, int64)
	StoreOneRequest(Req *Part.CreateRequest) (Res *PartResponse.RequestResponse, err error)
	StoreBulkRequest(req []Part.CreateRequest, nik string, username string) (Response []PartResponse.RequestResponse, err error)
	CheckDataIdParentGroupEvent(IdDisposalGroupEvent uint) (bool, error)
	CheckDataIdDisposalGroupEvent(IdDisposalGroupEvent uint) (bool, error)
	DeleteRequest(partReqId uint) (err error)
	FindOneRequest(partReqId uint) (Res *PartResponse.GetOneRequestResponse, err error)
	FindAllRequestByNik(Req *Part.SearchRequest) ([]Request.Request, error)
	FindRequestByCode(partReqCode string) (Res []Request.Request, err error)
}

type GetStagingItemExternalRepo interface {
	FindAllSystemQuantity(string, string) (uint, error)
	FindAll(locationCode string) ([]Request.StagingItemExternal, error)
}

type RequestUseCase interface {
	GetAllRequestByNik(Req *Part.SearchRequest) ([]Request.Request, error)
	GetAllRequest(Req *Part.SearchRequest) ([]Request.Request, error, int64)
	CreateOneRequest(Req *Part.CreateRequest) (Res *PartResponse.RequestResponse, err error)
	CreateBulkRequest(Req []Part.CreateRequest, nik string, username string) (Res []PartResponse.RequestResponse, Flowstep []FlowStep.FlowStep, err error)
	DeleteRequest(partReqId uint) (err error)
	GetOneRequest(partReqId uint) (Res *PartResponse.GetOneRequestResponse, err error)
	GetRequestByCode(partReqCode string) (Res []Request.Request, err error)
}
