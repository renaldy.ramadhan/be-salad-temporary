package interfaces

import (
	Sequence "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/presistence"
)

type SequenceRepository interface {
	GetAllSequence() ([]Sequence.Sequence, error)
	//StoreOneSequence(Req *Sequence.CreateSequenceRequest) (Res *Sequence.Sequence, err error)
	GetSequenceByType(Type presistence.RequestType) (Res Sequence.Sequence, err error)
	DoResetSequence(Type presistence.RequestType) (err error)
	UpdateSequenceLevelByType(Type presistence.RequestType) (err error)
}
