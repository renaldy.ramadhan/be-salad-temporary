package interfaces

import (
	RequestApprovalHistory "BE-Epson/domains/part/entities"
	"BE-Epson/shared/models/requests/master"
)

type RequestApprovalHistoryRepository interface {
	FindAll() (data []RequestApprovalHistory.RequestApprovalHistory, totalData int64, err error)
	FindOne(id uint) (data *RequestApprovalHistory.RequestApprovalHistory, err error)
}

type RequestApprovalHistoryUseCase interface {
	GetAllRequestApprovalHistory(Req *master.RequestApprovalHistoryGet) ([]RequestApprovalHistory.RequestApprovalHistory, error)
	GetOneRequestApprovalHistory(id uint) (*RequestApprovalHistory.RequestApprovalHistory, error)
}
