package interfaces

import (
	ItemPivot "BE-Epson/domains/part/entities"
	"BE-Epson/shared/models/requests/Part"
)

type ItemPivotRepository interface {
	FindAll() (data []ItemPivot.ItemPivot, err error)
	FindById(id uint) (data *ItemPivot.ItemPivot, err error)
	FindByRequestId(id uint) (data []ItemPivot.ItemPivot, err error)
	StoreItemPivot(req []ItemPivot.ItemPivot) (data []ItemPivot.ItemPivot, err error)
	PutRequestData(req []Part.UpdateItemPivot) (datas []ItemPivot.ItemPivot, err error)
}

type ItemPivotUsecase interface {
	GetAll() (data []ItemPivot.ItemPivot, err error)
	GetById(id uint) (data *ItemPivot.ItemPivot, err error)
	GetByRequestId(id uint) (data []ItemPivot.ItemPivot, err error)
	CreateItemPivot(req []ItemPivot.ItemPivot) (data []ItemPivot.ItemPivot, err error)
	UpdateRequestData(req []Part.UpdateItemPivot) (data []ItemPivot.ItemPivot, err error)
}
