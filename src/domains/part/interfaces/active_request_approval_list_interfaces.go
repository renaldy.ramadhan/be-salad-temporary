package interfaces

import (
	ActiveRequestApprovalList "BE-Epson/domains/part/entities"
	"BE-Epson/shared/models/requests/Part"
	Part2 "BE-Epson/shared/models/responses/Part"
)

type ActiveRequestApprovalListRepository interface {
	FindAll(string) (data []ActiveRequestApprovalList.ActiveRequestApprovalList, err error)
	FindOne(id uint) (*ActiveRequestApprovalList.ActiveRequestApprovalList, error)
	FindAllById(id uint) (data []ActiveRequestApprovalList.ActiveRequestApprovalList, err error)
	FindByRequestId(id uint) (data []Part2.GetOneActiveRequestApprovalListResponse, err error)
	FindByEventId(id uint) (Res []Part2.GetOneActiveRequestApprovalListResponse, err error)
	StoreOne(Req *ActiveRequestApprovalList.ActiveRequestApprovalList) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error)
	PutStatusActiveRequestList(Req *Part.UpdateStatusActiveRequestList) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error)
	PutStatusApprovalEvent(Req *Part.UpdateStatusEventApproval) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error)
	FindApprovalList(string) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error)
	FindApprovalListAssigned(string, *ActiveRequestApprovalList.ActiveApprovalListParams) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error, int64)
	CheckIsApproverByRequestId(string, uint) (bool, error)
	CheckPrintDataWhcInRequestDivision(IdRequest uint, DepartmentId uint) (bool, error)
	CheckIsApproverByEventId(nik string, id uint, reqType string) (bool, bool, error)
}

type ActiveRequestApprovalListUseCase interface {
	CheckIsApproverByRequestId(string, uint) (bool, error)
	GetAllActiveRequestApprovalList(string) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error)
	GetActiveRequestApprovalListById(Id uint) (*ActiveRequestApprovalList.ActiveRequestApprovalList, error)
	GetAllActiveRequestApprovalListById(Id uint) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error)
	GetByRequestId(id uint) ([]Part2.GetOneActiveRequestApprovalListResponse, error)
	GetByEventId(id uint) ([]Part2.GetOneActiveRequestApprovalListResponse, error)
	CreateOneActiveRequestApprovalList(Req *ActiveRequestApprovalList.ActiveRequestApprovalList) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error)
	UpdateStatusActiveRequestList(Req *Part.UpdateStatusActiveRequestList) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error)
	UpdateStatusApprovalEvent(Req *Part.UpdateStatusEventApproval) (Res *ActiveRequestApprovalList.ActiveRequestApprovalList, err error)
	GetApprovaLoginlList(string) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error)
	GetApprovaLoginListAssigned(string, *ActiveRequestApprovalList.ActiveApprovalListParams) ([]ActiveRequestApprovalList.ActiveRequestApprovalList, error, int64)
	GetPrintData(request *ActiveRequestApprovalList.Request) (bool, error)
	CheckIsApproverByEventId(nik string, id uint, reqType string) (bool, bool, error)
}
