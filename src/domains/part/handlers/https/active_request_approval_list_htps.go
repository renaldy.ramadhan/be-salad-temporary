package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	interfaces2 "BE-Epson/domains/event/interfaces"
	repository2 "BE-Epson/domains/event/interfaces/impl/repository"
	usecase2 "BE-Epson/domains/event/interfaces/impl/usecase"
	"BE-Epson/domains/master/entities"
	ActiveRequestApprovalList "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/interfaces"
	"BE-Epson/domains/part/interfaces/impl/repository"
	"BE-Epson/domains/part/interfaces/impl/usecase"
	"BE-Epson/domains/part/presistence"
	StagingInterface "BE-Epson/domains/staging/interfaces"
	repository3 "BE-Epson/domains/staging/interfaces/impl/repository"
	"BE-Epson/middlewares"
	SessionUser "BE-Epson/shared/entities"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/requests/Part"
	PartResponse "BE-Epson/shared/models/responses/Part"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"time"
)

type ActiveRequestApprovalListHttp struct {
	ActiveRequestApprovalListUsecase interfaces.ActiveRequestApprovalListUseCase
	DisposalApproverOption           interfaces2.ActiveEventApprovalListUseCase
}

func NewActiveRequestApprovalListHttp(router *gin.Engine, stagingUserRepository StagingInterface.StagingUserRepository) *ActiveRequestApprovalListHttp {
	handler := &ActiveRequestApprovalListHttp{
		ActiveRequestApprovalListUsecase: usecase.NewActiveRequestApprovalListUseCase(repository.NewActiveRequestApprovalListRepo(database.ConnectDatabase(Const.DB_SALAD)),
			repository3.NewSAPAdjustmentRepository(database.ConnectDatabase(Const.EXTERNAL_STK_ADJ)),
			repository.NewRequestRepository(database.ConnectDatabase(Const.DB_SALAD)),
			repository3.NewStagingEmployeeCompanyRepository(database.ConnectDatabase(Const.DB_SALAD)),
			repository3.NewStagingUserRepository(database.ConnectDatabase(Const.DB_SALAD))),
		DisposalApproverOption: usecase2.NewActiveEventApprovalListUseCase(repository2.NewActiveEventApprovalListRepo(database.ConnectDatabase(Const.DB_SALAD))),
	}
	activeRequestApprovalList := router.Group("/salad/api/active-request-list")
	{
		activeRequestApprovalList.GET("", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAllActiveRequestApprovalList)
		activeRequestApprovalList.GET("/by-request", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAllByRequestIdRequestApprovalList)
		activeRequestApprovalList.GET("/by-event", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetApprovalListByEvent)
		activeRequestApprovalList.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetActiveRequestApprovalListById)
		activeRequestApprovalList.GET("/approval-list/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAllActiveRequestApprovalListById)
		activeRequestApprovalList.GET("/approval-list", middlewares.VerifyUserKeycloak(stagingUserRepository), middlewares.GetUserInfo(), handler.GetApprovaLoginlList)
		activeRequestApprovalList.GET("/approval-list-assigned",
			middlewares.VerifyUserKeycloak(stagingUserRepository),
			middlewares.GetUserInfo(),
			//middlewares.MiddlewareKosong(),
			handler.GetApprovaLoginlListAssigned)
		activeRequestApprovalList.POST("/add", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateActiveRequestApprovalList)
		activeRequestApprovalList.POST("/update-status",
			//middlewares.MiddlewareKosong(),
			middlewares.VerifyUserKeycloak(stagingUserRepository),
			middlewares.GetUserInfo(),
			handler.UpdateStatusActiveRequestApprovalList)
		activeRequestApprovalList.POST("/update-approval-event", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.UpdateStatusApprovalEvent)
		activeRequestApprovalList.GET("/check-approval-by-req", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CheckIsApproverByRequestId)
		activeRequestApprovalList.GET("/check-approval-by-event", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CheckIsApproverByEventId)
	}
	return handler
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) CheckIsApproverByRequestId(c *gin.Context) {
	nik := c.MustGet("UserData").(*SessionUser.SessionUser).Nik
	id := helpers.ParseToUint(c.Query("id"))
	flag, _ := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.CheckIsApproverByRequestId(nik, id)
	//if err != nil {
	//	c.JSON(http.StatusBadRequest, gin.H{
	//		"message": err.Error(),
	//	})
	//	return
	//}
	c.JSON(http.StatusOK, flag)
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) CheckIsApproverByEventId(c *gin.Context) {
	nik := c.MustGet("UserData").(*SessionUser.SessionUser).Nik
	id := helpers.ParseToUint(c.Query("id"))
	reqType := c.Query("type")
	statusRequest, statusApprover, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.CheckIsApproverByEventId(nik, id, reqType)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"statusRequest":  statusRequest,
		"statusApprover": statusApprover,
	})
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) GetAllActiveRequestApprovalListById(c *gin.Context) {
	data, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.GetAllActiveRequestApprovalListById(helpers.ParseToUint(c.Param("id")))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) GetApprovalListByEvent(c *gin.Context) {
	id := helpers.ParseToUint(c.Query("id"))
	data, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.GetByEventId(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) GetApprovaLoginlList(c *gin.Context) {
	userNik := c.MustGet("UserData").(*SessionUser.SessionUser).Nik
	data, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.GetApprovaLoginlList(userNik)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) GetApprovaLoginlListAssigned(c *gin.Context) {
	userNik := c.MustGet("UserData").(*SessionUser.SessionUser).Nik
	params := &ActiveRequestApprovalList.ActiveApprovalListParams{
		Page:     helpers.ParseInt(c.Query("page")),
		PageSize: helpers.ParseInt(c.Query("pageSize")),
		Type:     c.Query("type"),
	}
	data, err, total := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.GetApprovaLoginListAssigned(userNik, params)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data":  data,
		"total": total,
	})
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) UpdateStatusActiveRequestApprovalList(c *gin.Context) {
	data := &Part.UpdateStatusActiveRequestList{}
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if data.Nik == "" {
		data.Nik = c.MustGet("UserData").(*SessionUser.SessionUser).Nik
	}
	resData, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.UpdateStatusActiveRequestList(data)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resData)
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) UpdateStatusApprovalEvent(c *gin.Context) {
	data := &Part.UpdateStatusEventApproval{}
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	result, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.UpdateStatusApprovalEvent(data)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, result)
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) GetAllByRequestIdRequestApprovalList(c *gin.Context) {
	id := helpers.ParseToUint(c.Query("id"))
	data, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.GetByRequestId(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) GetAllActiveRequestApprovalList(c *gin.Context) {
	types := helpers.ParseToString(c.Query("type"))
	data, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.GetAllActiveRequestApprovalList(types)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) GetActiveRequestApprovalListById(c *gin.Context) {
	data, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.GetActiveRequestApprovalListById(helpers.ParseToUint(c.Param("id")))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (ActiveRequestApprovalListHttp *ActiveRequestApprovalListHttp) CreateActiveRequestApprovalList(c *gin.Context) {
	//responseData := []ActiveRequestApprovalList.ActiveRequestApprovalList{}
	requestData := c.MustGet("ResponseCreateRequest").(*PartResponse.RequestResponse)
	approverData := c.MustGet("ApproverData").([]Part.GetApproveData)
	idDisposalEvent := c.MustGet("IdDisposalEvent").(*uint)
	var (
		numSeq          uint
		dueDateApproval *time.Time
	)
	status := ""
	organizationApprovalRouteData := c.MustGet("OrganizationApprovalRouteData").([]Part.GetOrganizationApprovalRoute)
	flowStepData := c.MustGet("FlowStep").([]entities.FlowStep)
	for _, value := range flowStepData {
		startDate := time.Now()
		if requestData.Type == presistence.Disposal {
			approvalSetting, _ := ActiveRequestApprovalListHttp.DisposalApproverOption.FindApproverSettingByEvent(*idDisposalEvent, *value.IdDepartment)
			if approvalSetting != nil {
				dueDateApproval = approvalSetting.DueDateApproval
			}
		}
		if value.Type == presistence.Organization {
			_, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.CreateOneActiveRequestApprovalList(
				&ActiveRequestApprovalList.ActiveRequestApprovalList{
					Model:             gorm.Model{},
					IdFlowStep:        value.ID,
					IdDepartment:      *value.IdDepartment,
					IdRequest:         &requestData.Id,
					ApprovalSequence:  0,
					Notes:             "",
					StartDateApproval: &startDate,
					DateApproval:      &startDate,
					Status:            helpers.IsStatusActiveApprovalList(presistence.Done),
					ApproverNik:       requestData.NikRequest,
					DueDateApproval:   dueDateApproval,
				})
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"message": err.Error(),
				})
				return
			}
			break
		}
	}
	for _, orgData := range organizationApprovalRouteData {
		startDate := time.Now()
		numSeq++
		if numSeq == 1 {
			status = "In Progress"
		} else {
			status = "Queued"
		}
		if requestData.Type == presistence.Disposal {
			approvalSetting, _ := ActiveRequestApprovalListHttp.DisposalApproverOption.FindApproverSettingByEvent(*idDisposalEvent, *orgData.FlowStep.IdDepartment)
			if approvalSetting != nil {
				dueDateApproval = approvalSetting.DueDateApproval
			}
		}
		_, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.CreateOneActiveRequestApprovalList(
			&ActiveRequestApprovalList.ActiveRequestApprovalList{
				Model:             gorm.Model{},
				IdFlowStep:        orgData.FlowStep.ID,
				IdDepartment:      *orgData.FlowStep.IdDepartment,
				IdRequest:         &requestData.Id,
				ApprovalSequence:  numSeq,
				Notes:             "",
				StartDateApproval: &startDate,
				Status:            helpers.IsStatusActiveApprovalList(status),
				ApproverNik:       orgData.OrganizationApprovalData.Nik,
				DueDateApproval:   dueDateApproval,
			})
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
	}
	for _, locData := range approverData {
		numSeq++
		if requestData.Type == presistence.Disposal {
			approvalSetting, _ := ActiveRequestApprovalListHttp.DisposalApproverOption.FindApproverSettingByEvent(*idDisposalEvent, *locData.FlowStep.IdDepartment)
			if approvalSetting != nil {
				dueDateApproval = approvalSetting.DueDateApproval
			}
		}
		_, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.CreateOneActiveRequestApprovalList(
			&ActiveRequestApprovalList.ActiveRequestApprovalList{
				Model:            gorm.Model{},
				IdFlowStep:       locData.FlowStep.ID,
				IdDepartment:     *locData.FlowStep.IdDepartment,
				IdRequest:        &requestData.Id,
				ApprovalSequence: numSeq,
				Notes:            "",
				Status:           "",
				ApproverNik:      locData.Approver.Nik,
				DueDateApproval:  dueDateApproval,
			})
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		//responseData = append(responseData, *data)
	}

	c.JSON(http.StatusCreated, requestData)
}

func (ActiveRequestApprovalListHttp ActiveRequestApprovalListHttp) CreateActiveRequestApprovalListRingiBulk(c *gin.Context) {
	ResponseRequest := c.MustGet("ResponseCreateBulkRequest").([]PartResponse.RequestResponse)
	organizationApprovalRouteData := c.MustGet("OrganizationApprovalRouteDataBulk").([]Part.GetOrganizationApprovalRoute)
	flowStepData := c.MustGet("ResponseFlowStep").([]entities.FlowStep)

	var numSeq uint
	status := ""

	for _, requestData := range ResponseRequest {
		for _, value := range flowStepData {
			startDate := time.Now()
			if value.Type == presistence.Organization {
				_, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.CreateOneActiveRequestApprovalList(
					&ActiveRequestApprovalList.ActiveRequestApprovalList{
						Model:             gorm.Model{},
						IdFlowStep:        value.ID,
						IdDepartment:      *value.IdDepartment,
						IdRequest:         &requestData.Id,
						ApprovalSequence:  0,
						Notes:             "",
						StartDateApproval: &startDate,
						DateApproval:      &startDate,
						Status:            helpers.IsStatusActiveApprovalList(presistence.Done),
						ApproverNik:       requestData.NikRequest,
					})
				if err != nil {
					c.JSON(http.StatusBadRequest, gin.H{
						"message": err.Error(),
					})
					return
				}
				break
			}
		}

		for _, orgData := range organizationApprovalRouteData {
			startDate := time.Now()

			numSeq++
			if numSeq == 1 {
				status = "In Progress"
			} else {
				status = "Queued"
			}
			_, err := ActiveRequestApprovalListHttp.ActiveRequestApprovalListUsecase.CreateOneActiveRequestApprovalList(
				&ActiveRequestApprovalList.ActiveRequestApprovalList{
					Model:             gorm.Model{},
					IdFlowStep:        orgData.FlowStep.ID,
					IdDepartment:      *orgData.FlowStep.IdDepartment,
					IdRequest:         &requestData.Id,
					ApprovalSequence:  numSeq,
					Notes:             "",
					StartDateApproval: &startDate,
					Status:            helpers.IsStatusActiveApprovalList(status),
					ApproverNik:       orgData.OrganizationApprovalData.Nik,
				})
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"message": err.Error(),
				})
				return
			}
		}
		numSeq = 0
	}

	c.JSON(http.StatusCreated, ResponseRequest)
}
