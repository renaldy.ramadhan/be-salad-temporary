package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	"BE-Epson/domains/part/interfaces"
	"BE-Epson/domains/part/interfaces/impl/repository"
	"BE-Epson/domains/part/interfaces/impl/usecase"
	StagingInterface "BE-Epson/domains/staging/interfaces"
	StagingRepo "BE-Epson/domains/staging/interfaces/impl/repository"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"net/http"

	"github.com/gin-gonic/gin"
)

// type DocumentPrintHttp struct {
// 	documentDetailUsecase interfaces.DocumentDetailUsecase
// }

type DocumentPrintHttp struct {
	DocumentPrintUsecase interfaces.DocumentPrintUsecase
}

func NewDocumentPrintHttp(router *gin.Engine, stagingUserRepository StagingInterface.StagingUserRepository) *DocumentPrintHttp {
	handler := &DocumentPrintHttp{
		DocumentPrintUsecase: usecase.NewDocumentPrintUsecase(
			repository.NewItemPivotRepository(database.ConnectDatabase(Const.DB_SALAD)),
			repository.NewActiveRequestApprovalListRepo(database.ConnectDatabase(Const.DB_SALAD)),
			StagingRepo.NewStagingUserRepository(database.ConnectDatabase(Const.DB_SALAD)),
		),
	}
	documentDetail := router.Group("/salad/api/document-print")
	{
		documentDetail.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetById)
	}
	return handler
}

func (DocumentPrintHttp *DocumentPrintHttp) GetById(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	docPdfByte, err := DocumentPrintHttp.DocumentPrintUsecase.GetById(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Header("Content-Disposition", "attachment; filename=part_request.pdf")
	c.Data(http.StatusOK, "application/pdf", docPdfByte) //for txt application/octet-stream
}
