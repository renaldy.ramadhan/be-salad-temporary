package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	ApproverPivotHttp "BE-Epson/domains/master/handlers/https"
	repository2 "BE-Epson/domains/master/interfaces/impl/repository"
	"BE-Epson/domains/part/interfaces"
	"BE-Epson/domains/part/interfaces/impl/repository"
	"BE-Epson/domains/part/interfaces/impl/usecase"
	OrganizationApprovalRouteHttp "BE-Epson/domains/staging/handlers/http"
	StagingInterface "BE-Epson/domains/staging/interfaces"
	repository3 "BE-Epson/domains/staging/interfaces/impl/repository"
	"BE-Epson/middlewares"
	SessionUser "BE-Epson/shared/entities"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/requests/Part"
	"encoding/json"
	"fmt"
	"mime/multipart"
	"net/http"

	"github.com/go-playground/validator/v10"

	"github.com/gin-gonic/gin"
)

type RequestHttp struct {
	RequestUsecase                interfaces.RequestUseCase
	WorkflowHttps                 *ApproverPivotHttp.WorkflowHttps
	FlowStepHttp                  *ApproverPivotHttp.FlowStepHttp
	ActiveRequestHttp             *ActiveRequestApprovalListHttp
	OrganizationApprovalRouteHttp *OrganizationApprovalRouteHttp.OrganizationApprovalRouteHttp
	//ApprovalGroupHttp             *ApproverGroupHttp.ApproverGroupHttp
	ApproverPivotHttp  *ApproverPivotHttp.ApproverPivotHttp
	DocumentDetailHttp *DocumentDetailHttp
}

func NewRequestHttp(router *gin.Engine, WorkFlow *ApproverPivotHttp.WorkflowHttps, FlowStep *ApproverPivotHttp.FlowStepHttp, OrganizationApprovalRouteHttp *OrganizationApprovalRouteHttp.OrganizationApprovalRouteHttp, ActiveRequestList *ActiveRequestApprovalListHttp, ApproverPivot *ApproverPivotHttp.ApproverPivotHttp,
	DocumentDetail *DocumentDetailHttp, stagingUserRepository StagingInterface.StagingUserRepository) *RequestHttp {
	handler := &RequestHttp{
		RequestUsecase: usecase.NewRequestUseCase(
			repository.NewRequestRepository(database.ConnectDatabase(Const.DB_SALAD)),
			repository.NewRequestStagingItemsRepositoryExternal(database.ConnectDatabase(Const.EXTERNAL_DB)),
			repository.NewSequenceRepository(database.ConnectDatabase(Const.DB_SALAD)),
			repository2.NewWorkflowRepository(database.ConnectDatabase(Const.DB_SALAD)),
			repository2.NewFlowStepRepository(database.ConnectDatabase(Const.DB_SALAD)),
			repository3.NewOrganizationApprovalRouteRepository(database.ConnectDatabase(Const.DB_SALAD))),
		WorkflowHttps:                 WorkFlow,
		FlowStepHttp:                  FlowStep,
		ActiveRequestHttp:             ActiveRequestList,
		OrganizationApprovalRouteHttp: OrganizationApprovalRouteHttp,
		//ApprovalGroupHttp:             ApproverGroupHttp,
		ApproverPivotHttp:  ApproverPivot,
		DocumentDetailHttp: DocumentDetail,
	}
	Request := router.Group("/salad/api/request")
	{
		Request.GET("", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAllRequest)
		Request.POST("/add",
			middlewares.VerifyUserKeycloak(stagingUserRepository),
			middlewares.GetUserInfo(),
			//middlewares.MiddlewareKosong(),
			handler.CreateRequest,
			//middlewares.GenerateSequenceCode(),
			//middlewares.GenerateApprovalGroup(),
			handler.WorkflowHttps.GetWorkflowById,
			handler.FlowStepHttp.GetAllFlowStepByWorkflow,
			handler.OrganizationApprovalRouteHttp.GenerateApprovalRoute, //generate organizational approval
			handler.ApproverPivotHttp.GetAllLocationApprover,            //get pivot table by approval group & get approver id
			handler.ActiveRequestHttp.CreateActiveRequestApprovalList,
			//handler.
		)
		Request.POST("/create-bulk",
			middlewares.VerifyUserKeycloak(stagingUserRepository),
			middlewares.GetUserInfo(),
			handler.CreateBulkRequest,
			handler.OrganizationApprovalRouteHttp.GenerateApprovalRouteRingiBulk,
			handler.ActiveRequestHttp.CreateActiveRequestApprovalListRingiBulk,
		)
		Request.GET("/get-by-nik", middlewares.VerifyUserKeycloak(stagingUserRepository), middlewares.GetUserInfo(), handler.GetAllRequestByNik)
		Request.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetOneRequest)
		Request.DELETE("/delete/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.DeleteRequest)
		Request.GET("/search", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetRequestByCode)
	}
	return handler
}

func (RequestHttp *RequestHttp) GetAllRequestByNik(c *gin.Context) {
	data := &Part.SearchRequest{
		Id:     helpers.ParseUint(c.Query("id")),
		Type:   helpers.IsRequestType(c.Query("type")),
		Search: helpers.ParseToString(c.Query("search")),
		Nik:    c.MustGet("UserData").(*SessionUser.SessionUser).Nik,
	}
	Request, err := RequestHttp.RequestUsecase.GetAllRequestByNik(data)
	if err != nil {
		c.JSON(http.StatusPreconditionFailed, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, Request)
}

func (RequestHttp *RequestHttp) GetRequestByCode(c *gin.Context) {
	code := helpers.ParseToString(c.Query("code"))
	Request, err := RequestHttp.RequestUsecase.GetRequestByCode(code)
	if err != nil {
		c.JSON(http.StatusPreconditionFailed, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, Request)
}

// Bussiness Logic
func (RequestHttp *RequestHttp) GetAllRequest(c *gin.Context) {
	data := &Part.SearchRequest{
		Id:       helpers.ParseUint(c.Query("id")),
		IdEvent:  helpers.ParseUint(c.Query("id_event")),
		Type:     helpers.IsRequestType(c.Query("type")),
		Page:     helpers.ParseInt(c.Query("page")),
		PageSize: helpers.ParseInt(c.Query("pageSize")),
		Search:   helpers.ParseToString(c.Query("search")),
		Nik:      c.MustGet("UserData").(*SessionUser.SessionUser).Nik,
	}
	Request, err, count := RequestHttp.RequestUsecase.GetAllRequest(data)
	if err != nil {
		c.JSON(http.StatusPreconditionFailed, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"totalData": count,
		"data":      Request,
	})
}

func (RequestHttp *RequestHttp) CreateBulkRequest(c *gin.Context) {
	data := []Part.CreateRequest{}
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": fmt.Sprintf("Failed to parse JSON data: %v", err),
		})
		return
	}
	Nik := c.MustGet("UserData").(*SessionUser.SessionUser).Nik
	UserName := c.MustGet("UserData").(*SessionUser.SessionUser).Name

	// Create Bulk Request
	Request, Flowstep, err := RequestHttp.RequestUsecase.CreateBulkRequest(data, Nik, UserName)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Set("ResponseCreateBulkRequest", Request)
	c.Set("ResponseFlowStep", Flowstep)
	c.Set("UserNik", Nik)
}

func (RequestHttp *RequestHttp) CreateRequest(c *gin.Context) {
	if err := c.Request.ParseMultipartForm(30 << 40); err != nil { // 30 MB max memory
		c.JSON(http.StatusBadRequest, gin.H{"message": "Error parsing form data: " + err.Error()})
		return
	}
	data := &Part.CreateRequest{}
	jsonData := c.PostForm("jsonData")
	if err := json.Unmarshal([]byte(jsonData), &data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": fmt.Sprintf("Failed to parse JSON data: %v", err),
		})
		return
	}
	validate := validator.New()
	if err := validate.Struct(data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": fmt.Sprintf("Validation failed: %v", err),
		})
		return
	}
	data.Nik = c.MustGet("UserData").(*SessionUser.SessionUser).Nik
	// Mengambil file dari form-data
	files := c.Request.MultipartForm.File["files"]
	data.Files = make([]*multipart.FileHeader, 0, len(files))
	for _, file := range files {
		data.Files = append(data.Files, file)
	}
	data.UserName = c.MustGet("UserData").(*SessionUser.SessionUser).Name
	Request, err := RequestHttp.RequestUsecase.CreateOneRequest(data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	data.Id = Request.Id
	Request.LocationCode = data.LocationCodeFk
	// Create Document
	if len(data.Files) > 0 {
		if errSave := RequestHttp.DocumentDetailHttp.documentDetailUsecase.CreateMultipleFiles(data); errSave != nil {
			fmt.Println("Error while saving document: %v", errSave)
		}
	}
	c.Set("IdDisposalEvent", data.IdDisposalGroupEvent)
	c.Set("ResponseCreateRequest", Request)
}

func (RequestHttp *RequestHttp) GetOneRequest(c *gin.Context) {
	id := c.Param("id")
	parseId := helpers.ParseUint(id)
	if parseId == 0 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": fmt.Sprintf("Failed to parse id %s", id),
		})
		return
	}
	Request, err := RequestHttp.RequestUsecase.GetOneRequest(parseId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	Request.PrintData, err = RequestHttp.ActiveRequestHttp.ActiveRequestApprovalListUsecase.GetPrintData(Request.Request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, Request)
}

func (RequestHttp *RequestHttp) DeleteRequest(c *gin.Context) {
	id := c.Param("id")
	parseId := helpers.ParseUint(id)
	if parseId == 0 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": fmt.Sprintf("Failed to parse id %s", id),
		})
		return
	}
	err := RequestHttp.RequestUsecase.DeleteRequest(parseId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "Part Request deleted successfully",
	})
}
