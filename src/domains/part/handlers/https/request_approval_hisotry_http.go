package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	"BE-Epson/domains/part/interfaces"
	"BE-Epson/domains/part/interfaces/impl/repository"
	"BE-Epson/domains/part/interfaces/impl/usecase"
	StagingInterface "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/requests/master"
	"github.com/gin-gonic/gin"
	"net/http"
)

type RequestApprovalHistoryHttp struct {
	requestApprovalHistoryUsecase interfaces.RequestApprovalHistoryUseCase
}

func NewRequestApprovalHistoryHttp(router *gin.Engine, stagingUserRepository StagingInterface.StagingUserRepository) *RequestApprovalHistoryHttp {
	handler := &RequestApprovalHistoryHttp{
		requestApprovalHistoryUsecase: usecase.NewRequestApprovalHistoryUsecase(repository.NewRequestApprovalHistoryRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}
	requestApprovalHistory := router.Group("/salad/api/request-approval-history")
	{
		requestApprovalHistory.GET("", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAll)
		requestApprovalHistory.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetOne)
	}
	return handler
}
func (RequestApprovalHistoryHttp *RequestApprovalHistoryHttp) GetAll(c *gin.Context) {
	data := &master.RequestApprovalHistoryGet{
		IdRequestApprovalHistory: helpers.ParseUint(c.Query("id_request_approval_history")),
		IdRequest:                helpers.ParseUint(c.Query("id_request")),
		IdApprover:               helpers.ParseUint(c.Query("id_approver")),
		Limit:                    helpers.ParseUint(c.Query("limit")),
		Offset:                   helpers.ParseUint(c.Query("offset")),
		Page:                     helpers.ParseUint(c.Query("page")),
	}
	result, err := RequestApprovalHistoryHttp.requestApprovalHistoryUsecase.GetAllRequestApprovalHistory(data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, result)
}

func (RequestApprovalHistoryHttp *RequestApprovalHistoryHttp) GetOne(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	result, err := RequestApprovalHistoryHttp.requestApprovalHistoryUsecase.GetOneRequestApprovalHistory(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, result)
}
