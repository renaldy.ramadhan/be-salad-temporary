package https

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	ItemPivot "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/interfaces"
	"BE-Epson/domains/part/interfaces/impl/repository"
	"BE-Epson/domains/part/interfaces/impl/usecase"
	StagingInterface "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/requests/Part"
	"github.com/gin-gonic/gin"
	"net/http"
)

type ItemPivotHttp struct {
	itemPivotUsecase interfaces.ItemPivotUsecase
}

func NewItemPivotHttp(router *gin.Engine, stagingUserRepository StagingInterface.StagingUserRepository) *ItemPivotHttp {
	handler := &ItemPivotHttp{
		itemPivotUsecase: usecase.NewItemPivotUsecase(repository.NewItemPivotRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}
	itemPivot := router.Group("/salad/api/item-pivot")
	{
		itemPivot.GET("", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAll)
		itemPivot.GET("/request/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetByRequestId)
		itemPivot.POST("/add", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.Create)
		itemPivot.PUT("/update/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.UpdateRequestData)
	}
	return handler
}

func (ItemPivotHttp *ItemPivotHttp) UpdateRequestData(c *gin.Context) {
	req := []Part.UpdateItemPivot{}
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	_, err := ItemPivotHttp.itemPivotUsecase.UpdateRequestData(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "success update data !",
	})
}

func (ItemPivotHttp *ItemPivotHttp) Create(c *gin.Context) {
	req := []ItemPivot.ItemPivot{}
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	result, err := ItemPivotHttp.itemPivotUsecase.CreateItemPivot(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, result)
}

func (ItemPivotHttp *ItemPivotHttp) GetAll(c *gin.Context) {
	result, err := ItemPivotHttp.itemPivotUsecase.GetAll()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, result)
}

func (ItemPivotHttp *ItemPivotHttp) GetById(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	result, err := ItemPivotHttp.itemPivotUsecase.GetById(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, result)
}

func (ItemPivotHttp *ItemPivotHttp) GetByRequestId(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	result, err := ItemPivotHttp.itemPivotUsecase.GetByRequestId(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, result)
}
