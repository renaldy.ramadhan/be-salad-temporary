package https

import (
	appConfig "BE-Epson/configs/app"
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	DocumentDetail "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/interfaces"
	"BE-Epson/domains/part/interfaces/impl/repository"
	"BE-Epson/domains/part/interfaces/impl/usecase"
	StagingInterface "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type DocumentDetailHttp struct {
	documentDetailUsecase interfaces.DocumentDetailUsecase
}

func NewDocumentDetailHttp(router *gin.Engine, stagingUserRepository StagingInterface.StagingUserRepository) *DocumentDetailHttp {
	handler := &DocumentDetailHttp{
		documentDetailUsecase: usecase.NewDocumentDetailUsecase(repository.NewDocumentDetailRepository(
			database.ConnectDatabase(Const.DB_SALAD))),
	}
	documentDetail := router.Group("/salad/api/document")
	{
		documentDetail.GET("", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAll)
		documentDetail.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetById)
		documentDetail.POST("/add", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.Create)
		documentDetail.GET("/download/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetDocumentPDF)

		documentDetail.GET("/request/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAllByRequestId)
		documentDetail.DELETE("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.DeleteById)
	}
	return handler
}

func (DocumentDetailHttp *DocumentDetailHttp) GetDocumentPDF(ctx *gin.Context) {
	// Custome Request Need to be here
	data, err := DocumentDetailHttp.documentDetailUsecase.GetDocumentPDF(helpers.ParseUint(ctx.Param("id")))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	// Open the file
	fileData, err := os.Open(data.PathDestination)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to open file"})
		return
	}
	defer fileData.Close()
	// Read the first 512 bytes of the file to determine its content type
	fileHeader := make([]byte, 512)
	_, err = fileData.Read(fileHeader)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to read file"})
		return
	}
	fileContentType := helpers.GetMimeType(data.DocumentExtensions)
	// Get the file info
	fileInfo, err := fileData.Stat()
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get file info"})
		return
	}
	// Set the headers for the file transfer and return the file
	ctx.Header("Content-Description", "File Transfer")
	ctx.Header("Content-Transfer-Encoding", "binary")
	ctx.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%s", strings.ReplaceAll(data.DocumentRealName, " ", "")))
	ctx.Header("Content-Type", fileContentType)
	ctx.Header("Content-Length", fmt.Sprintf("%d", fileInfo.Size()))
	ctx.File(data.PathDestination)
}

func (DocumentDetailHttp *DocumentDetailHttp) GetAll(c *gin.Context) {
	// Custome Request Need to be here
	documentDetails, err := DocumentDetailHttp.documentDetailUsecase.GetAll(1000, 0, "")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, documentDetails)
}

func (DocumentDetailHttp *DocumentDetailHttp) GetAllByRequestId(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))

	documentDetails, err := DocumentDetailHttp.documentDetailUsecase.GetAllByRequestId(1000, 0, "", id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, documentDetails)
}

func (DocumentDetailHttp *DocumentDetailHttp) GetById(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	documentDetail, err := DocumentDetailHttp.documentDetailUsecase.GetById(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, documentDetail)
}

func (DocumentDetailHttp *DocumentDetailHttp) DeleteById(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	documentDetail, err := DocumentDetailHttp.documentDetailUsecase.RemoveById(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, documentDetail)
}

func (DocumentDetailHttp *DocumentDetailHttp) Create(c *gin.Context) {
	// Pertama, parse form-data

	if err := c.Request.ParseMultipartForm(30 << 40); err != nil { // 30 MB max memory
		c.JSON(http.StatusBadRequest, gin.H{"message": "Error parsing form data: " + err.Error()})
		return
	}
	// Kedua, ambil file dari form-data
	file, header, err := c.Request.FormFile("file")

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Error retrieving the file: " + err.Error()})
		return
	}

	defer file.Close()

	// Ketiga, simpan file yang sudah diambil dari form-data

	idRequest := c.Request.FormValue("IdRequest")
	documentNotes := c.Request.FormValue("DocumentNotes")
	documentRealname := header.Filename
	documentExtensions := strings.Split(header.Filename, ".")[1]
	pathDestination := filepath.Join(appConfig.GetConfig().App.UploadFolder, idRequest, header.Filename)
	pathDestinationDir := filepath.Join(appConfig.GetConfig().App.UploadFolder, idRequest)

	// fmt.Println("destination >>" + pathDestination)
	//buat directori jika belum ada
	errCreateDir := os.MkdirAll(pathDestinationDir, os.ModePerm)
	if errCreateDir != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Error creating directory: " + err.Error()})
		return
	}

	//simpan file ke storage / directory

	f, err := os.OpenFile(pathDestination, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Error opening the file: " + err.Error()})
		return
	}

	io.Copy(f, file)

	data := &DocumentDetail.DocumentDetail{
		Model:              gorm.Model{},
		IdRequest:          helpers.ParseToUint(idRequest),
		PathDestination:    pathDestination,
		DocumentRealName:   documentRealname,
		DocumentNotes:      documentNotes,
		DocumentExtensions: documentExtensions,
	}

	// fmt.Println(data)

	errSave := DocumentDetailHttp.documentDetailUsecase.CreateDocuments(data)

	if errSave != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"message": "Document Detail Created",
		"data":    data,
	})
}
