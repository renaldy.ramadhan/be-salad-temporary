package interfaces

import StagingItem "BE-Epson/domains/staging/entities"

type StagingItemRepository interface {
	FindAll(string, string) ([]StagingItem.StagingItem, error)
	FindById(id string) (*StagingItem.StagingItem, error)
	StoreOne(data *StagingItem.StagingItem) (*StagingItem.StagingItem, error)
	StoreBulk(data []StagingItem.StagingItem) ([]StagingItem.StagingItem, error)
}

type StagingItemUsecase interface {
	GetAll(string, string, string) ([]StagingItem.StagingItem, error)
	GetById(id string) (*StagingItem.StagingItem, error)
	CreateOne(data *StagingItem.StagingItem) (*StagingItem.StagingItem, error)
	CreateBulk(data []StagingItem.StagingItem) ([]StagingItem.StagingItem, error)
}
