package interfaces

import (
	"BE-Epson/domains/staging/entities"
	"BE-Epson/shared/models/responses/Part"
)

type SAPAdjustmentRepository interface {
	FindAll() (res []entities.SAPAdjustment, err error)
	Store(request *Part.GetOneRequestResponse, costCenter string) (res []entities.SAPAdjustment, err error)
}

type SAPAdjustmentUsecase interface {
	GetAllSAPAdjustment() (res []entities.SAPAdjustment, err error)
	CreateSAPAdjustment(request *Part.GetOneRequestResponse, costCenter string) (res []entities.SAPAdjustment, err error)
}
