package interfaces

import (
	OrganizationApprovalRoute "BE-Epson/domains/staging/entities"
)

type OrganizationApprovalRouteRepository interface {
	FindAll() ([]OrganizationApprovalRoute.OrganizationApprovalRoute, error)
	FindById(id string) (*OrganizationApprovalRoute.OrganizationApprovalRoute, error)
	StoreOne(data *OrganizationApprovalRoute.OrganizationApprovalRoute) (*OrganizationApprovalRoute.OrganizationApprovalRoute, error)
	StoreBulk(data []OrganizationApprovalRoute.OrganizationApprovalRoute) ([]OrganizationApprovalRoute.OrganizationApprovalRoute, error)
	FindAllOrganizationApprovalByNik(nik string, level bool, id *uint) ([]OrganizationApprovalRoute.OrganizationApprovalRoute, error)
	CheckDataIsApprover(string2 string) (bool, error)
}

type OrganizationApprovalRouteUsecase interface {
	GetAll() ([]OrganizationApprovalRoute.OrganizationApprovalRoute, error)
	GetById(id string) (*OrganizationApprovalRoute.OrganizationApprovalRoute, error)
	CreateOne(data *OrganizationApprovalRoute.OrganizationApprovalRoute) (*OrganizationApprovalRoute.OrganizationApprovalRoute, error)
	CreateBulk(data []OrganizationApprovalRoute.OrganizationApprovalRoute) ([]OrganizationApprovalRoute.OrganizationApprovalRoute, error)
	GetAllOrganizationApprovalByNik(string, bool, *uint) ([]OrganizationApprovalRoute.OrganizationApprovalRoute, error)
	CheckDataIsApprover(approver string) (bool, error)
}
