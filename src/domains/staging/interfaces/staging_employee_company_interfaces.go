package interfaces

import "BE-Epson/domains/staging/entities"

type StagingEmployeeCompanyRepository interface {
	FindCostCenterByNik(nik string) (stagingEmployeeCompanies *entities.StagingEmployeeCompany, err error)
}
