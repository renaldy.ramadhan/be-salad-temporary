package interfaces

import (
	StagingPlant "BE-Epson/domains/staging/entities"
)

type PlantRepository interface {
	FindAll() ([]StagingPlant.StagingPlant, error)
	FindLocationByPlantId(id string) ([]StagingPlant.StagingPlant, error)
}

type PlantUseCase interface {
	GetAllPlant() ([]StagingPlant.StagingPlant, error)
	GetLocationByPlantId(id string) ([]StagingPlant.StagingPlant, error)
}
