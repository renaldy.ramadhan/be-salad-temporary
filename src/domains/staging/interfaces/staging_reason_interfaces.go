package interfaces

import StagingReason "BE-Epson/domains/staging/entities"

type StagingReasonRepository interface {
	FindAll(string) ([]StagingReason.StagingReason, error)
	FindById(id uint) (*StagingReason.StagingReason, error)
	StoreOne(data *StagingReason.StagingReason) (*StagingReason.StagingReason, error)
	StoreBulk(data []StagingReason.StagingReason) ([]StagingReason.StagingReason, error)
}

type StagingReasonUsecase interface {
	GetAll(string) ([]StagingReason.StagingReason, error)
	GetById(id uint) (*StagingReason.StagingReason, error)
	CreateOne(data *StagingReason.StagingReason) (*StagingReason.StagingReason, error)
	CreateBulk(data []StagingReason.StagingReason) ([]StagingReason.StagingReason, error)
}
