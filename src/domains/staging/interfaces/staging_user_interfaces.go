package interfaces

import (
	StagingUser "BE-Epson/domains/staging/entities"
	UserManagement "BE-Epson/domains/user/entities"
)

type StagingUserRepository interface {
	FindAll(query string) ([]StagingUser.StagingUser, error)
	FindById(id string) (*StagingUser.StagingUser, error)
	StoreOne(data *StagingUser.StagingUser) (*StagingUser.StagingUser, error)
	StoreBulk(data []StagingUser.StagingUser) ([]StagingUser.StagingUser, error)
	FindApprovers() ([]StagingUser.StagingUser, error)
	FindByNik(nik string) (*StagingUser.StagingUser, error)
	FindByEmail(email string) (res *StagingUser.StagingUser, err error)
	FindUserByNik(nik string) (*UserManagement.User, error)
	Update(data *StagingUser.StagingUser, nik string) (*StagingUser.StagingUser, error)
	InsertUser(data *UserManagement.User) (*UserManagement.User, error)
	UpdateUser(data *UserManagement.User, nik string) (*UserManagement.User, error)
}

type StagingUserUsecase interface {
	GetAll(query string) ([]StagingUser.StagingUser, error)
	GetById(id string) (*StagingUser.StagingUser, error)
	CreateOne(data *StagingUser.StagingUser) (*StagingUser.StagingUser, error)
	CreateBulk(data []StagingUser.StagingUser) ([]StagingUser.StagingUser, error)
	GetApprovers() ([]StagingUser.StagingUser, error)
}
