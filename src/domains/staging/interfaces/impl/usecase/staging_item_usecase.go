package usecase

import (
	RequestInterfaces "BE-Epson/domains/part/interfaces"
	StagingItem "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
)

type stagingItemUsecase struct {
	stagingItemRepository interfaces.StagingItemRepository
	stagingExternalitems  RequestInterfaces.GetStagingItemExternalRepo
}

func NewStagingItemUsecase(stagingItemRepository interfaces.StagingItemRepository, stagingExternalitems RequestInterfaces.GetStagingItemExternalRepo) *stagingItemUsecase {
	return &stagingItemUsecase{
		stagingItemRepository: stagingItemRepository,
		stagingExternalitems:  stagingExternalitems,
	}
}

func (StagingItemUsecase *stagingItemUsecase) GetAll(search, pkCode, locationCode string) ([]StagingItem.StagingItem, error) {
	var systemQuantity uint
	stagingItems, err := StagingItemUsecase.stagingItemRepository.FindAll(search, pkCode)
	if err != nil {
		return nil, err
	}
	stagingItemExternals, err := StagingItemUsecase.stagingExternalitems.FindAll(locationCode)
	for index, item := range stagingItems {
		for _, external := range stagingItemExternals {
			if external.ItemCode == item.ItemCode && external.LocationCode == locationCode {
				systemQuantity = external.SystemQuantity
			}
		}
		stagingItems[index].SystemQuantity = systemQuantity
	}
	return stagingItems, nil
}

func (StagingItemUsecase *stagingItemUsecase) GetById(Id string) (*StagingItem.StagingItem, error) {
	stagingItems, err := StagingItemUsecase.stagingItemRepository.FindById(Id)
	if err != nil {
		return nil, err
	}
	return stagingItems, nil
}

func (StagingItemUsecase *stagingItemUsecase) CreateOne(data *StagingItem.StagingItem) (resData *StagingItem.StagingItem, err error) {
	resData, err = StagingItemUsecase.stagingItemRepository.StoreOne(data)
	if err != nil {
		return nil, err
	}
	return resData, nil
}

func (StagingItemUsecase *stagingItemUsecase) CreateBulk(data []StagingItem.StagingItem) (resData []StagingItem.StagingItem, err error) {
	resData, err = StagingItemUsecase.stagingItemRepository.StoreBulk(data)
	if err != nil {
		return nil, err
	}
	return resData, nil
}
