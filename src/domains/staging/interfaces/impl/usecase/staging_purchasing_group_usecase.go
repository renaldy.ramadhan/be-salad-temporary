package usecase

import (
	StagingPurchasingGroup "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
	Const "BE-Epson/shared/errors"
	"errors"
)

type stagingPurchasingGroupUsecase struct {
	stagingPurchasingGroupRepository interfaces.StagingPurchasingGroupRepository
}

func NewStagingPurchasingGroupUsecase(stagingPurchasingGroupRepository interfaces.StagingPurchasingGroupRepository) *stagingPurchasingGroupUsecase {
	return &stagingPurchasingGroupUsecase{stagingPurchasingGroupRepository: stagingPurchasingGroupRepository}
}

func (StagingPurchasingGroupUsecase *stagingPurchasingGroupUsecase) GetAll() ([]StagingPurchasingGroup.StagingPurchasingGroup, error) {
	stagingPurchasingGroups, err := StagingPurchasingGroupUsecase.stagingPurchasingGroupRepository.FindAll()
	if err != nil {
		return nil, err
	}
	return stagingPurchasingGroups, nil
}

func (StagingPurchasingGroupUsecase *stagingPurchasingGroupUsecase) GetById(id uint) (*StagingPurchasingGroup.StagingPurchasingGroup, error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	data, err := StagingPurchasingGroupUsecase.stagingPurchasingGroupRepository.FindById(id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (StagingPurchasingGroupUsecase *stagingPurchasingGroupUsecase) CreateOne(data *StagingPurchasingGroup.StagingPurchasingGroup) (resData *StagingPurchasingGroup.StagingPurchasingGroup, err error) {
	resData, err = StagingPurchasingGroupUsecase.stagingPurchasingGroupRepository.StoreOne(data)
	if err != nil {
		return nil, err
	}
	return resData, nil
}

func (StagingPurchasingGroupUsecase *stagingPurchasingGroupUsecase) CreateBulk(data []StagingPurchasingGroup.StagingPurchasingGroup) (resData []StagingPurchasingGroup.StagingPurchasingGroup, err error) {
	resData, err = StagingPurchasingGroupUsecase.stagingPurchasingGroupRepository.StoreBulk(data)
	if err != nil {
		return nil, err
	}
	return resData, nil
}

func (StagingPurchasingGroupUsecase *stagingPurchasingGroupUsecase) UpdateById(data *StagingPurchasingGroup.StagingPurchasingGroup, id string) (*StagingPurchasingGroup.StagingPurchasingGroup, error) {
	result, err := StagingPurchasingGroupUsecase.stagingPurchasingGroupRepository.Update(data, id)
	if err != nil {
		return nil, err
	}

	return result, nil
}
