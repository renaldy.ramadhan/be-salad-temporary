package usecase

import (
	StagingReason "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/helpers"
	"errors"
)

type stagingReasonUsecase struct {
	stagingReasonRepository interfaces.StagingReasonRepository
}

func NewStagingReasonUsecase(stagingReasonRepository interfaces.StagingReasonRepository) *stagingReasonUsecase {
	return &stagingReasonUsecase{stagingReasonRepository: stagingReasonRepository}
}

func (StagingReasonUsecase *stagingReasonUsecase) GetAll(types string) ([]StagingReason.StagingReason, error) {
	if types == "" {
		return nil, errors.New(Const.TYPE_IS_REQUIRED)
	}
	if flag := helpers.IsReasonTypes(types); flag != true {
		return nil, errors.New(Const.TYPE_IS_NOT_VALID)
	}
	stagingReasons, err := StagingReasonUsecase.stagingReasonRepository.FindAll(types)
	if err != nil {
		return nil, err
	}
	return stagingReasons, nil
}

func (StagingReasonUsecase *stagingReasonUsecase) GetById(id uint) (*StagingReason.StagingReason, error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	data, err := StagingReasonUsecase.stagingReasonRepository.FindById(id)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (StagingReasonUsecase *stagingReasonUsecase) CreateOne(data *StagingReason.StagingReason) (resData *StagingReason.StagingReason, err error) {
	resData, err = StagingReasonUsecase.stagingReasonRepository.StoreOne(data)
	if err != nil {
		return nil, err
	}
	return resData, nil
}

func (StagingReasonUsecase *stagingReasonUsecase) CreateBulk(data []StagingReason.StagingReason) (resData []StagingReason.StagingReason, err error) {
	resData, err = StagingReasonUsecase.stagingReasonRepository.StoreBulk(data)
	if err != nil {
		return nil, err
	}
	return resData, nil
}
