package usecase

import (
	StagingPlant "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
)

type stagingPlantUseCase struct {
	stagingPlantRepo interfaces.PlantRepository
}

func NewStagingPlantUseCase(plantRepo interfaces.PlantRepository) *stagingPlantUseCase {
	return &stagingPlantUseCase{plantRepo}
}

func (PlantUc *stagingPlantUseCase) GetAllPlant() ([]StagingPlant.StagingPlant, error) {
	plants, err := PlantUc.stagingPlantRepo.FindAll()
	if err != nil {
		return nil, err
	}
	return plants, nil
}

func (PlantUc *stagingPlantUseCase) GetLocationByPlantId(id string) ([]StagingPlant.StagingPlant, error) {
	plants, err := PlantUc.stagingPlantRepo.FindLocationByPlantId(id)
	if err != nil {
		return nil, err
	}
	return plants, nil
}
