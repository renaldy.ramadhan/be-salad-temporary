package usecase

import (
	StagingUser "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
	Const "BE-Epson/shared/errors"
	"gopkg.in/errgo.v2/errors"
)

type stagingUserUsecase struct {
	stagingUserRepository interfaces.StagingUserRepository
}

func NewStagingUserUsecase(stagingUserRepository interfaces.StagingUserRepository) *stagingUserUsecase {
	return &stagingUserUsecase{
		stagingUserRepository: stagingUserRepository,
	}
}

func (StagingUserUsecase *stagingUserUsecase) GetAll(query string) ([]StagingUser.StagingUser, error) {
	res, err := StagingUserUsecase.stagingUserRepository.FindAll(query)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (StagingUserUsecase *stagingUserUsecase) GetById(id string) (*StagingUser.StagingUser, error) {
	if id == "" {
		return nil, errors.New(Const.USERNAME_EMPTY)
	}
	res, err := StagingUserUsecase.stagingUserRepository.FindById(id)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (StagingUserUsecase *stagingUserUsecase) CreateOne(data *StagingUser.StagingUser) (*StagingUser.StagingUser, error) {
	res, err := StagingUserUsecase.stagingUserRepository.StoreOne(data)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (StagingUserUsecase *stagingUserUsecase) CreateBulk(data []StagingUser.StagingUser) ([]StagingUser.StagingUser, error) {
	res, err := StagingUserUsecase.stagingUserRepository.StoreBulk(data)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (StagingUserUsecase *stagingUserUsecase) GetApprovers() ([]StagingUser.StagingUser, error) {
	res, err := StagingUserUsecase.stagingUserRepository.FindApprovers()
	if err != nil {
		return nil, err
	}
	return res, nil
}
