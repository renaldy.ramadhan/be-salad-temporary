package usecase

import (
	OrganizationApprovalRoute "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
	Const "BE-Epson/shared/errors"
	"errors"
)

type organizationApprovalRouteUsecase struct {
	organizationApprovalRouteRepository interfaces.OrganizationApprovalRouteRepository
}

func NewOrganizationApprovalRouteUsecase(organizationApprovalRouteRepository interfaces.OrganizationApprovalRouteRepository) *organizationApprovalRouteUsecase {
	return &organizationApprovalRouteUsecase{organizationApprovalRouteRepository: organizationApprovalRouteRepository}
}

func (OrganizationApprovalRouteUsecase *organizationApprovalRouteUsecase) GetAll() ([]OrganizationApprovalRoute.OrganizationApprovalRoute, error) {
	organizationApprovalRoutes, err := OrganizationApprovalRouteUsecase.organizationApprovalRouteRepository.FindAll()
	if err != nil {
		return nil, err
	}
	return organizationApprovalRoutes, nil
}

func (OrganizationApprovalRouteUsecase *organizationApprovalRouteUsecase) GetById(id string) (*OrganizationApprovalRoute.OrganizationApprovalRoute, error) {
	if id == "" {
		return nil, errors.New(Const.NIK_EMPTY)
	}
	organizationApprovalRoute, err := OrganizationApprovalRouteUsecase.organizationApprovalRouteRepository.FindById(id)
	if err != nil {
		return nil, err
	}
	return organizationApprovalRoute, nil
}

func (OrganizationApprovalRouteUsecase *organizationApprovalRouteUsecase) CreateOne(data *OrganizationApprovalRoute.OrganizationApprovalRoute) (*OrganizationApprovalRoute.OrganizationApprovalRoute, error) {
	organizationApprovalRoute, err := OrganizationApprovalRouteUsecase.organizationApprovalRouteRepository.StoreOne(data)
	if err != nil {
		return nil, err
	}
	return organizationApprovalRoute, nil
}

func (OrganizationApprovalRouteUsecase *organizationApprovalRouteUsecase) CreateBulk(data []OrganizationApprovalRoute.OrganizationApprovalRoute) ([]OrganizationApprovalRoute.OrganizationApprovalRoute, error) {
	organizationApprovalRoutes, err := OrganizationApprovalRouteUsecase.organizationApprovalRouteRepository.StoreBulk(data)
	if err != nil {
		return nil, err
	}
	return organizationApprovalRoutes, nil
}

func (OrganizationApprovalRouteUsecase *organizationApprovalRouteUsecase) GetAllOrganizationApprovalByNik(nik string, level bool, LevelSeq *uint) ([]OrganizationApprovalRoute.OrganizationApprovalRoute, error) {
	if nik == "" {
		return nil, errors.New(Const.NIK_EMPTY)
	}
	organizationApprovalRoutes, err := OrganizationApprovalRouteUsecase.organizationApprovalRouteRepository.FindAllOrganizationApprovalByNik(nik, level, LevelSeq)
	if err != nil {
		return nil, err
	}
	return organizationApprovalRoutes, nil
}

func (OrganizationApprovalRouteUsecase *organizationApprovalRouteUsecase) CheckDataIsApprover(Nik string) (bool, error) {
	if Nik == "" {
		return false, errors.New(Const.NIK_EMPTY)
	}
	isApprover, err := OrganizationApprovalRouteUsecase.organizationApprovalRouteRepository.CheckDataIsApprover(Nik)
	if err != nil {
		return false, err
	}
	return isApprover, nil
}
