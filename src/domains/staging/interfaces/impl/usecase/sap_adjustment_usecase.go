package usecase

import (
	"BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
	"BE-Epson/shared/models/responses/Part"
)

type SAPAdjustmentUsecase struct {
	SAPAdjustmentRepository interfaces.SAPAdjustmentRepository
}

func NewSAPAdjustmentUsecase(SAPAdjustmentRepository interfaces.SAPAdjustmentRepository) *SAPAdjustmentUsecase {
	return &SAPAdjustmentUsecase{SAPAdjustmentRepository: SAPAdjustmentRepository}
}

func (usecase *SAPAdjustmentUsecase) GetAllSAPAdjustment() (res []entities.SAPAdjustment, err error) {
	res, err = usecase.SAPAdjustmentRepository.FindAll()
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (usecase *SAPAdjustmentUsecase) CreateSAPAdjustment(request *Part.GetOneRequestResponse, costCenter string) (res []entities.SAPAdjustment, err error) {
	res, err = usecase.SAPAdjustmentRepository.Store(request, costCenter)
	if err != nil {
		return nil, err
	}
	return res, nil
}
