package repository

import (
	OrganizationApprovalRoute "BE-Epson/domains/staging/entities"
	"fmt"

	"gopkg.in/errgo.v2/errors"
	"gorm.io/gorm"
)

type organizationApprovalRouteRepository struct {
	DB *gorm.DB
}

func NewOrganizationApprovalRouteRepository(DB *gorm.DB) *organizationApprovalRouteRepository {
	return &organizationApprovalRouteRepository{DB: DB}
}

func (OrganizationApprovalRouteRepository *organizationApprovalRouteRepository) FindAll() (organizationApprovalRoutes []OrganizationApprovalRoute.OrganizationApprovalRoute, err error) {
	err = OrganizationApprovalRouteRepository.DB.Find(&organizationApprovalRoutes).Error
	return organizationApprovalRoutes, err
}

func (OrganizationApprovalRouteRepository *organizationApprovalRouteRepository) FindById(id string) (organizationApprovalRoute *OrganizationApprovalRoute.OrganizationApprovalRoute, err error) {
	err = OrganizationApprovalRouteRepository.DB.First(&organizationApprovalRoute, id).Error
	if err != nil {
		return nil, err
	}
	return organizationApprovalRoute, err
}

func (OrganizationApprovalRouteRepository *organizationApprovalRouteRepository) StoreOne(data *OrganizationApprovalRoute.OrganizationApprovalRoute) (organizationApprovalRoute *OrganizationApprovalRoute.OrganizationApprovalRoute, err error) {
	err = OrganizationApprovalRouteRepository.DB.Create(&data).Error
	if err != nil {
		return nil, err
	}
	return data, err
}

func (OrganizationApprovalRouteRepository *organizationApprovalRouteRepository) StoreBulk(data []OrganizationApprovalRoute.OrganizationApprovalRoute) (organizationApprovalRoutes []OrganizationApprovalRoute.OrganizationApprovalRoute, err error) {
	err = OrganizationApprovalRouteRepository.DB.Create(&data).Error
	if err != nil {
		return nil, errors.New("Error when insert bulk data")
	}
	return data, nil
}

func (OrganizationApprovalRouteRepository *organizationApprovalRouteRepository) FindAllOrganizationApprovalByNik(nik string, level bool, LevelSeq *uint) (organizationApprovalRoutes []OrganizationApprovalRoute.OrganizationApprovalRoute, err error) {
	data := &OrganizationApprovalRoute.OrganizationApprovalRoute{}
	if OrganizationApprovalRouteRepository.DB.Where("nik = ?", nik).First(&data).Error != nil {
		return nil, errors.New("Error when get data")
	}
	if level {
		//query := OrganizationApprovalRouteRepository.DB.Distinct("nik", "line_code", "wf_name", "wf_seq", "name", "max(wf_level) as wf_level")
		////Select("nik, line_code, wf_name, wf_seq, name, max(wf_level) as wf_level")
		//if *LevelSeq == 2 {
		//	query = query.Where("line_code = ? AND wf_level IN (1,2)", data.LineCode)
		//} else if *LevelSeq == 3 {
		//	query = query.Where("line_code = ? AND wf_level IN (1,2,3)", data.LineCode)
		//}
		//query = query.Group("nik, line_code, wf_name, wf_seq, name").Order("wf_level desc")
		//err = query.Find(&organizationApprovalRoutes).Error
		levels := make([]uint, *LevelSeq)
		for i := 1; i <= int(*LevelSeq); i++ {
			levels[i-1] = uint(i)
		}
		fmt.Println(levels)
		//Proceed with the query
		err = OrganizationApprovalRouteRepository.DB.Distinct("nik", "line_code", "wf_name", "wf_seq", "name").
			Select("nik, line_code, wf_name, wf_seq, name, max(wf_level) as wf_level").
			Where("line_code = ? AND wf_level IN (?)", data.LineCode, levels).
			Group("nik, line_code, wf_name, wf_seq, name").
			Order("wf_level desc").
			Find(&organizationApprovalRoutes).Error
		if err != nil {
			return nil, err
		}
		return organizationApprovalRoutes, err
	}
	if err != nil {
		return nil, err
	}
	return organizationApprovalRoutes, err
}

func (OrganizationApprovalRouteRepository *organizationApprovalRouteRepository) CheckDataIsApprover(string2 string) (bool, error) {
	data := &OrganizationApprovalRoute.OrganizationApprovalRoute{}
	if OrganizationApprovalRouteRepository.DB.Where("nik = ? AND (wf_level >= 1)", string2).Find(&data).Error != nil {
		return false, errors.New("Error when get data")
	}
	if data.Nik == "" {
		return false, errors.New("Data not found")
	}
	return true, nil
}
