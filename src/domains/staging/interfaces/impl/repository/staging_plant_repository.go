package repository

import (
	StagingPlant "BE-Epson/domains/staging/entities"
	"gorm.io/gorm"
)

type stagingPlantRepository struct {
	DB *gorm.DB
}

func NewStagingPlantRepository(DB *gorm.DB) *stagingPlantRepository {
	return &stagingPlantRepository{DB}
}

func (PlanRepo *stagingPlantRepository) FindAll() (data []StagingPlant.StagingPlant, err error) {
	err = PlanRepo.DB.Find(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (PlanRepo *stagingPlantRepository) FindLocationByPlantId(id string) (data []StagingPlant.StagingPlant, err error) {
	err = PlanRepo.DB.Where("plant_code = ?", id).Find(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}
