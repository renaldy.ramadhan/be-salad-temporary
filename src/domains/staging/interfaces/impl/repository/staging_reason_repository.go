package repository

import (
	StagingReason "BE-Epson/domains/staging/entities"
	"gorm.io/gorm"
)

type stagingReasonRepository struct {
	DB *gorm.DB
}

func NewStagingReasonRepository(DB *gorm.DB) *stagingReasonRepository {
	return &stagingReasonRepository{DB: DB}
}

func (StagingReasonRepo *stagingReasonRepository) FindAll(types string) (stagingReasons []StagingReason.StagingReason, err error) {
	err = StagingReasonRepo.DB.Find(&stagingReasons, "reason_type = ?", types).Error
	if err != nil {
		return nil, err
	}
	return stagingReasons, nil
}

func (StagingReasonRepo *stagingReasonRepository) FindById(id uint) (stagingReason *StagingReason.StagingReason, err error) {
	err = StagingReasonRepo.DB.Where("id = ?", id).First(&stagingReason).Error
	if err != nil {
		return nil, err
	}
	return stagingReason, nil
}

func (StagingReasonRepo *stagingReasonRepository) StoreOne(data *StagingReason.StagingReason) (*StagingReason.StagingReason, error) {
	err := StagingReasonRepo.DB.Create(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (StagingReasonRepo *stagingReasonRepository) StoreBulk(data []StagingReason.StagingReason) ([]StagingReason.StagingReason, error) {
	err := StagingReasonRepo.DB.Create(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}
