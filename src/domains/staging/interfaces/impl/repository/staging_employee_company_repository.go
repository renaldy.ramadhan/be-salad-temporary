package repository

import (
	"BE-Epson/domains/staging/entities"
	"gorm.io/gorm"
)

type stagingEmployeeCompany struct {
	DB *gorm.DB
}

func NewStagingEmployeeCompanyRepository(DB *gorm.DB) *stagingEmployeeCompany {
	return &stagingEmployeeCompany{DB: DB}
}

func (StagingEmployeeCompanyRepository *stagingEmployeeCompany) FindCostCenterByNik(nik string) (stagingEmployeeCompanies *entities.StagingEmployeeCompany, err error) {
	err = StagingEmployeeCompanyRepository.DB.Find(&stagingEmployeeCompanies, "no_pegawai = ?", nik).Error
	if err != nil {
		return nil, err
	}
	return stagingEmployeeCompanies, err
}
