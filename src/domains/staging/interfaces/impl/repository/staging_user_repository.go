package repository

import (
	StagingUser "BE-Epson/domains/staging/entities"
	UserManagement "BE-Epson/domains/user/entities"
	constErr "BE-Epson/shared/errors"
	"errors"

	"gorm.io/gorm"
)

type stagingUserRepository struct {
	DB *gorm.DB
}

func NewStagingUserRepository(DB *gorm.DB) *stagingUserRepository {
	return &stagingUserRepository{
		DB: DB,
	}
}

func (StagingUserRepository *stagingUserRepository) buildQuery(query *gorm.DB, param string) *gorm.DB {
	if param != "" {
		search := "%" + param + "%"
		query = query.Where("nik LIKE ?", search)
	}

	return query
}

func (StagingUserRepository *stagingUserRepository) FindAll(query string) ([]StagingUser.StagingUser, error) {
	var stagingUser []StagingUser.StagingUser
	db := StagingUserRepository.buildQuery(StagingUserRepository.DB, query)
	err := db.Find(&stagingUser).Error
	if err != nil {
		return nil, err
	}
	return stagingUser, nil
}

func (StagingUserRepository *stagingUserRepository) FindById(id string) (res *StagingUser.StagingUser, err error) {
	if err = StagingUserRepository.DB.Where("user_name = ?", id).First(&res).Error; err != nil {
		return nil, err
	}
	return res, nil
}

func (StagingUserRepository *stagingUserRepository) StoreOne(data *StagingUser.StagingUser) (res *StagingUser.StagingUser, err error) {
	if data.Name == "" {
		return nil, errors.New(constErr.USERNAME_EMPTY)
	}
	if err = StagingUserRepository.DB.Create(&data).Error; err != nil {
		return nil, err
	}
	return data, nil
}

func (StagingUserRepository *stagingUserRepository) InsertUser(data *UserManagement.User) (*UserManagement.User, error) {
	if err := StagingUserRepository.DB.Create(&data).Error; err != nil {
		return nil, err
	}

	return data, nil
}

func (StagingUserRepository *stagingUserRepository) StoreBulk(data []StagingUser.StagingUser) (res []StagingUser.StagingUser, err error) {
	if err = StagingUserRepository.DB.Create(&data).Error; err != nil {
		return nil, err
	}
	return data, nil
}

func (StagingUserRepository *stagingUserRepository) FindApprovers() (res []StagingUser.StagingUser, err error) {
	if err = StagingUserRepository.DB.Model(&StagingUser.StagingUser{}).Where("role = ?", "approver").Find(&res).Error; err != nil {
		return nil, err
	}
	return res, nil
}

func (StagingUserRepository *stagingUserRepository) FindByNik(nik string) (res *StagingUser.StagingUser, err error) {
	if err = StagingUserRepository.DB.Where("nik = ?", nik).First(&res).Error; err != nil {
		return nil, err
	}
	return res, nil
}

func (StagingUserRepository *stagingUserRepository) FindByEmail(email string) (res *StagingUser.StagingUser, err error) {
	if err = StagingUserRepository.DB.Find(&res, "email = ?", email).Error; err != nil {
		return nil, err
	}
	return res, nil
}

func (StagingUserRepository *stagingUserRepository) FindUserByNik(nik string) (res *UserManagement.User, err error) {
	if err = StagingUserRepository.DB.Where("nik = ?", nik).First(&res).Error; err != nil {
		return nil, err
	}
	return res, nil
}

func (StagingUserRepository *stagingUserRepository) Update(data *StagingUser.StagingUser, nik string) (*StagingUser.StagingUser, error) {
	err := StagingUserRepository.DB.Where("nik = ?", nik).Updates(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (StagingUserRepository *stagingUserRepository) UpdateUser(data *UserManagement.User, nik string) (*UserManagement.User, error) {
	err := StagingUserRepository.DB.Where("nik = ?", nik).Updates(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil

}
