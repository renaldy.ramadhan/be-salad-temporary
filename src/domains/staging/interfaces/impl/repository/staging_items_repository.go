package repository

import (
	StagingItem "BE-Epson/domains/staging/entities"
	"BE-Epson/shared/helpers"
	"errors"
	"gorm.io/gorm"
)

type stagingItemsRepository struct {
	DB *gorm.DB
}

func NewStagingItemsRepository(DB *gorm.DB) *stagingItemsRepository {
	return &stagingItemsRepository{DB: DB}
}

func (StagingItemsRepo *stagingItemsRepository) FindAll(search, pkCode string) (stagingItem []StagingItem.StagingItem, err error) {
	query := StagingItemsRepo.DB
	if search != "" {
		query = query.Where("LOWER(item_code) LIKE ? OR LOWER(item_name) LIKE ?", "%"+helpers.ToLower(search)+"%", "%"+helpers.ToLower(search)+"%")
	}
	if pkCode != "" {
		query = query.Where("purchasing_group_code_fk = ?", pkCode)
	} else {
		return nil, errors.New("Item is not found in this purchasing group code!")
	}
	err = query.Find(&stagingItem).Error
	if err != nil {
		return nil, err
	}
	return stagingItem, nil
}

func (StagingItemsRepo *stagingItemsRepository) FindById(id string) (stagingItem *StagingItem.StagingItem, err error) {
	err = StagingItemsRepo.DB.First(&stagingItem, "item_code = ?", id).Error
	if err != nil {
		return nil, err
	}
	return stagingItem, nil
}

func (StagingItemsRepo *stagingItemsRepository) StoreOne(data *StagingItem.StagingItem) (*StagingItem.StagingItem, error) {
	err := StagingItemsRepo.DB.Create(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (StagingItemsRepo *stagingItemsRepository) StoreBulk(data []StagingItem.StagingItem) ([]StagingItem.StagingItem, error) {
	err := StagingItemsRepo.DB.Create(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}
