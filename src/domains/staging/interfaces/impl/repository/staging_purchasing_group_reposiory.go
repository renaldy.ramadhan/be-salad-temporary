package repository

import (
	StagingPurchasingGroup "BE-Epson/domains/staging/entities"
	"gorm.io/gorm"
)

type stagingPurchasingGroupRepository struct {
	DB *gorm.DB
}

func NewStagingPurchasingGroupRepository(DB *gorm.DB) *stagingPurchasingGroupRepository {
	return &stagingPurchasingGroupRepository{DB: DB}
}

func (StagingPurchasingGroupRepo *stagingPurchasingGroupRepository) FindAll() (stagingPurchasingGroups []StagingPurchasingGroup.StagingPurchasingGroup, err error) {
	err = StagingPurchasingGroupRepo.DB.Find(&stagingPurchasingGroups).Error
	if err != nil {
		return nil, err
	}
	return stagingPurchasingGroups, nil
}

func (StagingPurchasingGroupRepo *stagingPurchasingGroupRepository) FindById(id uint) (stagingPurchasingGroup *StagingPurchasingGroup.StagingPurchasingGroup, err error) {
	err = StagingPurchasingGroupRepo.DB.Where("id = ?", id).First(&stagingPurchasingGroup).Error
	if err != nil {
		return nil, err
	}
	return stagingPurchasingGroup, nil
}

func (StagingPurchasingGroupRepo *stagingPurchasingGroupRepository) StoreOne(data *StagingPurchasingGroup.StagingPurchasingGroup) (*StagingPurchasingGroup.StagingPurchasingGroup, error) {
	err := StagingPurchasingGroupRepo.DB.Create(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (StagingPurchasingGroupRepo *stagingPurchasingGroupRepository) StoreBulk(data []StagingPurchasingGroup.StagingPurchasingGroup) ([]StagingPurchasingGroup.StagingPurchasingGroup, error) {
	err := StagingPurchasingGroupRepo.DB.Create(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (StagingPurchasingGroupRepo *stagingPurchasingGroupRepository) Update(data *StagingPurchasingGroup.StagingPurchasingGroup, id string) (*StagingPurchasingGroup.StagingPurchasingGroup, error) {
	err := StagingPurchasingGroupRepo.DB.Where("purchasing_group_code = ?", id).Updates(&data).Error
	if err != nil {
		return nil, err
	}

	return data, nil
}
