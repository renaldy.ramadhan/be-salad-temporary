package repository

import (
	"BE-Epson/domains/part/presistence"
	"BE-Epson/domains/staging/entities"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/responses/Part"
	"fmt"
	"gorm.io/gorm"
	"math"
	"strconv"
	"time"
)

type SAPAdjustmentRepository struct {
	DB *gorm.DB
}

func NewSAPAdjustmentRepository(DB *gorm.DB) *SAPAdjustmentRepository {
	return &SAPAdjustmentRepository{DB: DB}
}

func (repo *SAPAdjustmentRepository) FindAll() (res []entities.SAPAdjustment, err error) {
	err = repo.DB.Find(&res).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (repo *SAPAdjustmentRepository) Store(request *Part.GetOneRequestResponse, costCenter string) (res []entities.SAPAdjustment, err error) {
	var sequenceNumber string
	var stkAdj entities.SAPAdjustment
	var response []entities.SAPAdjustment
	location, _ := time.LoadLocation("Asia/Jakarta")
	for i, item := range request.Items {
		sequence := i + 1
		sequenceNumber = fmt.Sprintf("%05s", strconv.Itoa(sequence))

		dateStr := request.Request.RequestDate.In(location).Format("20060102")
		timeStr := request.Request.RequestDate.In(location).Format("150405")

		if request.Request.Type == presistence.Discrepancy {
			if math.Signbit(helpers.ParseFloat(item.RequestedQuantity)) {
				stkAdj.ITBH_BWART = request.Request.Reason.MvtNegative
			} else {
				stkAdj.ITBH_BWART = request.Request.Reason.MvtPositive
			}
		}

		if item.RequestedQuantity == item.ApprovedQuantity {
			stkAdj.ITBH_MENGE = helpers.ParseFloat(item.RequestedQuantity)
		} else {
			stkAdj.ITBH_MENGE = helpers.ParseFloat(item.ApprovedQuantity)
		}

		if request.Request.Type == presistence.Disposal {
			stkAdj.ITBH_LGORT = helpers.StringPointer(request.Request.DestroyLocation)
		} else {
			stkAdj.ITBH_LGORT = helpers.StringPointer(request.Request.LocationPlant.LocationCode)
		}

		stkAdj.ITBH_ADATE = dateStr
		stkAdj.ITBH_ATIME = timeStr
		stkAdj.ITBH_CDSEQ = sequenceNumber
		stkAdj.ITBH_BLDAT = helpers.StringPointer(dateStr)
		stkAdj.ITBH_BUDAT = helpers.StringPointer(dateStr)
		stkAdj.ITBH_GRUND = helpers.StringPointer(helpers.ParseToString(request.Request.IdReason))
		stkAdj.ITBH_WERKS = helpers.StringPointer(request.Request.LocationPlant.PlantCode)
		stkAdj.ITBH_MATNR = helpers.StringPointer(item.ItemCodeFk)
		stkAdj.ITBH_MEINS = helpers.StringPointer(item.UnitOfMeasure)
		stkAdj.ITBH_KOSTL = helpers.StringPointer(costCenter)
		stkAdj.ITBH_SGTXT = helpers.StringPointer(request.Request.PartRequestCode)
		stkAdj.ITBH_REGDT = time.Now().In(location)
		stkAdj.ITBH_UPDDT = time.Now().In(location)

		err = repo.DB.Create(&stkAdj).Error
		if err != nil {
			return nil, err
		}

		response = append(response, stkAdj)
	}
	return response, nil
}
