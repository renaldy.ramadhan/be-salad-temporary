package interfaces

import StagingPurchasingGroup "BE-Epson/domains/staging/entities"

type StagingPurchasingGroupRepository interface {
	FindAll() ([]StagingPurchasingGroup.StagingPurchasingGroup, error)
	FindById(id uint) (*StagingPurchasingGroup.StagingPurchasingGroup, error)
	StoreOne(data *StagingPurchasingGroup.StagingPurchasingGroup) (*StagingPurchasingGroup.StagingPurchasingGroup, error)
	StoreBulk(data []StagingPurchasingGroup.StagingPurchasingGroup) ([]StagingPurchasingGroup.StagingPurchasingGroup, error)
	Update(data *StagingPurchasingGroup.StagingPurchasingGroup, id string) (*StagingPurchasingGroup.StagingPurchasingGroup, error)
}

type StagingPurchasingGroupUsecase interface {
	GetAll() ([]StagingPurchasingGroup.StagingPurchasingGroup, error)
	GetById(id uint) (*StagingPurchasingGroup.StagingPurchasingGroup, error)
	CreateOne(data *StagingPurchasingGroup.StagingPurchasingGroup) (*StagingPurchasingGroup.StagingPurchasingGroup, error)
	CreateBulk(data []StagingPurchasingGroup.StagingPurchasingGroup) ([]StagingPurchasingGroup.StagingPurchasingGroup, error)
	UpdateById(data *StagingPurchasingGroup.StagingPurchasingGroup, id string) (*StagingPurchasingGroup.StagingPurchasingGroup, error)
}
