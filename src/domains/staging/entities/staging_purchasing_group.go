package entities

import "BE-Epson/shared/entities"

type StagingPurchasingGroup struct {
	PurchasingGroupCode string `gorm:"type:varchar(255);primaryKey" json:"PurchasingGroupCode"`
	Plant               string `gorm:"type:varchar(255);not null;" json:"Plant"`
	Manager             string `gorm:"type:varchar(255);not null;" json:"Manager"`
	entities.Timestamp
}
