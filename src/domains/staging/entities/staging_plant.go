package entities

import "BE-Epson/shared/entities"

type StagingPlant struct {
	LocationCode string `gorm:"type:varchar(255);not null;unique;primaryKey;" json:"LocationCode"`
	LocationName string `gorm:"not null;" json:"LocationName"`
	PlantCode    string `gorm:"not null;" json:"PlantCode"`
	entities.Timestamp
}
