package entities

type OrganizationApprovalRoute struct {
	Nik      string `gorm:"type:varchar(255);not null;" json:"Nik"`
	LineCode string `gorm:"type:varchar(255);not null;" json:"LineCode"`
	WfLevel  int    `gorm:"type:int;not null;" json:"WfLevel"`
	WfName   string `gorm:"type:varchar(255);not null;" json:"WfName"`
	WfSeq    int    `gorm:"type:int;" json:"WfSeq"`
	Name     string `gorm:"type:varchar(255);not null;" json:"Name"`
}
