package entities

import "time"

type SAPAdjustment struct {
	ITBH_ZA_DTTYP  string    `gorm:"not null;default:'IN032';column:ITBH_ZA_DTTYP" json:"ITBH_ZA_DTTYP"`
	ITBH_ZA_SYSTYP string    `gorm:"not null;default:'ESTOCK';column:ITBH_ZA_SYSTYP" json:"ITBH_ZA_SYSTYP"`
	ITBH_ZA_ADDKEY string    `gorm:"not null;default:'XX';column:ITBH_ZA_ADDKEY" json:"ITBH_ZA_ADDKEY"`
	ITBH_ADATE     string    `gorm:"not null;column:ITBH_ADATE" json:"ITBH_ADATE"`
	ITBH_ATIME     string    `gorm:"not null;column:ITBH_ATIME" json:"ITBH_ATIME"`
	ITBH_IFTYPE    string    `gorm:"not null;default:'I';column:ITBH_IFTYPE" json:"ITBH_IFTYPE"`
	ITBH_CDSEQ     string    `gorm:"not null;column:ITBH_CDSEQ" json:"ITBH_CDSEQ"`
	ITBH_PROCFLG   *string   `gorm:"column:ITBH_PROCFLG" json:"ITBH_PROCFLG"`
	ITBH_BLDAT     *string   `gorm:"column:ITBH_BLDAT" json:"ITBH_BLDAT"`
	ITBH_BUDAT     *string   `gorm:"column:ITBH_BUDAT" json:"ITBH_BUDAT"`
	ITBH_BWART     *string   `gorm:"column:ITBH_BWART" json:"ITBH_BWART"`
	ITBH_SOBKZ     *string   `gorm:"column:ITBH_SOBKZ" json:"ITBH_SOBKZ"`
	ITBH_GRUND     *string   `gorm:"column:ITBH_GRUND" json:"ITBH_GRUND"`
	ITBH_WERKS     *string   `gorm:"column:ITBH_WERKS" json:"ITBH_WERKS"`
	ITBH_LGORT     *string   `gorm:"column:ITBH_LGORT" json:"ITBH_LGORT"`
	ITBH_MATNR     *string   `gorm:"column:ITBH_MATNR" json:"ITBH_MATNR"`
	ITBH_MENGE     float64   `gorm:"column:ITBH_MENGE" json:"ITBH_MENGE"`
	ITBH_MEINS     *string   `gorm:"column:ITBH_MEINS" json:"ITBH_MEINS"`
	ITBH_KOSTL     *string   `gorm:"column:ITBH_KOSTL" json:"ITBH_KOSTL"`
	ITBH_AUFNR     *string   `gorm:"column:ITBH_AUFNR" json:"ITBH_AUFNR"`
	ITBH_SGTXT     *string   `gorm:"column:ITBH_SGTXT" json:"ITBH_SGTXT"`
	ITBH_REGDT     time.Time `gorm:"column:ITBH_REGDT" json:"ITBH_REGDT"`
	ITBH_UPDDT     time.Time `gorm:"column:ITBH_UPDDT" json:"ITBH_UPDDT"`
	ITBH_R3STS     *string   `gorm:"column:ITBH_R3STS" json:"ITBH_R3STS"`
	ITBH_R3UPDKEY1 *string   `gorm:"column:ITBH_R3UPDKEY1" json:"ITBH_R3UPDKEY1"`
	ITBH_R3UPDKEY2 *string   `gorm:"column:ITBH_R3UPDKEY2" json:"ITBH_R3UPDKEY2"`
	ITBH_R3UPDKEY3 *string   `gorm:"column:ITBH_R3UPDKEY3" json:"ITBH_R3UPDKEY3"`
	ITBH_R3ERRMSG  *string   `gorm:"column:ITBH_R3ERRMSG" json:"ITBH_R3ERRMSG"`
	ITBH_IEISTS    *string   `gorm:"column:ITBH_IEISTS" json:"ITBH_IEISTS"`
}

func (SAPAdjustment) TableName() string {
	return "T_ADX_ITBH_STK_ADJ"
}
