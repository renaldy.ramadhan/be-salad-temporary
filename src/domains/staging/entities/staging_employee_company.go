package entities

type StagingEmployeeCompany struct {
	NoPegawai  string `gorm:"not null" json:"NoPegawai"`
	CostCenter string `gorm:"not null" json:"CostCenter"`
}
