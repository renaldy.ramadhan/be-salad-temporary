package entities

type StagingItem struct {
	ItemCode               string                 `gorm:"type:varchar(50);primaryKey" json:"ItemCode" binding:"required"`
	ItemName               string                 `gorm:"type:varchar(100)" json:"ItemName" binding:"required"`
	GrossWeight            string                 `gorm:"type:string" json:"GrossWeight"`
	WeightUnit             string                 `gorm:"type:varchar(255)" json:"WeightUnit"`
	NetWeight              string                 `gorm:"type:string" json:"NetWeight"`
	UnitOfMeasure          string                 `gorm:"type:varchar(255)" json:"UnitOfMeasure"`
	PricePerUnit           string                 `gorm:"type:string" json:"PricePerUnit"`
	PurchasingGroupCodeFk  string                 `gorm:"type:varchar(255)" json:"PurchasingGroupCode"`
	SystemQuantity         uint                   `json:"SystemQuantity"`
	StagingPurchasingGroup StagingPurchasingGroup `gorm:"foreignKey:PurchasingGroupCodeFk;references:PurchasingGroupCode;" json:"-"`
}
