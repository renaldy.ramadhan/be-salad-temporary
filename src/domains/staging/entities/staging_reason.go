package entities

import "gorm.io/gorm"

type StagingReason struct {
	gorm.Model
	ReasonType  string  `gorm:"type:varchar(255);not null;" json:"Type"`
	ReasonName  string  `gorm:"type:varchar(255);not null;" json:"ReasonName"`
	Description string  `gorm:"type:varchar(255);not null;" json:"Description"`
	MvtNegative *string `gorm:"type:varchar(255);" json:"MvtNegative"`
	MvtPositive *string `gorm:"type:varchar(255);" json:"MvtPositive"`
}
