package entities

type StagingUser struct {
	Name       string  `gorm:"type:varchar(255);not null;" json:"Name"`
	Nik        string  `gorm:"type:varchar(255);not null;unique;primaryKey;" json:"Nik"`
	Department string  `gorm:"type:varchar(255);not null;" json:"Department"`
	Email      *string `gorm:"type:varchar(255);" json:"Email"`
}
