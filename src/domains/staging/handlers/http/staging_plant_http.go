package http

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	"BE-Epson/domains/staging/interfaces"
	"BE-Epson/domains/staging/interfaces/impl/repository"
	"BE-Epson/domains/staging/interfaces/impl/usecase"
	"BE-Epson/middlewares"
	"github.com/gin-gonic/gin"
	"net/http"
)

type StagingPlantHttp struct {
	stagingPlantUsecase interfaces.PlantUseCase
}

func NewStagingPlantHttp(router *gin.Engine, stagingUserRepository interfaces.StagingUserRepository) *StagingPlantHttp {
	handler := &StagingPlantHttp{
		stagingPlantUsecase: usecase.NewStagingPlantUseCase(
			repository.NewStagingPlantRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}
	plant := router.Group("/salad/api/plant")
	{
		plant.GET("/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAllPlant)
		plant.GET("/location/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetLocationByPlantId)
	}
	return handler
}

func (PlantHttp *StagingPlantHttp) GetAllPlant(c *gin.Context) {
	plant, err := PlantHttp.stagingPlantUsecase.GetAllPlant()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, plant)
}

func (PlantHttp *StagingPlantHttp) GetLocationByPlantId(c *gin.Context) {
	id := c.Param("id")
	plant, err := PlantHttp.stagingPlantUsecase.GetLocationByPlantId(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, plant)
}
