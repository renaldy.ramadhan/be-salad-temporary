package http

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	PartRepo "BE-Epson/domains/part/interfaces/impl/repository"
	StagingItem "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
	"BE-Epson/domains/staging/interfaces/impl/repository"
	"BE-Epson/domains/staging/interfaces/impl/usecase"
	"BE-Epson/middlewares"
	"github.com/gin-gonic/gin"
	"net/http"
)

type StagingItemHttp struct {
	stagingItemUsecase interfaces.StagingItemUsecase
}

func NewStagingItemHttp(router *gin.Engine, stagingUserRepository interfaces.StagingUserRepository) *StagingItemHttp {
	handler := &StagingItemHttp{
		stagingItemUsecase: usecase.NewStagingItemUsecase(
			repository.NewStagingItemsRepository(database.ConnectDatabase(Const.DB_SALAD)),
			PartRepo.NewRequestStagingItemsRepositoryExternal(database.ConnectDatabase(Const.EXTERNAL_DB))),
	}
	stagingAdmin := router.Group("/salad/api/staging-items")
	{
		stagingAdmin.GET("/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAll)
		stagingAdmin.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetById)
		stagingAdmin.POST("/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateOne)
		stagingAdmin.POST("/bulk", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateBulk)
	}
	return handler
}

func (StagingItemHttp *StagingItemHttp) GetAll(context *gin.Context) {
	search := context.Query("search")
	pkCode := context.Query("purchasing_group_code")
	locationCode := context.Query("location_code")
	stagingItems, err := StagingItemHttp.stagingItemUsecase.GetAll(search, pkCode, locationCode)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusOK, stagingItems)
}

func (StagingItemHttp *StagingItemHttp) GetById(context *gin.Context) {
	id := context.Param("id")
	stagingItem, err := StagingItemHttp.stagingItemUsecase.GetById(id)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusOK, stagingItem)
}

func (StagingItemHttp *StagingItemHttp) CreateOne(context *gin.Context) {
	var stagingItem *StagingItem.StagingItem
	err := context.ShouldBindJSON(&stagingItem)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	resData, err := StagingItemHttp.stagingItemUsecase.CreateOne(stagingItem)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusCreated, resData)
}

func (StagingItemHttp *StagingItemHttp) CreateBulk(context *gin.Context) {
	var stagingItems []StagingItem.StagingItem
	err := context.ShouldBindJSON(&stagingItems)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	resData, err := StagingItemHttp.stagingItemUsecase.CreateBulk(stagingItems)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusCreated, resData)
}
