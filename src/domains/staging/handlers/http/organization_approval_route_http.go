package http

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	FlowStep "BE-Epson/domains/master/entities"
	"BE-Epson/domains/part/presistence"
	OrganizationApprovalRoute "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
	"BE-Epson/domains/staging/interfaces/impl/repository"
	"BE-Epson/domains/staging/interfaces/impl/usecase"
	"BE-Epson/middlewares"
	SessionUser "BE-Epson/shared/entities"
	"BE-Epson/shared/models/requests/Part"
	Part2 "BE-Epson/shared/models/responses/Part"
	"net/http"

	"github.com/gin-gonic/gin"
)

type OrganizationApprovalRouteHttp struct {
	orgApprovalRouteUsecase interfaces.OrganizationApprovalRouteUsecase
}

func NewOrganizationApprovalRouteHttp(router *gin.Engine, stagingUserRepository interfaces.StagingUserRepository) *OrganizationApprovalRouteHttp {
	handler := &OrganizationApprovalRouteHttp{
		usecase.NewOrganizationApprovalRouteUsecase(repository.NewOrganizationApprovalRouteRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}
	orgApprovalRoute := router.Group("/salad/api/organization-approval-route")
	{
		orgApprovalRoute.GET("/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAll)
		orgApprovalRoute.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetById)
		orgApprovalRoute.POST("/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateOne)
		orgApprovalRoute.POST("/bulk", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateBulk)
	}
	return handler
}

func (OrganizationApprovalRouteHttp *OrganizationApprovalRouteHttp) CheckIsApprover(c *gin.Context) {
	data := &Part.CheckIsApprover{}
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	flag, _ := OrganizationApprovalRouteHttp.orgApprovalRouteUsecase.CheckDataIsApprover(data.Nik)
	if flag {
		c.JSON(http.StatusOK, gin.H{
			"message": flag,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": flag,
	})
}

func (OrganizationApprovalRouteHttp *OrganizationApprovalRouteHttp) CheckDataIsApprover(Nik string) (bool, error) {
	var (
		flag bool
		err  error
	)
	flag, err = OrganizationApprovalRouteHttp.orgApprovalRouteUsecase.CheckDataIsApprover(Nik)
	if err != nil {
		return false, err
	}
	if flag {
		return true, nil
	}
	return false, err
}

func (OrganizationApprovalRouteHttp *OrganizationApprovalRouteHttp) GenerateApprovalRoute(c *gin.Context) {
	var (
		data             []Part.GetOrganizationApprovalRoute
		organizationData []OrganizationApprovalRoute.OrganizationApprovalRoute
		err              error
		seqLevel         uint
	)
	ResponseData := c.MustGet("ResponseCreateRequest").(*Part2.RequestResponse)
	userData := c.MustGet("UserData").(*SessionUser.SessionUser)
	flowStepData := c.MustGet("FlowStep").([]FlowStep.FlowStep)
	if ResponseData.Type == presistence.Disposal {
		seqLevel = 3
	} else if ResponseData.Type == presistence.Request {
		seqLevel = 2
	} else {
		seqLevel = 2
	}
	for _, value := range flowStepData {
		err = nil
		if value.Type == presistence.Organization {
			organizationData, err = OrganizationApprovalRouteHttp.orgApprovalRouteUsecase.GetAllOrganizationApprovalByNik(userData.Nik, true, &seqLevel)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"message": err.Error(),
				})
				return
			}
			for _, orgData := range organizationData {
				data = append(data, Part.GetOrganizationApprovalRoute{
					OrganizationApprovalData: orgData,
					FlowStep:                 value,
				})
			}
		}
	}
	c.Set("OrganizationApprovalRouteData", data)
}

func (OrganizationApprovalRouteHttp OrganizationApprovalRouteHttp) GenerateApprovalRouteRingiBulk(c *gin.Context) {
	var (
		data     []Part.GetOrganizationApprovalRoute
		seqLevel uint
	)

	userData := c.MustGet("UserNik").(string)
	flowStepData := c.MustGet("ResponseFlowStep").([]FlowStep.FlowStep)

	seqLevel = 2
	for _, value := range flowStepData {
		if value.Type == presistence.Organization {
			organizationData, err := OrganizationApprovalRouteHttp.orgApprovalRouteUsecase.GetAllOrganizationApprovalByNik(userData, true, &seqLevel)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"message": err.Error(),
				})
				return
			}
			for _, orgData := range organizationData {
				data = append(data, Part.GetOrganizationApprovalRoute{
					OrganizationApprovalData: orgData,
					FlowStep:                 value,
				})
			}
		}
	}
	c.Set("OrganizationApprovalRouteDataBulk", data)
}

func (OrganizationApprovalRouteHttp *OrganizationApprovalRouteHttp) GetAll(c *gin.Context) {
	data, err := OrganizationApprovalRouteHttp.orgApprovalRouteUsecase.GetAll()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (OrganizationApprovalRouteHttp *OrganizationApprovalRouteHttp) GetById(c *gin.Context) {
	id := c.Param("id")
	data, err := OrganizationApprovalRouteHttp.orgApprovalRouteUsecase.GetById(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (OrganizationApprovalRouteHttp *OrganizationApprovalRouteHttp) CreateOne(c *gin.Context) {
	var data *OrganizationApprovalRoute.OrganizationApprovalRoute
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	organizationApprovalRoute, err := OrganizationApprovalRouteHttp.orgApprovalRouteUsecase.CreateOne(data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, organizationApprovalRoute)
}

func (OrganizationApprovalRouteHttp *OrganizationApprovalRouteHttp) CreateBulk(c *gin.Context) {
	var data []OrganizationApprovalRoute.OrganizationApprovalRoute
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	organizationApprovalRoutes, err := OrganizationApprovalRouteHttp.orgApprovalRouteUsecase.CreateBulk(data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, organizationApprovalRoutes)
}
