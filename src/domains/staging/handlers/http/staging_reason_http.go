package http

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	StagingReason "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
	"BE-Epson/domains/staging/interfaces/impl/repository"
	"BE-Epson/domains/staging/interfaces/impl/usecase"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"github.com/gin-gonic/gin"
	"net/http"
)

type StagingReasonHttp struct {
	stagingReasonUsecase interfaces.StagingReasonUsecase
}

func NewStagingReasonHttp(router *gin.Engine, stagingUserRepository interfaces.StagingUserRepository) *StagingReasonHttp {
	handler := &StagingReasonHttp{
		stagingReasonUsecase: usecase.NewStagingReasonUsecase(repository.NewStagingReasonRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}
	stagingAdmin := router.Group("/salad/api/staging")
	{
		stagingAdmin.GET("/reasons", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAll)
		stagingAdmin.GET("/reasons/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetById)
		stagingAdmin.POST("/reasons", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateOne)
		stagingAdmin.POST("/reasons/bulk", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateBulk)
	}
	return handler
}

func (StagingReasonHttp *StagingReasonHttp) GetAll(context *gin.Context) {
	types := helpers.ParseToString(context.Query("type"))
	stagingReasons, err := StagingReasonHttp.stagingReasonUsecase.GetAll(types)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusOK, stagingReasons)
}

func (StagingReasonHttp *StagingReasonHttp) GetById(context *gin.Context) {
	id := helpers.ParseUint(context.Param("id"))
	stagingReason, err := StagingReasonHttp.stagingReasonUsecase.GetById(id)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusOK, stagingReason)
}

func (StagingReasonHttp *StagingReasonHttp) CreateOne(context *gin.Context) {
	var stagingReason *StagingReason.StagingReason
	err := context.ShouldBindJSON(&stagingReason)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	resData, err := StagingReasonHttp.stagingReasonUsecase.CreateOne(stagingReason)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusCreated, resData)
}

func (StagingReasonHttp *StagingReasonHttp) CreateBulk(context *gin.Context) {
	var stagingReasons []StagingReason.StagingReason
	err := context.ShouldBindJSON(&stagingReasons)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	resData, err := StagingReasonHttp.stagingReasonUsecase.CreateBulk(stagingReasons)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusCreated, resData)
}
