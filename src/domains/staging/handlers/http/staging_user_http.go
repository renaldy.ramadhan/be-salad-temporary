package http

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	StagingUser "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
	"BE-Epson/domains/staging/interfaces/impl/repository"
	"BE-Epson/domains/staging/interfaces/impl/usecase"
	"BE-Epson/middlewares"
	SessionUser "BE-Epson/shared/entities"
	"github.com/gin-gonic/gin"
	"net/http"
)

type StagingUserHttp struct {
	stagingUserUsecase interfaces.StagingUserUsecase
}

func NewStagingUserHttp(router *gin.Engine, stagingUserRepository interfaces.StagingUserRepository) *StagingUserHttp {
	handler := &StagingUserHttp{
		stagingUserUsecase: usecase.NewStagingUserUsecase(repository.NewStagingUserRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}
	stagingUser := router.Group("/salad/api/staging-user")
	{
		stagingUser.GET("/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAll)
		stagingUser.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetById)
		stagingUser.POST("/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateOne)
		stagingUser.POST("/bulk", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateBulk)
		stagingUser.GET("/get-approvers", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetApprovers)
		stagingUser.GET("/data-login", middlewares.VerifyUserKeycloak(stagingUserRepository), middlewares.GetUserInfo(), handler.GetDataLogin)
	}
	return handler
}

func (StagingUserHttp *StagingUserHttp) GetDataLogin(c *gin.Context) {
	data := c.MustGet("UserData").(*SessionUser.SessionUser)
	if data == nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "User Not Found",
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (StagingUserHttp *StagingUserHttp) GetApprovers(c *gin.Context) {
	data, err := StagingUserHttp.stagingUserUsecase.GetApprovers()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (StagingUserHttp *StagingUserHttp) GetAll(c *gin.Context) {
	query := c.Query("search")
	data, err := StagingUserHttp.stagingUserUsecase.GetAll(query)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (StagingUserHttp *StagingUserHttp) GetById(c *gin.Context) {
	id := c.Param("id")
	data, err := StagingUserHttp.stagingUserUsecase.GetById(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, data)
}

func (StagingUserHttp *StagingUserHttp) CreateOne(c *gin.Context) {
	var data *StagingUser.StagingUser
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := StagingUserHttp.stagingUserUsecase.CreateOne(data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, data)
}

func (StagingUserHttp *StagingUserHttp) CreateBulk(c *gin.Context) {
	var data []StagingUser.StagingUser
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := StagingUserHttp.stagingUserUsecase.CreateBulk(data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, data)
}
