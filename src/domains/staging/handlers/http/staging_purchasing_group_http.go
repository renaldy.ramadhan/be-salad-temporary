package http

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	StagingPurchasingGroup "BE-Epson/domains/staging/entities"
	"BE-Epson/domains/staging/interfaces"
	"BE-Epson/domains/staging/interfaces/impl/repository"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"net/http"

	"BE-Epson/domains/staging/interfaces/impl/usecase"
	"github.com/gin-gonic/gin"
)

type StagingPurchasingGroupHttp struct {
	StagingPurchasingGroupUsecase interfaces.StagingPurchasingGroupUsecase
}

func NewStagingPurchasingGroupHttp(router *gin.Engine, stagingUserRepository interfaces.StagingUserRepository) *StagingPurchasingGroupHttp {
	handler := &StagingPurchasingGroupHttp{
		StagingPurchasingGroupUsecase: usecase.NewStagingPurchasingGroupUsecase(repository.NewStagingPurchasingGroupRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}
	admin := router.Group("/salad/api/staging-purchasing-group")
	{
		admin.GET("/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAll)
		admin.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetById)
		admin.POST("/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateOne)
		admin.POST("/bulk", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateBulk)
		admin.PUT("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.Update)
	}
	return handler
}

func (StagingPurchasingGroupHttp *StagingPurchasingGroupHttp) GetAll(context *gin.Context) {
	data, err := StagingPurchasingGroupHttp.StagingPurchasingGroupUsecase.GetAll()
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusOK, data)
}

func (StagingPurchasingGroupHttp *StagingPurchasingGroupHttp) GetById(context *gin.Context) {
	id := helpers.ParseUint(context.Param("id"))
	data, err := StagingPurchasingGroupHttp.StagingPurchasingGroupUsecase.GetById(id)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusOK, data)
}

func (StagingPurchasingGroupHttp *StagingPurchasingGroupHttp) CreateOne(context *gin.Context) {
	var data *StagingPurchasingGroup.StagingPurchasingGroup
	err := context.ShouldBindJSON(&data)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err = StagingPurchasingGroupHttp.StagingPurchasingGroupUsecase.CreateOne(data)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusCreated, data)
}

func (StagingPurchasingGroupHttp *StagingPurchasingGroupHttp) CreateBulk(context *gin.Context) {
	var data []StagingPurchasingGroup.StagingPurchasingGroup
	err := context.ShouldBindJSON(&data)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err = StagingPurchasingGroupHttp.StagingPurchasingGroupUsecase.CreateBulk(data)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	context.JSON(http.StatusCreated, data)
}

func (StagingPurchasingGroupHttp *StagingPurchasingGroupHttp) Update(ctx *gin.Context) {
	// ambil id dari url
	id := ctx.Param("id")
	// inisialisasi tipe data body
	var data *StagingPurchasingGroup.StagingPurchasingGroup
	// ambil data dari body
	err := ctx.ShouldBindJSON(&data)
	// jika terjadi error payload tidak sesuai
	if err != nil {
		// return error 400 bad request
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	// update data
	data, err = StagingPurchasingGroupHttp.StagingPurchasingGroupUsecase.UpdateById(data, id)
	// jika terjadi error saat update
	if err != nil {
		// return error 500 internal server error
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	// return data yang telah diupdate (success)
	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data":    "data",
	})
}
