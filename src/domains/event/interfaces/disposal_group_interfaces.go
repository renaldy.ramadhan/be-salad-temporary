package interfaces

import (
	DisposalGroup "BE-Epson/domains/event/entities"
	Workflow "BE-Epson/domains/master/entities"
	"BE-Epson/shared/models/requests/Part"
)

type DisposalGroupRepository interface {
	FindAll(params *DisposalGroup.DisposalQueryParams) (disposalGroups []DisposalGroup.DisposalGroup, err error, totalData int64)
	FindOne(id uint) (disposalGroup *DisposalGroup.DisposalGroup, err error)
	StoreOne(payload *DisposalGroup.DisposalGroupRequest) (data *DisposalGroup.DisposalGroup, err error)
	Update(data *DisposalGroup.DisposalGroup, param string) (*DisposalGroup.DisposalGroup, error)
	FindActive() (data []DisposalGroup.DisposalGroup, err error)
	FindWorkflowEvent(eventType *string) (dataWorkflow *Workflow.Workflow)
	FindFlowStepEvent(workflowId uint) (dataFlowstep []Workflow.FlowStep)
	CheckForEligibleEvent(currentDate string) (data []DisposalGroup.DisposalGroup, err error)
	PDFData(id int) (data *Part.DisposalEventPdf, err error)
}

type DisposalGroupUsecase interface {
	GetAll(params *DisposalGroup.DisposalQueryParams) (disposalGroups []DisposalGroup.DisposalGroup, err error, totalData int64)
	GetOneById(id uint) (disposalGroup *DisposalGroup.DisposalGroup, err error)
	GetWorkflowEvent(eventType *string) (dataWorkflow *Workflow.Workflow)
	GetFlowStepEvent(workflowId uint) (dataFlowstep []Workflow.FlowStep)
	CreateDisposalGroup(disposalGroup *DisposalGroup.DisposalGroupRequest) (dataDisposal *DisposalGroup.DisposalGroup, errDisposal error)
	UpdateDisposalGroup(data *DisposalGroup.DisposalGroup, param string) (*DisposalGroup.DisposalGroup, error)
	FindActiveEvent() (data []DisposalGroup.DisposalGroup, err error)
	CheckIsEligible(currentDate string) (data []DisposalGroup.DisposalGroup, err error)
	GeneratePDF(id int) (data *Part.DisposalEventPdf, err error)
}
