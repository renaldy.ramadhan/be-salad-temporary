package repository

import (
	DisposalGroup "BE-Epson/domains/event/entities"
	Workflow "BE-Epson/domains/master/entities"
	ActiveRequestApprovalList "BE-Epson/domains/part/entities"
	"BE-Epson/domains/part/presistence"
	StagingUser "BE-Epson/domains/staging/entities"
	"BE-Epson/shared/helpers"
	"BE-Epson/shared/models/requests/Part"
	"fmt"
	"gorm.io/gorm"
	"strconv"
	"strings"
	"time"
)

type disposalGroupRepository struct {
	DB *gorm.DB
}

func NewDisposalGroupRepository(db *gorm.DB) *disposalGroupRepository {
	return &disposalGroupRepository{
		DB: db,
	}
}

func (repo *disposalGroupRepository) buildQuery(DB *gorm.DB, params *DisposalGroup.DisposalQueryParams) (*gorm.DB, int64) {
	var totalData int64
	querycount := DB.Model(&DisposalGroup.DisposalGroup{})
	if params.Type != "" {
		DB = DB.Where("type = ?", params.Type)
		querycount = querycount.Where("type = ?", params.Type)
	}

	if params.Page != 0 && params.PageSize != 0 {
		DB = DB.Offset((params.Page - 1) * params.PageSize).Limit(params.PageSize)
	}

	if params.Status != "" {
		DB = DB.Where("status = ?", params.Status)
		querycount = querycount.Where("status = ?", params.Status)
	}
	querycount.Count(&totalData)

	return DB, totalData
}

func (repository *disposalGroupRepository) FindAll(params *DisposalGroup.DisposalQueryParams) (disposalGroups []DisposalGroup.DisposalGroup, err error, totalData int64) {
	query, total := repository.buildQuery(repository.DB, params)

	err = query.Find(&disposalGroups).Error
	totalData = total

	if err != nil {
		return nil, err, 0
	}
	return disposalGroups, nil, totalData
}

func (repository *disposalGroupRepository) FindOne(id uint) (disposalGroup *DisposalGroup.DisposalGroup, err error) {
	err = repository.DB.Where("id = ?", id).Find(&disposalGroup).Error
	if err != nil {
		return nil, err
	}
	return disposalGroup, nil
}

func (repository *disposalGroupRepository) StoreOne(payload *DisposalGroup.DisposalGroupRequest) (data *DisposalGroup.DisposalGroup, err error) {
	loc, _ := time.LoadLocation("Asia/Jakarta")
	currentDate := time.Now().In(loc)
	format := currentDate.Format(time.RFC3339)

	var activeRequestApprovalList ActiveRequestApprovalList.ActiveRequestApprovalList
	disposalGroup := &DisposalGroup.DisposalGroup{
		Name:          payload.Name,
		DisposalCode:  payload.DisposalCode,
		LocationName:  payload.LocationName,
		Type:          payload.Type,
		CreatedBy:     payload.CreatedBy,
		ExecutionDate: payload.ExecutionDate,
		StartDate:     payload.StartDate,
		EndDate:       payload.EndDate,
	}

	if format >= payload.StartDate.Format(time.RFC3339) && format <= payload.EndDate.Format(time.RFC3339) {
		activeStatus := "Active"
		disposalGroup.Status = &activeStatus
	}

	if err := repository.DB.Create(&disposalGroup).Error; err != nil {
		return nil, err
	}

	groupId := disposalGroup.ID
	for _, value := range payload.ApproverSetting {
		err := repository.DB.Find(&activeRequestApprovalList).Create(&ActiveRequestApprovalList.ActiveRequestApprovalList{
			IdDepartment:     value.IdDepartment,
			IdEvent:          &groupId,
			IdFlowStep:       value.IdFlowStep,
			DueDateApproval:  value.DueDateApproval,
			ApprovalSequence: value.ApprovalSequence,
			ApproverNik:      value.ApproverNik,
			Status:           presistence.Queued,
		}).Error

		if err != nil {
			return nil, err
		}
	}

	return disposalGroup, nil
}

func (repository *disposalGroupRepository) FindWorkflowEvent(eventType *string) (dataWorkflow *Workflow.Workflow) {
	errWorkflow := repository.DB.Where("type = ?", eventType).Find(&dataWorkflow).Error
	if errWorkflow != nil {
		return nil
	}

	return dataWorkflow
}

func (repository *disposalGroupRepository) FindFlowStepEvent(workflowId uint) (dataFlowstep []Workflow.FlowStep) {
	errFlowStep := repository.DB.Where("id_workflow = ? AND type != ?", workflowId, 1).Find(&dataFlowstep).Error
	if errFlowStep != nil {
		return nil
	}

	return dataFlowstep
}

func (repository *disposalGroupRepository) Update(data *DisposalGroup.DisposalGroup, param string) (*DisposalGroup.DisposalGroup, error) {
	err := repository.DB.Where("id = ?", param).Updates(&data).Error
	if err != nil {
		return nil, err
	}

	return data, err
}

func (repository *disposalGroupRepository) FindActive() (data []DisposalGroup.DisposalGroup, err error) {
	err = repository.DB.Where("status = ?", "Active").Find(&data).Error
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (repository *disposalGroupRepository) CheckForEligibleEvent(currentDate string) (data []DisposalGroup.DisposalGroup, err error) {
	err = repository.DB.Where("end_date >= ? AND status = 'Active'", currentDate, currentDate).Find(&data).Error
	if err != nil {
		return nil, err
	}

	return data, nil

}

func (repo *disposalGroupRepository) PDFData(id int) (data *Part.DisposalEventPdf, err error) {
	var (
		requests          []ActiveRequestApprovalList.Request
		disposalGroup     *DisposalGroup.DisposalGroup
		approvalList      []ActiveRequestApprovalList.ActiveRequestApprovalList
		modelItemPivot    *ActiveRequestApprovalList.ItemPivot
		listItemSystem    []ActiveRequestApprovalList.ItemStagingPivot
		listItemNonSystem []ActiveRequestApprovalList.ItemStagingPivot
		systemQty         int64
		nonSystemQty      int64
		nonSystemAmount   float64
		systemAmount      float64
		nonSystemWeight   float64
		systemWeight      float64
	)
	data = &Part.DisposalEventPdf{}
	eventApprovalLists := make([]Part.EventApprovalList, 0)
	stagingUser := &StagingUser.StagingUser{}
	itemNonSystem := make([]ActiveRequestApprovalList.ItemStagingPivot, 0)
	itemSystem := make([]ActiveRequestApprovalList.ItemStagingPivot, 0)

	errDisposal := repo.DB.Where("id = ?", id).Find(&disposalGroup).Error
	if errDisposal != nil {
		return nil, errDisposal
	}

	errApprovalList := repo.DB.Where("id_event = ?", id).Preload("Department").Find(&approvalList).Error
	if errApprovalList != nil {
		return nil, errApprovalList
	}

	err = repo.DB.Where("id_disposal_group_event_fk = ?", id).Preload("ItemPivot").Find(&requests).Error
	if err != nil {
		return nil, err

	}

	for _, request := range requests {
		if request.DisposalType == "System" {
			err = repo.DB.Model(&modelItemPivot).Where("id_request_fk = ?", request.ID).Joins("JOIN staging_items ON staging_items.item_code = item_pivots.item_code_fk").Scan(&listItemSystem).Error
			if err != nil {
				return nil, err
			}
			itemSystem = append(itemSystem, listItemSystem...)
		} else {
			err = repo.DB.Model(&modelItemPivot).Where("id_request_fk = ?", request.ID).Joins("JOIN staging_items ON staging_items.item_code = item_pivots.item_code_fk").Scan(&listItemNonSystem).Error
			if err != nil {
				return nil, err
			}
			itemNonSystem = append(itemNonSystem, listItemNonSystem...)
		}
	}

	for _, itemNon := range itemNonSystem {
		nonSystemQty += helpers.ParseIntToInt64(itemNon.ApprovedQuantity)
		nonSystemAmount += itemNon.Amount

		if strings.ContainsRune(itemNon.NetWeight, ',') {
			itemNon.NetWeight = strings.Replace(itemNon.NetWeight, ",", ".", -1)
		}

		result, _ := strconv.ParseFloat(itemNon.NetWeight, 64)
		nonSystemWeight += result
	}

	for _, itemSys := range itemSystem {
		systemQty += helpers.ParseIntToInt64(itemSys.ApprovedQuantity)
		systemAmount += itemSys.Amount

		if strings.ContainsRune(itemSys.NetWeight, ',') {
			itemSys.NetWeight = strings.Replace(itemSys.NetWeight, ",", ".", -1)
		}

		result, _ := strconv.ParseFloat(itemSys.NetWeight, 64)
		systemWeight += result
	}

	for _, approval := range approvalList {
		stagingUser.Nik = approval.ApproverNik
		errStaging := repo.DB.Find(stagingUser).Error
		if errStaging != nil {
			return nil, errStaging
		}
		fmt.Println(stagingUser.Name)
		eventApprovalLists = append(eventApprovalLists, Part.EventApprovalList{
			DateApproval:     approval.DateApproval,
			ApprovalSequence: approval.ApprovalSequence,
			Notes:            approval.Notes,
			ApproverNik:      approval.ApproverNik,
			Status:           approval.Status,
			ApproverName:     stagingUser.Name,
			DepartmentName:   approval.Department.DepartmentName,
		})
	}

	data.ReferenceCode = disposalGroup.DisposalCode
	data.ExecutionDate = disposalGroup.ExecutionDate
	data.StartDate = disposalGroup.StartDate
	data.EndDate = disposalGroup.EndDate
	data.TotalSystemQty = systemQty
	data.TotalNonSystemQty = nonSystemQty
	data.TotalAmountSystem = systemAmount
	data.TotalAmountNonSystem = nonSystemAmount
	data.TotalWeightSystem = systemWeight
	data.TotalWeightNonSystem = nonSystemWeight
	data.ApprovalList = eventApprovalLists

	return data, err

}

func (repository *disposalGroupRepository) DoUpdateStatus() (err error) {
	model := &[]DisposalGroup.DisposalGroup{}
	result := repository.DB.Find(&model)

	loc, _ := time.LoadLocation("Asia/Jakarta")
	now := time.Now().In(loc)
	format := now.Format(time.RFC3339)
	if result.RowsAffected > 0 {
		for _, value := range *model {
			formatedStartDate := value.StartDate.Format(time.RFC3339)
			formatedEndDate := value.EndDate.Format(time.RFC3339)
			if format >= formatedStartDate && format <= formatedEndDate {
				repository.DB.Model(&model).Where("id = ?", value.ID).Update("status", "Active")
			} else {
				repository.DB.Model(&model).Where("id = ?", value.ID).Update("status", "In Active")
			}
		}
	} else {
		return nil
	}
	return nil
}
