package repository

import (
	"BE-Epson/domains/event/entities"
	"gorm.io/gorm"
)

type activeEventApprovalListRepo struct {
	DB *gorm.DB
}

func NewActiveEventApprovalListRepo(db *gorm.DB) *activeEventApprovalListRepo {
	return &activeEventApprovalListRepo{DB: db}
}

func (activeEventApprovalListRepo activeEventApprovalListRepo) FindAll() (data []entities.DisposalApproverOption, err error) {
	err = activeEventApprovalListRepo.DB.Find(&data).Error
	if err != nil {
		return nil, err
	}

	return data, err
}

func (activeEventApprovalListRepo activeEventApprovalListRepo) StoreOne(data *entities.DisposalApproverOption) (*entities.DisposalApproverOption, error) {
	err := activeEventApprovalListRepo.DB.Create(data).Error
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (activeEventApprovalListRepo activeEventApprovalListRepo) FindApproverSettingByEvent(eventId uint, departmentId uint) (data *entities.DisposalApproverOption, err error) {
	err = activeEventApprovalListRepo.DB.Where("id_event = ? AND id_department = ?", eventId, departmentId).Find(&data).Error
	if err != nil {
		return nil, err
	}

	return data, err

}
