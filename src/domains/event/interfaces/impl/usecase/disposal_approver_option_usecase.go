package usecase

import (
	"BE-Epson/domains/event/entities"
	"BE-Epson/domains/event/interfaces"
)

type activeEventApprovalListUseCase struct {
	activeEventApprovalListRepo interfaces.ActiveEventApprovalListRepository
}

func NewActiveEventApprovalListUseCase(repository interfaces.ActiveEventApprovalListRepository) *activeEventApprovalListUseCase {
	return &activeEventApprovalListUseCase{activeEventApprovalListRepo: repository}
}

func (activeEventApprovalListUseCase activeEventApprovalListUseCase) FindListApproval() (data []entities.DisposalApproverOption, err error) {
	data, err = activeEventApprovalListUseCase.activeEventApprovalListRepo.FindAll()
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (activeEventApprovalListUseCase activeEventApprovalListUseCase) CreateApproverList(data *entities.DisposalApproverOption) (*entities.DisposalApproverOption, error) {
	data, err := activeEventApprovalListUseCase.activeEventApprovalListRepo.StoreOne(data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (activeEventApprovalListUseCase activeEventApprovalListUseCase) FindApproverSettingByEvent(eventId uint, departmentId uint) (data *entities.DisposalApproverOption, err error) {
	data, err = activeEventApprovalListUseCase.activeEventApprovalListRepo.FindApproverSettingByEvent(eventId, departmentId)
	if err != nil {
		return nil, err
	}

	return data, nil
}
