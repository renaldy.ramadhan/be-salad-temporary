package usecase

import (
	DisposalGroup "BE-Epson/domains/event/entities"
	"BE-Epson/domains/event/interfaces"
	Workflow "BE-Epson/domains/master/entities"
	Const "BE-Epson/shared/errors"
	"BE-Epson/shared/models/requests/Part"
	"errors"
)

type disposalGroupUsecase struct {
	disposalGroupRepository interfaces.DisposalGroupRepository
}

func NewDisposalGroupUsecase(disposalGroupRepository interfaces.DisposalGroupRepository) *disposalGroupUsecase {
	return &disposalGroupUsecase{
		disposalGroupRepository: disposalGroupRepository,
	}
}

func (usecase *disposalGroupUsecase) GetAll(params *DisposalGroup.DisposalQueryParams) (disposalGroups []DisposalGroup.DisposalGroup, err error, totalData int64) {
	disposalGroups, err, totalData = usecase.disposalGroupRepository.FindAll(params)
	if err != nil {
		return nil, err, totalData
	}
	return disposalGroups, nil, totalData
}

func (usecase *disposalGroupUsecase) GetOneById(id uint) (disposalGroup *DisposalGroup.DisposalGroup, err error) {
	if id < 1 {
		return nil, errors.New(Const.ID_LESS_THAN_ONE)
	}
	disposalGroup, err = usecase.disposalGroupRepository.FindOne(id)
	if err != nil {
		return nil, err
	}
	return disposalGroup, nil
}

func (usecase *disposalGroupUsecase) CreateDisposalGroup(disposalGroup *DisposalGroup.DisposalGroupRequest) (dataDisposal *DisposalGroup.DisposalGroup, errDisposal error) {
	dataDisposal, errDisposal = usecase.disposalGroupRepository.StoreOne(disposalGroup)
	if errDisposal != nil {
		return nil, errDisposal
	}
	return dataDisposal, nil
}

func (useCase *disposalGroupUsecase) GetWorkflowEvent(eventType *string) (dataWorkflow *Workflow.Workflow) {
	dataWorkflow = useCase.disposalGroupRepository.FindWorkflowEvent(eventType)
	return dataWorkflow
}

func (useCase *disposalGroupUsecase) GetFlowStepEvent(workflowId uint) (dataFlowstep []Workflow.FlowStep) {
	dataFlowstep = useCase.disposalGroupRepository.FindFlowStepEvent(workflowId)
	return dataFlowstep
}

func (useCase *disposalGroupUsecase) UpdateDisposalGroup(data *DisposalGroup.DisposalGroup, param string) (*DisposalGroup.DisposalGroup, error) {
	data, err := useCase.disposalGroupRepository.Update(data, param)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (useCase *disposalGroupUsecase) FindActiveEvent() (data []DisposalGroup.DisposalGroup, err error) {
	data, err = useCase.disposalGroupRepository.FindActive()
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (usecase *disposalGroupUsecase) CheckIsEligible(currentDate string) (data []DisposalGroup.DisposalGroup, err error) {
	data, err = usecase.disposalGroupRepository.CheckForEligibleEvent(currentDate)
	if err != nil {
		return nil, err
	}

	return data, nil

}

func (usecase *disposalGroupUsecase) GeneratePDF(id int) (data *Part.DisposalEventPdf, err error) {
	data, err = usecase.disposalGroupRepository.PDFData(id)
	if err != nil {
		return nil, err
	}

	return data, nil

}
