package interfaces

import "BE-Epson/domains/event/entities"

type ActiveEventApprovalListRepository interface {
	FindAll() (data []entities.DisposalApproverOption, err error)
	StoreOne(data *entities.DisposalApproverOption) (*entities.DisposalApproverOption, error)
	FindApproverSettingByEvent(eventId uint, departmentId uint) (data *entities.DisposalApproverOption, err error)
}

type ActiveEventApprovalListUseCase interface {
	FindListApproval() (data []entities.DisposalApproverOption, err error)
	CreateApproverList(data *entities.DisposalApproverOption) (*entities.DisposalApproverOption, error)
	FindApproverSettingByEvent(eventId uint, departmentId uint) (data *entities.DisposalApproverOption, err error)
}
