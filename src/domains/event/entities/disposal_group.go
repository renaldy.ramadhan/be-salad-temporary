package entities

import (
	"gorm.io/gorm"
	"time"
)

type DisposalGroup struct {
	gorm.Model
	Name          *string    `json:"Name" binding:"required"`
	DisposalCode  *string    `json:"DisposalCode" binding:"required"`
	LocationName  string     `gorm:"not null" json:"LocationName" binding:"required"`
	Type          *string    `json:"Type" binding:"required"`
	CreatedBy     *string    `json:"CreatedBy" binding:"required"`
	ExecutionDate *time.Time `json:"ExecutionDate"`
	StartDate     *time.Time `gorm:"not null" json:"StartDate"`
	EndDate       *time.Time `gorm:"not null" json:"EndDate"`
	Status        *string    `gorm:"default:'In Active'" json:"Status"`
}

type DisposalGroupRequest struct {
	Name            *string                  `json:"Name" binding:"required"`
	DisposalCode    *string                  `json:"DisposalCode" binding:"required"`
	LocationName    string                   `gorm:"not null" json:"LocationName" binding:"required"`
	Type            *string                  `json:"Type" binding:"required"`
	CreatedBy       *string                  `json:"CreatedBy" binding:"required"`
	ExecutionDate   *time.Time               `json:"ExecutionDate"`
	StartDate       *time.Time               `gorm:"not null" json:"StartDate"`
	EndDate         *time.Time               `gorm:"not null" json:"EndDate"`
	ApproverSetting []DisposalApproverOption `json:"ApproverSetting"`
}

type DisposalQueryParams struct {
	Type     string `json:"Type"`
	Page     int    `json:"Page"`
	PageSize int    `json:"PageSize"`
	Status   string `json:"Status"`
}
