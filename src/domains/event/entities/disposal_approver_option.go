package entities

import (
	"BE-Epson/domains/master/entities"
	"gorm.io/gorm"
	"time"
)

type DisposalApproverOption struct {
	gorm.Model
	IdFlowStep       uint                `gorm:"not null" json:"IdFlowStep"`
	IdDepartment     uint                `gorm:"not null" json:"IdDepartment"`
	IdEvent          uint                `json:"IdEvent"`
	DateApproval     *time.Time          `json:"DateApproval"`
	DueDateApproval  *time.Time          `json:"DueDateApproval"`
	ApprovalSequence uint                `gorm:"not null" json:"ApprovalSequence"`
	ApproverNik      string              `gorm:"not null" json:"ApproverNik"`
	ApproverName     *string             `gorm:"not null" json:"ApproverName"`
	Notes            *string             `json:"Notes"`
	FlowStep         entities.FlowStep   `gorm:"foreignKey:IdFlowStep;" json:"-"`
	Department       entities.Department `gorm:"foreignKey:IdDepartment;" json:"Department"`
	Event            DisposalGroup       `gorm:"foreignKey:IdEvent;" json:"Event"`
}

type DisposalApproverOptionFilter struct {
	WorkflowType string `json:"WorkflowType"`
}

type DisposalApproverOptionResponse struct {
	IdFlowStep       uint    `json:"IdFlowStep"`
	IdDepartment     uint    `json:"IdDepartment"`
	DepartmentName   string  `json:"DepartmentName"`
	ApprovalSequence uint    `json:"ApprovalSequence"`
	ApproverNik      string  `json:"ApproverNik"`
	ApproverName     *string `json:"ApproverName"`
}
