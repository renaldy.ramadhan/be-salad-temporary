package handlers

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	ActiveApprovalEvent "BE-Epson/domains/event/entities"
	"BE-Epson/domains/event/interfaces"
	"BE-Epson/domains/event/interfaces/impl/repository"
	"BE-Epson/domains/event/interfaces/impl/usecase"
	approver "BE-Epson/domains/master/interfaces"
	repository2 "BE-Epson/domains/master/interfaces/impl/repository"
	usecase2 "BE-Epson/domains/master/interfaces/impl/usecase"
	StagingInterface "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"github.com/gin-gonic/gin"
)

type ActiveEventApproverListHttp struct {
	activeEventApproverUseCase interfaces.ActiveEventApprovalListUseCase
	approverGroupUseCase       approver.ApproverGroupUseCase
	approverUseCase            approver.ApproverUseCase
	disposalGroupUseCase       interfaces.DisposalGroupUsecase
	departmentUseCase          approver.DepartmentUseCase
}

func NewActiveEventApproverListHttp(router *gin.Engine, stagingUserRepository StagingInterface.StagingUserRepository) *ActiveEventApproverListHttp {
	handler := &ActiveEventApproverListHttp{
		activeEventApproverUseCase: usecase.NewActiveEventApprovalListUseCase(repository.NewActiveEventApprovalListRepo(database.ConnectDatabase(Const.DB_SALAD))),
		approverGroupUseCase:       usecase2.NewApproverGroupUseCase(repository2.NewApproverGroupRepo(database.ConnectDatabase(Const.DB_SALAD))),
		approverUseCase:            usecase2.NewApproverUseCase(repository2.NewApproverRepo(database.ConnectDatabase(Const.DB_SALAD))),
		disposalGroupUseCase:       usecase.NewDisposalGroupUsecase(repository.NewDisposalGroupRepository(database.ConnectDatabase(Const.DB_SALAD))),
		departmentUseCase:          usecase2.NewDepartmentUsecase(repository2.NewDepartmentRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}

	activeEventApproveList := router.Group("/salad/api/approval-setting")
	{
		activeEventApproveList.GET("/", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.FindListApproval)
		activeEventApproveList.POST("/generate", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CreateApproverList)
	}
	return handler
}

func (ActiveEventApproverListHttp ActiveEventApproverListHttp) FindListApproval(c *gin.Context) {
	data, err := ActiveEventApproverListHttp.activeEventApproverUseCase.FindListApproval()
	if err != nil {
		c.JSON(500, gin.H{
			"message": "Internal Server Error!",
		})
		return
	}

	c.JSON(200, data)
}

func (ActiveEventApproverListHttp *ActiveEventApproverListHttp) CreateApproverList(c *gin.Context) {
	var respoonse []ActiveApprovalEvent.DisposalApproverOptionResponse
	payload := &ActiveApprovalEvent.DisposalApproverOptionFilter{}
	if err := c.ShouldBindJSON(payload); err != nil {
		c.JSON(400, gin.H{
			"message": "Bad Request!",
		})
		return

	}
	workflow := ActiveEventApproverListHttp.disposalGroupUseCase.GetWorkflowEvent(&payload.WorkflowType)
	flowsteps := ActiveEventApproverListHttp.disposalGroupUseCase.GetFlowStepEvent(workflow.ID)

	listApproverGroup, err := ActiveEventApproverListHttp.approverGroupUseCase.GetListApproverGroupByType(workflow.Type)
	if err != nil {
		c.JSON(500, gin.H{
			"message": "Internal Server Error!",
		})
	}

	listFlowstepData := []map[string]interface{}{}
	listApproverGroupData := []map[string]interface{}{}
	listApproversData := []map[string]interface{}{}

	for _, flowstep := range flowsteps {
		listFlowstepData = append(listFlowstepData, map[string]interface{}{
			"flowSequence": flowstep.FlowSequence,
			"idflowstep":   flowstep.ID,
		})
	}

	for _, group := range listApproverGroup {
		department, _ := ActiveEventApproverListHttp.departmentUseCase.GetOneDepartment(group.IdDepartment)
		listApproverGroupData = append(listApproverGroupData, map[string]interface{}{
			"idDepartment": group.IdDepartment,
			"department":   department.DepartmentName,
			"idGroup":      group.ID,
		})
	}

	for _, approverGroup := range listApproverGroupData {
		approverData, _ := ActiveEventApproverListHttp.approverUseCase.GetApproverByGroupId(approverGroup["idGroup"].(uint))
		for _, approver := range approverData {
			listApproversData = append(listApproversData, map[string]interface{}{
				"Nik":  approver.Nik,
				"Name": approver.EmployeeName,
			})
		}
	}

	for i, listApprover := range listApproversData {

		approvalEvent := &ActiveApprovalEvent.DisposalApproverOptionResponse{
			ApproverNik:      listApprover["Nik"].(string),
			ApproverName:     helpers.StringPointer(listApprover["Name"].(string)),
			IdFlowStep:       flowsteps[i].ID,
			IdDepartment:     listApproverGroupData[i]["idDepartment"].(uint),
			DepartmentName:   listApproverGroupData[i]["department"].(string),
			ApprovalSequence: flowsteps[i].FlowSequence,
		}
		respoonse = append(respoonse, *approvalEvent)
	}

	c.JSON(200, gin.H{
		"message": "success",
		"result":  respoonse,
	})
}
