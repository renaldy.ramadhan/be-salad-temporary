package handlers

import (
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/database"
	DisposalGroup "BE-Epson/domains/event/entities"
	"BE-Epson/domains/event/interfaces"
	"BE-Epson/domains/event/interfaces/impl/repository"
	"BE-Epson/domains/event/interfaces/impl/usecase"
	StagingInterface "BE-Epson/domains/staging/interfaces"
	"BE-Epson/middlewares"
	"BE-Epson/shared/helpers"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

type DisposalGroupHttp struct {
	disposalGroupUsecase interfaces.DisposalGroupUsecase
}

func NewDisposalGroupHttp(router *gin.Engine, stagingUserRepository StagingInterface.StagingUserRepository) *DisposalGroupHttp {
	handler := &DisposalGroupHttp{
		disposalGroupUsecase: usecase.NewDisposalGroupUsecase(repository.NewDisposalGroupRepository(database.ConnectDatabase(Const.DB_SALAD))),
	}
	disposalGroup := router.Group("/salad/api/disposal-group")
	{
		disposalGroup.GET("", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetAll)
		disposalGroup.GET("/active", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.FindAllActive)
		disposalGroup.GET("/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GetById)
		disposalGroup.POST("/add", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.Create)
		disposalGroup.PUT("update/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.Update)
		disposalGroup.GET("/is-eligible", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.CheckEligible)
		disposalGroup.GET("generate/pdf/:id", middlewares.VerifyUserKeycloak(stagingUserRepository), handler.GeneratePdf)
	}
	return handler
}

func (DisposalGroupHttp *DisposalGroupHttp) GetAll(c *gin.Context) {
	params := &DisposalGroup.DisposalQueryParams{
		Page:     helpers.ParseInt(c.Query("page")),
		PageSize: helpers.ParseInt(c.Query("page_size")),
		Type:     helpers.ParseToString(c.Query("type")),
		Status:   helpers.ParseToString(c.Query("status")),
	}
	fmt.Println(params)
	result, err, totalData := DisposalGroupHttp.disposalGroupUsecase.GetAll(params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data":  result,
		"total": totalData,
	})
}

func (DisposalGroupHttp *DisposalGroupHttp) GetById(c *gin.Context) {
	id := helpers.ParseUint(c.Param("id"))
	result, err := DisposalGroupHttp.disposalGroupUsecase.GetOneById(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, result)
}

func (DisposalGroupHttp *DisposalGroupHttp) Create(c *gin.Context) {
	data := &DisposalGroup.DisposalGroupRequest{}
	if err := c.ShouldBindJSON(data); err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	resultDisposalEvent, err := DisposalGroupHttp.disposalGroupUsecase.CreateDisposalGroup(data)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resultDisposalEvent)
}

func (DisposalGroupHttp *DisposalGroupHttp) Update(c *gin.Context) {
	id := c.Param("id")
	data := &DisposalGroup.DisposalGroup{}
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Bad Request",
			"error":   err.Error(),
		})
		return
	}

	result, err := DisposalGroupHttp.disposalGroupUsecase.UpdateDisposalGroup(data, id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internal Server Error",
			"error":   err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, result)
}

func (DisposalGroupHttp *DisposalGroupHttp) FindAllActive(c *gin.Context) {
	result, err := DisposalGroupHttp.disposalGroupUsecase.FindActiveEvent()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internal Server Error",
		})
		return
	}

	c.JSON(http.StatusOK, result)
}

func (DisposalGroupHttp *DisposalGroupHttp) CheckEligible(c *gin.Context) {
	var status bool
	loc, _ := time.LoadLocation("Asia/Jakarta")
	now := time.Now().In(loc)
	format := now.Format(time.RFC3339)

	result, err := DisposalGroupHttp.disposalGroupUsecase.CheckIsEligible(format)
	if len(result) > 0 {
		status = true
	} else {
		status = false
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internal Server Error",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status": status,
	})
}

func (DisposalGroupHttp *DisposalGroupHttp) GeneratePdf(c *gin.Context) {
	id := helpers.ParseInt(c.Param("id"))
	data, err := DisposalGroupHttp.disposalGroupUsecase.GeneratePDF(id)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internal Server Error",
		})
		return

	}
	c.JSON(http.StatusOK, data)
}
