package database

import (
	"BE-Epson/domains/event/entities"
	master "BE-Epson/domains/master/entities"
	Part "BE-Epson/domains/part/entities"
	StagingReason "BE-Epson/domains/staging/entities"
	UserManagement "BE-Epson/domains/user/entities"
	"gorm.io/gorm"
	"gorm.io/plugin/dbresolver"
)

func Migrate(db *gorm.DB) error {
	return db.Clauses(dbresolver.Use("DB_SALAD")).Debug().AutoMigrate(
		//&StagingReason.StagingUser{},
		&StagingReason.OrganizationApprovalRoute{},
		&Part.Sequence{},
		&StagingReason.StagingPlant{},
		&master.Department{},
		&StagingReason.StagingReason{},
		&StagingReason.StagingPurchasingGroup{},
		&entities.DisposalGroup{},
		&Part.Request{},
		&StagingReason.StagingItem{},
		&master.Workflow{},
		&master.FlowStep{},
		&Part.ItemPivot{},
		&Part.DocumentDetail{},
		&master.ApproverGroup{},
		&master.Approver{},
		&master.ApproverPivot{},
		&Part.ActiveRequestApprovalList{},
		&Part.RequestApprovalHistory{},
		&entities.DisposalApproverOption{},
		&StagingReason.StagingEmployeeCompany{},
		&UserManagement.User{},
	)
}
