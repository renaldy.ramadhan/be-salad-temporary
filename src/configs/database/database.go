package database

import (
	"BE-Epson/configs/app"
	Const "BE-Epson/configs/const"
	"BE-Epson/configs/seeder"
	"fmt"
	"gorm.io/gorm/logger"
	"io"
	"log"
	"net/url"
	"os"
	"time"

	logs "BE-Epson/shared/logger"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"gorm.io/plugin/dbresolver"
)

var (
	gormConfig = &gorm.Config{
		Logger:               logger.Default.LogMode(logger.Info),
		FullSaveAssociations: true,
		//NamingStrategy: schema.NamingStrategy{
		//	TablePrefix: conf.Schema, // Set the schema name here
		//},
	}
	db  *gorm.DB
	db1 *gorm.DB
	db2 *gorm.DB
)

//func ConnectDatabasePostgresSQL() *gorm.DB {
//	var dns string
//	if db == nil {
//		conf := app.GetConfig().Database
//		dns = fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable", conf.Host, conf.Username, conf.Password, conf.Database, conf.Port)
//		database, err := gorm.Open(postgres.New(
//			postgres.Config{
//				DSN: dns,
//			},
//		), &gorm.Config{
//			Logger:               logger.Default.LogMode(logger.Info),
//			FullSaveAssociations: true,
//		})
//		if err != nil {
//			panic("Error connecting to database : " + err.Error())
//		}
//		db = database
//	}
//	return db
//}

func fileWriter(filename string) io.Writer {
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("Failed to open log file: %v", err)
	}
	return file
}

func RegisterDB(app app.Database, db *gorm.DB) *gorm.DB {
	err := db.Use(dbresolver.Register(dbresolver.Config{
		Sources: []gorm.Dialector{sqlserver.Open(createDNS(app, 1))},
		//Replicas: []gorm.Dialector{sqlserver.Open(createDNS(app, 1))},
		Policy: dbresolver.RandomPolicy{},
		//TraceResolverMode: true,
	}).SetConnMaxLifetime(time.Hour).SetMaxIdleConns(10).SetMaxOpenConns(100))
	if err != nil {
		panic("Error connecting to database 2: " + err.Error())
	}
	return db
}

func ConnectDatabase(data string) *gorm.DB {
	// Open a file for logging
	//logFolderPath := "./logs/"
	//logFileName := fmt.Sprintf("db_%s.log", time.Now().Format("20060102"))
	//err := os.MkdirAll(logFolderPath, 0755)
	//if err != nil {
	//	log.Fatalf("Failed to create log folder: %v", err)
	//}

	configGorm := &gorm.Config{
		Logger: logger.New(log.New(io.MultiWriter(logs.Logs, os.Stdout), "\r\n", log.LstdFlags),
			logger.Config{
				// Set log level to Info to see the SQL queries
				LogLevel: logger.Info,
			}),
	}

	conf := app.GetConfig().Database // Ensure this method correctly fetches your configuration

	if data == Const.DB_SALAD {
		if db == nil {
			DNS := createDNS(conf, 0)
			var err error
			db, err = gorm.Open(sqlserver.Open(DNS), configGorm)
			if err != nil {
				logs.Error.Fatalf("Error connecting to database : %v", err)
			}
			sqlDB, _ := db.DB()

			// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
			sqlDB.SetMaxIdleConns(10)
			// SetMaxOpenConns sets the maximum number of open connections to the database.
			sqlDB.SetMaxOpenConns(100)
			// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
			sqlDB.SetConnMaxLifetime(time.Hour)

			return db
		}
		return db
	}
	if data == Const.EXTERNAL_DB {
		if db1 == nil {
			var err error
			logs.Debug.Println("Connecting to database 1 : ", conf.Database[0])
			logs.Debug.Println("Connecting to database 2 : ", conf.Database[1])
			DNS2 := createDNS(conf, 1)
			db1, err = gorm.Open(sqlserver.Open(DNS2), configGorm)
			if err != nil {
				logs.Error.Fatalf("Error connecting to database 2 : %v", err)
			}
			sqlDB, _ := db.DB()

			// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
			sqlDB.SetMaxIdleConns(10)
			// SetMaxOpenConns sets the maximum number of open connections to the database.
			sqlDB.SetMaxOpenConns(100)
			// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
			sqlDB.SetConnMaxLifetime(time.Hour)

			return db1
		}
		return db1
	}

	if data == Const.EXTERNAL_STK_ADJ {
		if db2 == nil {
			var err error
			logs.Debug.Println("Connecting to database 3 : ", conf.Database[2])
			DNS2 := createDNS(conf, 2)
			db2, err = gorm.Open(sqlserver.Open(DNS2), configGorm)
			if err != nil {
				logs.Error.Fatalf("Error connecting to database 2 : %v", err)
			}
			sqlDB, _ := db.DB()

			// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
			sqlDB.SetMaxIdleConns(10)
			// SetMaxOpenConns sets the maximum number of open connections to the database.
			sqlDB.SetMaxOpenConns(100)
			// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
			sqlDB.SetConnMaxLifetime(time.Hour)

			return db2
		}
		return db2
	}
	return db
}

func createDNS(conf app.Database, index int) string {
	query := url.Values{}
	query.Add("database", conf.Database[index])
	safeDNS := url.UserPassword(conf.Username[index], conf.Password[index]).String()
	return fmt.Sprintf("sqlserver://%s@%s:%d?%s",
		safeDNS, conf.Host[index], conf.Port[index], query.Encode(),
	)
}

func SeedData() {
	err := seeder.Seeder(db)
	if err != nil {
		panic("Error seeding data : " + err.Error())
	}
}
