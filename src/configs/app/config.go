package app

import (
	"errors"
	"fmt"
	"log"

	"github.com/Nerzal/gocloak/v7"
	"github.com/gin-gonic/gin"

	"github.com/spf13/viper"
)

var (
	config     *Config
	goCloakObj gocloak.GoCloak
)

type Config struct {
	App      App
	Database Database
	Google   Google
	Domain   Domain
	Proxy    Proxy
	Keycloak Keycloak
	CronJob  CronJob
	Email    Email
}

type CronJob struct {
	CronJobInterval     []string
	CronJobName         []string
	CronJobTableNames   []string
	CronJobTask         []string
	CronJobTargetColumn []string
}

type Keycloak struct {
	Realm        string
	GoCloak      string
	ClientId     string
	ClientSecret string
	GoCloakObj   gocloak.GoCloak
}

type Proxy struct {
	TrustedProxies []string
	TrustedDomain  []string
}

type Domain struct {
	DomainName         string
	DomainProtocol     string
	DomainPort         int
	DomainFrontendPath string
}

type App struct {
	Port         int
	Name         string
	Environment  string
	Locale       string
	Key          string
	Debug        bool
	MigrateKey   string
	UploadFolder string
	FERouting    bool
}

type Database struct {
	Host              []string
	Port              []int
	Database          []string
	Username          []string
	Password          []string
	Schema            []string
	ExternalTableName []string
	DatabaseSeed      bool
	DatabaseMigrate   bool
}

type Google struct {
}

type Email struct {
	Host     string
	Port     int
	Address  string
	Username string
	Password string
}

func loadConfig(environment string) (*viper.Viper, error) {
	v := viper.New()
	v.SetConfigName(fmt.Sprintf("configs/config-%s", environment))
	v.AddConfigPath(".")
	v.AutomaticEnv()
	if err := v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			return nil, errors.New("config file not found")
		}
		return nil, err
	}
	return v, nil
}

func TrustedProxies(servers *gin.Engine) *gin.Engine {
	err := servers.SetTrustedProxies(GetConfig().Proxy.TrustedProxies)
	if err != nil {
		panic("Error setting trusted proxies : " + err.Error())
	}
	return servers
}

func GetConfig() *Config {
	if config == nil {
		vp := viper.New()
		vp.BindEnv("APP_ENV")
		vp.SetDefault("APP_ENV", "development")
		v, err := loadConfig(vp.GetString("APP_ENV"))
		if err != nil {
			panic(err)
		}
		err = v.Unmarshal(&config)
		if err != nil {
			log.Printf("unable to decode into struct, %v", err)
			panic(err)
		}
	}
	if goCloakObj == nil {
		goCloakObj = gocloak.NewClient(config.Keycloak.GoCloak)
		config.Keycloak.GoCloakObj = goCloakObj
		if config.Keycloak.GoCloakObj == nil {
			panic("Error KeyCloak Go Cloak")
		}
	}
	return config
}
