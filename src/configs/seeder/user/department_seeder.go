package user

import (
	User "BE-Epson/domains/master/entities"
	"gorm.io/gorm"
)

func DepartmentSeeder(db *gorm.DB) error {
	divisions := []User.Department{
		{Model: gorm.Model{}, DepartmentName: "Requestor", LocationCodeFk: "D114"},
		{Model: gorm.Model{}, DepartmentName: "PPC", LocationCodeFk: "D100"},
		{Model: gorm.Model{}, DepartmentName: "WHC", LocationCodeFk: "D100"},
		{Model: gorm.Model{}, DepartmentName: "Destroy (WHC)", LocationCodeFk: "D100"},
		{Model: gorm.Model{}, DepartmentName: "Logistic", LocationCodeFk: "D100"},
		{Model: gorm.Model{}, DepartmentName: "IT", LocationCodeFk: "D100"},
		{Model: gorm.Model{}, DepartmentName: "Hrd", LocationCodeFk: "D100"},
		{Model: gorm.Model{}, DepartmentName: "Marketing", LocationCodeFk: "D100"},
		{Model: gorm.Model{}, DepartmentName: "Employee", LocationCodeFk: "D100"},
	}
	for _, division := range divisions {
		exists, err := divisionExists(db, division)
		if err != nil {
			return err
		}
		if !exists {
			if err := createDivision(db, &division); err != nil {
				return err
			}
		}
	}
	return nil
}
func divisionExists(db *gorm.DB, Department User.Department) (bool, error) {
	var count int64
	db.Model(&Department).Where("department_name = ?", Department.DepartmentName).Count(&count)
	return count > 0, nil
}
func createDivision(db *gorm.DB, Department *User.Department) error {
	if err := db.Create(&Department).Error; err != nil {
		return err
	}
	return nil
}
