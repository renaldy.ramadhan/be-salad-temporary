package staging

import (
	StagingPlant "BE-Epson/domains/staging/entities"
	"fmt"

	"gorm.io/gorm"
)

func StagingLocationSeeder(db *gorm.DB) error {

	stagingLocations := []StagingPlant.StagingPlant{

		{LocationName: "GB/Bonded Whs", PlantCode: "D101", LocationCode: "D100"},
		{LocationName: "Whs E4", PlantCode: "D101", LocationCode: "D114"},
		{LocationName: "Whs E2", PlantCode: "D101", LocationCode: "D116"},
		{LocationName: "Whs E3", PlantCode: "D101", LocationCode: "D120"},
		{LocationName: "WIP E2", PlantCode: "D101", LocationCode: "D1A1"},
		{LocationName: "PL WIP", PlantCode: "D101", LocationCode: "D1A2"},
		{LocationName: "PH WIP", PlantCode: "D101", LocationCode: "D1A3"},
		{LocationName: "WIP E4", PlantCode: "D101", LocationCode: "D1A4"},
		{LocationName: "WIP E3", PlantCode: "D101", LocationCode: "D1A7"},
		{LocationName: "Subassy WIP E3", PlantCode: "D101", LocationCode: "D1AC"},
		{LocationName: "Subassy WIP E4", PlantCode: "D101", LocationCode: "D1AD"},
		{LocationName: "Parts Export Whs", PlantCode: "D102", LocationCode: "D2S0"},
		{LocationName: "ASP Whs", PlantCode: "D103", LocationCode: "D3S0"},
	}
	for _, location := range stagingLocations {
		exists, err := locationExists(db, location)
		if err != nil {
			return fmt.Errorf("error checking master existence: %w", err)
		}
		if !exists {
			if err := createLocation(db, &location); err != nil {
				return fmt.Errorf("error creating master: %w", err)
			}
		}
	}
	return nil
}

func locationExists(db *gorm.DB, location StagingPlant.StagingPlant) (bool, error) {
	var count int64
	if err := db.Model(&StagingPlant.StagingPlant{}).Where("location_name = ? AND plant_code = ?", location.LocationName, location.PlantCode).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}
func createLocation(db *gorm.DB, location *StagingPlant.StagingPlant) error {
	if err := db.Create(&location).Error; err != nil {
		return err
	}
	return nil
}
