package staging

import (
	StagingReason "BE-Epson/domains/staging/entities"
	"errors"

	"gorm.io/gorm"
)

func StagingReasonSeeder(db *gorm.DB) error {
	stagingReasons := []StagingReason.StagingReason{
		{Model: gorm.Model{}, ReasonType: "Request", ReasonName: "JIG", Description: "JIG", MvtNegative: "", MvtPositive: "Z35"},
		{Model: gorm.Model{}, ReasonType: "Request", ReasonName: "ROHS", Description: "ROHS", MvtNegative: "", MvtPositive: "ZN7"},
		{Model: gorm.Model{}, ReasonType: "Request", ReasonName: "Evaluation", Description: "Evaluation", MvtNegative: "", MvtPositive: "Z27"},
		{Model: gorm.Model{}, ReasonType: "Request", ReasonName: "Consumable", Description: "Consumable", MvtNegative: "", MvtPositive: "ZY3"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "Recycle & Reused", Description: "Recycle & Reused", MvtNegative: "ZV7", MvtPositive: "ZV8"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "Screw Problem", Description: "Screw Problem", MvtNegative: "ZY8", MvtPositive: "ZY7"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "BOM Mistake", Description: "BOM Mistake", MvtNegative: "ZY8", MvtPositive: "ZY7"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "TEN Problem", Description: "TEN Problem", MvtNegative: "ZY8", MvtPositive: "ZY7"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "Result Problem ", Description: "Result Problem ", MvtNegative: "ZY8", MvtPositive: "ZY7"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "Rework ", Description: "Rework ", MvtNegative: "ZY8", MvtPositive: "ZY7"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "Routing Master Problem", Description: "Routing Master Problem", MvtNegative: "ZY8", MvtPositive: "ZY7"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "Material Master Problem ", Description: "Material Master Problem ", MvtNegative: "ZY8", MvtPositive: "ZY7"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "PO Expense Problem ", Description: "PO Expense Problem ", MvtNegative: "ZY8", MvtPositive: "ZY7"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "Prodder Problem ", Description: "Prodder Problem ", MvtNegative: "ZY8", MvtPositive: "ZY7"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "SPQ Problem ", Description: "SPQ Problem ", MvtNegative: "ZX5", MvtPositive: "ZX6"},
		{Model: gorm.Model{}, ReasonType: "Discrepancy", ReasonName: "Missing", Description: "Missing", MvtNegative: "ZX5", MvtPositive: "ZX6"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "Ten Instruction", Description: "Ten Instruction", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "Engineering Evaluation ", Description: "Engineering Evaluation ", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "Remain Part ES/PLS", Description: "Remain Part ES/PLS", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "NG Main Production (assembly)", Description: "NG Main Production (assembly)", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "NG PH Production (assembly)", Description: "NG PH Production (assembly)", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "NG PL Production (assembly)  ", Description: "NG PL Production (assembly)  ", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "NG Ribbon  Production (assembly)", Description: "NG Ribbon  Production (assembly)", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "VDS claim, NG Return VDS", Description: "VDS claim, NG Return VDS", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "Rework/Dismantle ", Description: "Rework/Dismantle ", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "ROHS Evaluation ", Description: "ROHS Evaluation ", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "NG Part Claim to vendor", Description: "NG Part Claim to vendor", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "OQC Evaluation", Description: "OQC Evaluation", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "NG Main Unit (FG)", Description: "NG Main Unit (FG)", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "Printer Unit Evaluation (FG)", Description: "Printer Unit Evaluation (FG)", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "Printer Unit OQC Evaluation (FG)", Description: "Printer Unit OQC Evaluation (FG)", MvtNegative: "", MvtPositive: "Z37"},
		{Model: gorm.Model{}, ReasonType: "Disposal", ReasonName: "NG Warehouse", Description: "NG Warehouse", MvtNegative: "", MvtPositive: "Z37"},
	}
	for _, stagingReason := range stagingReasons {
		exists, err := stagingReasonExists(db, stagingReason)
		if err != nil {
			return errors.New("error checking staging reason existence")
		}
		if !exists {
			if err := createStagingReason(db, &stagingReason); err != nil {
				return errors.New("error creating staging reason")
			}
		}
	}
	return nil
}

func stagingReasonExists(db *gorm.DB, stagingReason StagingReason.StagingReason) (bool, error) {
	var count int64
	if err := db.Model(&StagingReason.StagingReason{}).Where("reason_name = ?", stagingReason.ReasonName).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}

func createStagingReason(db *gorm.DB, stagingReason *StagingReason.StagingReason) error {
	if err := db.Create(&stagingReason).Error; err != nil {
		return err
	}
	return nil
}
