package staging

import (
	StagingPurchasingGroup "BE-Epson/domains/staging/entities"
	"errors"

	"gorm.io/gorm"
)

func StagingPurchasingGroupSeeder(db *gorm.DB) error {
	stagingPurchasingGroups := []StagingPurchasingGroup.StagingPurchasingGroup{
		{PurchasingGroup: "D101", PurchasingGroupCode: "D13", Manager: "Alaludin"},
		{PurchasingGroup: "D102", PurchasingGroupCode: "DA3", Manager: "Alaludin"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DM3", Manager: "Alaludin"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "D21", Manager: "Bagiyo"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "D31", Manager: "Bagiyo"},
		{PurchasingGroup: "D102", PurchasingGroupCode: "DA1", Manager: "Bagiyo"},
		{PurchasingGroup: "D102", PurchasingGroupCode: "DB1", Manager: "Bagiyo"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DM1", Manager: "Bagiyo"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DN1", Manager: "Bagiyo"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "D16", Manager: "Dedi Kusnanto"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "D18", Manager: "Dedi Kusnanto"},
		{PurchasingGroup: "D102", PurchasingGroupCode: "DA6", Manager: "Dedi Kusnanto"},
		{PurchasingGroup: "D102", PurchasingGroupCode: "DA8", Manager: "Dedi Kusnanto"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DM6", Manager: "Dedi Kusnanto"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DM8", Manager: "Dedi Kusnanto"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "D41", Manager: "Kondo"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "001", Manager: "Kondo"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "D17", Manager: "Nino Hartanto"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "D19", Manager: "Nino Hartanto"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "D15", Manager: "Nino Hartanto"},
		{PurchasingGroup: "D102", PurchasingGroupCode: "DA7", Manager: "Nino Hartanto"},
		{PurchasingGroup: "D102", PurchasingGroupCode: "DA9", Manager: "Nino Hartanto"},
		{PurchasingGroup: "D102", PurchasingGroupCode: "DA5", Manager: "Nino Hartanto"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DM7", Manager: "Nino Hartanto"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DM9", Manager: "Nino Hartanto"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DM5", Manager: "Nino Hartanto"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "D12", Manager: "Rizky"},
		{PurchasingGroup: "D101", PurchasingGroupCode: "D14", Manager: "Rizky"},
		{PurchasingGroup: "D102", PurchasingGroupCode: "DA2", Manager: "Rizky"},
		{PurchasingGroup: "D102", PurchasingGroupCode: "DA4", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DM4", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DM2", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "D11", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "D1A", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "D25", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DH0", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "D10", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "D20", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "DAC", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "D1C", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "D51", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "D40", Manager: "Rizky"},
		{PurchasingGroup: "D103", PurchasingGroupCode: "D1D", Manager: "Rizky"},
	}
	for _, stagingPurchasingGroup := range stagingPurchasingGroups {
		exists, err := stagingPurchasingGroupExists(db, stagingPurchasingGroup)
		if err != nil {
			return errors.New("error checking staging purchasing group existence")
		}
		if !exists {
			if err := createStagingPurchasingGroup(db, &stagingPurchasingGroup); err != nil {
				return errors.New("error creating staging purchasing group")
			}
		}
	}
	return nil
}

func stagingPurchasingGroupExists(db *gorm.DB, stagingPurchasingGroup StagingPurchasingGroup.StagingPurchasingGroup) (bool, error) {
	var count int64
	if err := db.Model(&StagingPurchasingGroup.StagingPurchasingGroup{}).Where("purchasing_group_code = ?", stagingPurchasingGroup.PurchasingGroupCode).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}

func createStagingPurchasingGroup(db *gorm.DB, stagingPurchasingGroup *StagingPurchasingGroup.StagingPurchasingGroup) error {
	if err := db.Create(&stagingPurchasingGroup).Error; err != nil {
		return err
	}
	return nil
}
