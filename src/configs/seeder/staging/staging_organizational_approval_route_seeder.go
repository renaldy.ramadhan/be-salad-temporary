package staging

import (
	OrganizationApprovalRoute "BE-Epson/domains/staging/entities"
	"fmt"

	"gorm.io/gorm"
)

func StagingOrganizationalApprovalRouteSeeder(db *gorm.DB) error {

	stagingApproval := []OrganizationApprovalRoute.OrganizationApprovalRoute{

		{Nik: "10903794", LineCode: "08AY0101", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "TORIQUL MAMAFIK"},
		{Nik: "16030005", LineCode: "08AY0101", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ISTIHARI LAKSMI NOERAISHA"},
		{Nik: "9727367", LineCode: "08AY0101", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "EVI ZUFIANTI"},
		{Nik: "11000178", LineCode: "08AY0101", WfLevel: 0, WfName: "CHIEF STAFF", WfSeq: 0, Name: "IWAN MARTIN"},
		{Nik: "16044042", LineCode: "08AY0101", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "LARAS SATI"},
		{Nik: "19070019", LineCode: "08AY0101", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "DEDE SOPIAN"},
		{Nik: "18030023", LineCode: "08AY0101", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "MUHAMMAD WAHYU SATYA PRAJA"},
		{Nik: "11000179", LineCode: "08AY0101", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "SYAMSUL MUIN"},
		{Nik: "9797655", LineCode: "08AY0101", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ZILDA ADRIANIS"},
		{Nik: "14100006", LineCode: "08AY0101", WfLevel: 0, WfName: "CHIEF STAFF", WfSeq: 0, Name: "ADE KURNIAWAN"},
		{Nik: "15030020", LineCode: "08AY0101", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "DECKY DAMARA"},
		{Nik: "22110002", LineCode: "08AY0101", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ANANDA FIKRI NUGROHO"},
		{Nik: "22030001", LineCode: "08AY0101", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "ITANO YUSUKE"},
		{Nik: "22030001", LineCode: "08AY0101", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "ITANO YUSUKE"},
		{Nik: "10000126", LineCode: "08AY0101", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "11000119", LineCode: "08AY0103", WfLevel: 0, WfName: "CHIEF STAFF", WfSeq: 0, Name: "AGUS WIBOWO"},
		{Nik: "10804187", LineCode: "08AY0103", WfLevel: 0, WfName: "CHIEF STAFF", WfSeq: 0, Name: "ALI FIKRI"},
		{Nik: "23070011", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "MUHAMMAD SHOLAHUDDIN AL AYYUBI"},
		{Nik: "19060008", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "FADHIL HAWARI"},
		{Nik: "21030005", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "REGIF SATYA PRASAJA"},
		{Nik: "20071020", LineCode: "08AY0103", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "KARINA SALSABILA"},
		{Nik: "21110010", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "FENDY YUDAH FIRDIAN"},
		{Nik: "9727032", LineCode: "08AY0103", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "ISMAT SANIA"},
		{Nik: "14110005", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "YANUAR LINGGAR PRASTAWA U"},
		{Nik: "20065767", LineCode: "08AY0103", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "VICKY ATTALA IQBAL"},
		{Nik: "9898066", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "DEWI NOVITA"},
		{Nik: "13070001", LineCode: "08AY0103", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "LIDOV PRASTOWO"},
		{Nik: "15100078", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "FADHIL ABDULLAH"},
		{Nik: "15110034", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "YUSMANSYAH"},
		{Nik: "15120003", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "SUGANDA"},
		{Nik: "18100003", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "NOPAL ANDRIYANTO"},
		{Nik: "15050017", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "M. ARI WIBOWO"},
		{Nik: "16090023", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "AHMAD IRFAN SATRIAWAN"},
		{Nik: "20055122", LineCode: "08AY0103", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "SARIFUDIN"},
		{Nik: "9696267", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ANI KHANIFAH"},
		{Nik: "10000155", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "DARMANATA"},
		{Nik: "10000183", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ALIYAH"},
		{Nik: "19105239", LineCode: "08AY0103", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "SADELIH"},
		{Nik: "10500006", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "RIZKO FERY"},
		{Nik: "15050010", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ALHDILA RIFQI BELLY"},
		{Nik: "9717098", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "MUTIK AFIFAH"},
		{Nik: "20065178", LineCode: "08AY0103", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "YAHYA SUBARKAH"},
		{Nik: "19100021", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "RIZKY ESA PRATAMA"},
		{Nik: "19060014", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "RIZKY EKA FIRDAUSI"},
		{Nik: "11001757", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "RICKY YUSMANTA PUTRA"},
		{Nik: "17110011", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "AGUNG PRASETIO WIBOWO"},
		{Nik: "10804807", LineCode: "08AY0103", WfLevel: 0, WfName: "CHIEF STAFF", WfSeq: 0, Name: "YUSIFA USMAN"},
		{Nik: "16045036", LineCode: "08AY0103", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "IBNU HIBBAN"},
		{Nik: "10706763", LineCode: "08AY0103", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ROMZI RAHMAN"},
		{Nik: "10200703", LineCode: "08AY0103", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "DENI SAEPUDIN"},
		{Nik: "10603150", LineCode: "08AY0103", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "PRIMA PUJA SAMAPTO"},
		{Nik: "10603150", LineCode: "08AY0103", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "PRIMA PUJA SAMAPTO"},
		{Nik: "10000126", LineCode: "08AY0103", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "9696268", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "DELYASTI HOPITRISNA P"},
		{Nik: "9727407", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "TRES DEWI KURNIASIH"},
		{Nik: "15040014", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "RISMANA"},
		{Nik: "10101789", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "LIA JUHLIA"},
		{Nik: "14100004", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "TEGAR ADHITYA MUKTI ALI"},
		{Nik: "16040003", LineCode: "08AY0104", WfLevel: 0, WfName: "CHIEF STAFF", WfSeq: 0, Name: "ARIF SUWANDHI"},
		{Nik: "19070013", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "FENY YULIANA ANDRIANI"},
		{Nik: "15030025", LineCode: "08AY0104", WfLevel: 0, WfName: "CHIEF STAFF", WfSeq: 0, Name: "SIGIT ARIYANTO"},
		{Nik: "10000278", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "MARIYAH"},
		{Nik: "22120011", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "FARHAN MUHADZDZIB"},
		{Nik: "23040003", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "BRIAN ARDIANATA"},
		{Nik: "19100018", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "M. KHOIRUL KHULUK"},
		{Nik: "9595277", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "MIKA ROTUA"},
		{Nik: "11001860", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "MISIRAN"},
		{Nik: "21100035", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "DEWI AYU NINGRUM"},
		{Nik: "15051018", LineCode: "08AY0104", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "FARIDIANA FATMAWATI"},
		{Nik: "10603152", LineCode: "08AY0104", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "EKA KRISWIDIANTORO"},
		{Nik: "10603152", LineCode: "08AY0104", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "EKA KRISWIDIANTORO"},
		{Nik: "10000126", LineCode: "08AY0104", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "16030011", LineCode: "08AY0105", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "PUTRI WAHYUNING JATI"},
		{Nik: "19090013", LineCode: "08AY0105", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "NAFI'AN ZUKHRUFAL MUTTAQI"},
		{Nik: "19070012", LineCode: "08AY0105", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ARNITA RAHMI"},
		{Nik: "10800028", LineCode: "08AY0105", WfLevel: 0, WfName: "CHIEF STAFF", WfSeq: 0, Name: "MARSONO"},
		{Nik: "23070009", LineCode: "08AY0105", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "FRANSISKUS BIMA WIDI NUGROHO"},
		{Nik: "19100012", LineCode: "08AY0105", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ANHAR TRIATMANTO"},
		{Nik: "19080016", LineCode: "08AY0105", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "HABY PURWA ANUGRAH"},
		{Nik: "16040010", LineCode: "08AY0105", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "INDRA WAHYU BUDIANTO"},
		{Nik: "15100088", LineCode: "08AY0105", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "BILLY ABBIE OSMOND"},
		{Nik: "10902417", LineCode: "08AY0105", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "DODY SUJOKO APRIAN AZMI"},
		{Nik: "10903125", LineCode: "08AY0105", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "DESY MAULIDIYANTI AMIN"},
		{Nik: "10706712", LineCode: "08AY0105", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "AGUNG WIBOWO"},
		{Nik: "10706712", LineCode: "08AY0105", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "AGUNG WIBOWO"},
		{Nik: "10000126", LineCode: "08AY0105", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "10800535", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "TRIANI AGUSTIN"},
		{Nik: "23010015", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "KEVIN HANDITYA PRADANA"},
		{Nik: "9727079", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "SITI NUR KHASANAH"},
		{Nik: "16100005", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "HUMAAM DZULHILMI"},
		{Nik: "19090012", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "DHELA ARDIANSYAH KURNIA ROSIDY"},
		{Nik: "20090005", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "KHAIRUNNISSA NUR FAJRIN"},
		{Nik: "22100005", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "TAUFIQUR ROHMAN"},
		{Nik: "9717002", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "YANIMISTIYANI"},
		{Nik: "23040006", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "MUHAMMAD NIZAM CHOIRUR RIZAK"},
		{Nik: "14100002", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "MUHAMMAD RIDHO WIDI SANJAYA"},
		{Nik: "10000188", LineCode: "08AY0106", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ANJAR YULIYANTI WIDYASTUTI"},
		{Nik: "10804925", LineCode: "08AY0106", WfLevel: 0, WfName: "CHIEF STAFF", WfSeq: 0, Name: "AKHMAD AGUS KURNIAWAN"},
		{Nik: "10804199", LineCode: "08AY0106", WfLevel: 0, WfName: "CHIEF STAFF", WfSeq: 0, Name: "BOBY SETIAWAN"},
		{Nik: "10800542", LineCode: "08AY0106", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "WAHYUDI RUSDIANTO"},
		{Nik: "10800542", LineCode: "08AY0106", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "WAHYUDI RUSDIANTO"},
		{Nik: "10000126", LineCode: "08AY0106", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "10800547", LineCode: "08AY0107", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "TEGUH WIDODO"},
		{Nik: "9595113", LineCode: "08AY0107", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "TUTUN CARYA ILMAYUDANDA"},
		{Nik: "10000126", LineCode: "08AY0107", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "17090014", LineCode: "08AY010701", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "MIRA EKA ANNISA"},
		{Nik: "9797491", LineCode: "08AY010701", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "SUPRIHATIN"},
		{Nik: "19050007", LineCode: "08AY010701", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "APRILLYA SUJARWATI PUTRI"},
		{Nik: "9797311", LineCode: "08AY010701", WfLevel: 0, WfName: "LEADER", WfSeq: 0, Name: "EKO SOETRISNO"},
		{Nik: "19020003", LineCode: "08AY010701", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "DESSY NURHAYATI"},
		{Nik: "9898921", LineCode: "08AY010701", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "SULASTRI"},
		{Nik: "19010008", LineCode: "08AY010701", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "HANA MARETA RACHMAWATI"},
		{Nik: "10800547", LineCode: "08AY010701", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "TEGUH WIDODO"},
		{Nik: "9595113", LineCode: "08AY010701", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "TUTUN CARYA ILMAYUDANDA"},
		{Nik: "10000126", LineCode: "08AY010701", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "21101026", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "NANDAR SUPRIATNA"},
		{Nik: "20010011", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "ANGGA SUMARDALIN"},
		{Nik: "21100020", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "FEBRI BUDI SANTOSO"},
		{Nik: "22085556", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "GILANG RAMADHAN DWI KURNIANTORO"},
		{Nik: "20010008", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "AKHMAD FATKHURROHMAN"},
		{Nik: "9696165", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "FAJAR NUGROHO"},
		{Nik: "21125149", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "MUHAMAD ILHAM"},
		{Nik: "22085553", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "ACHMAD FAUZAN DARMAWAN"},
		{Nik: "9898225", LineCode: "08AY010702", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "SARMAN"},
		{Nik: "21100019", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "EGO ADI NURJOYO"},
		{Nik: "21091001", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "AHMAD FAUZI WIARTO"},
		{Nik: "22115174", LineCode: "08AY010702", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "MUHAMMAD FARABI ANANDITO"},
		{Nik: "9595569", LineCode: "08AY010702", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "SALMAN"},
		{Nik: "9595113", LineCode: "08AY010702", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "TUTUN CARYA ILMAYUDANDA"},
		{Nik: "10000126", LineCode: "08AY010702", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "19083001", LineCode: "08AY010703", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "ADHITIA HERMANSYAH"},
		{Nik: "9999944", LineCode: "08AY010703", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "RISTENDI"},
		{Nik: "17010020", LineCode: "08AY010703", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "AHMAD APRIANTO"},
		{Nik: "19040011", LineCode: "08AY010703", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "IMAT SUTARMAN"},
		{Nik: "10100072", LineCode: "08AY010703", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "DWI LESTARI"},
		{Nik: "17010016", LineCode: "08AY010703", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "ZAENAL RASYID SHIDIQ"},
		{Nik: "9999630", LineCode: "08AY010703", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "BENNY YULYANTO"},
		{Nik: "9595113", LineCode: "08AY010703", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "TUTUN CARYA ILMAYUDANDA"},
		{Nik: "10000126", LineCode: "08AY010703", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "20010018", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "ESA FARUQI DZIKRIAZ"},
		{Nik: "15050007", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "RIDHA HERDIYANA"},
		{Nik: "9696254", LineCode: "08AY010704", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "ARIEL ANDRIANUR"},
		{Nik: "9727011", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "CUNCUN JOHARI"},
		{Nik: "22085558", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "MUHAMAD NANANG WAHYU HARTA WIDODO"},
		{Nik: "21125136", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "HANUM YUDHA TRIATMOJO"},
		{Nik: "9999054", LineCode: "08AY010704", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "JUMADI"},
		{Nik: "21125155", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "NGUDI SUTRISNO"},
		{Nik: "21095022", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "MOHAMAD RIDWAN"},
		{Nik: "9696104", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "ASEP ALI HARTONO"},
		{Nik: "14025297", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "SYAHRUDDIN"},
		{Nik: "13085039", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "IRMANSYAH"},
		{Nik: "9797879", LineCode: "08AY010704", WfLevel: 0, WfName: "LEADER", WfSeq: 0, Name: "MOCH.TAUFIK HIDAYAT"},
		{Nik: "22085555", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "FALDY APRI ANDOYO"},
		{Nik: "22035799", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "MUHAMAD HAMAS AL FAROBBY"},
		{Nik: "9999075", LineCode: "08AY010704", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "IRFAN HIDAYAT"},
		{Nik: "21125133", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "FARIS MUHAMMAD RAFI GUSRI"},
		{Nik: "22085554", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "EKA IMAM YULYANTO"},
		{Nik: "14123026", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "RECO HARLAIN"},
		{Nik: "9898656", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "SLAMET NURJAMAN"},
		{Nik: "21091015", LineCode: "08AY010704", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "MUHAMMAD RIZKI ALFANSYAH"},
		{Nik: "9999630", LineCode: "08AY010704", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "BENNY YULYANTO"},
		{Nik: "9595113", LineCode: "08AY010704", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "TUTUN CARYA ILMAYUDANDA"},
		{Nik: "10000126", LineCode: "08AY010704", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "11004750", LineCode: "08AY010705", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "USI SUSANTI"},
		{Nik: "9999889", LineCode: "08AY010705", WfLevel: 0, WfName: "LEADER", WfSeq: 0, Name: "INDRA RUDI BILLAH"},
		{Nik: "13073009", LineCode: "08AY010705", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "EKA MELLY AFIATI"},
		{Nik: "9595584", LineCode: "08AY010705", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "NARIYAH"},
		{Nik: "9898297", LineCode: "08AY010705", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "MUSBAKIR"},
		{Nik: "22095329", LineCode: "08AY010705", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "ASHRI OKTAVIANI"},
		{Nik: "22111187", LineCode: "08AY010705", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "REFALDI"},
		{Nik: "11117035", LineCode: "08AY010705", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "SINGGIH HARI WIBOWO"},
		{Nik: "22111186", LineCode: "08AY010705", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "AGUNG BUDI PAMUJI"},
		{Nik: "9898144", LineCode: "08AY010705", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "GIYATNO"},
		{Nik: "9595113", LineCode: "08AY010705", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "TUTUN CARYA ILMAYUDANDA"},
		{Nik: "10000126", LineCode: "08AY010705", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
		{Nik: "10000169", LineCode: "08AY010706", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "HERI RIYANTARA"},
		{Nik: "21095021", LineCode: "08AY010706", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "MOCHAMAD RAFLI NUR IRHAMSYAH"},
		{Nik: "21070026", LineCode: "08AY010706", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "SUTARYA"},
		{Nik: "9899449", LineCode: "08AY010706", WfLevel: 0, WfName: "STAFF", WfSeq: 0, Name: "SYARIFUDDIN"},
		{Nik: "22085557", LineCode: "08AY010706", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "M. PUTRA PANATAGAMA"},
		{Nik: "15035041", LineCode: "08AY010706", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "AMAN"},
		{Nik: "14025224", LineCode: "08AY010706", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "ARIF RIZAL FANANI"},
		{Nik: "12000199", LineCode: "08AY010706", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "YONES HABENGETAN"},
		{Nik: "22035773", LineCode: "08AY010706", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "JAMAL SETIYADI"},
		{Nik: "12020110", LineCode: "08AY010706", WfLevel: 0, WfName: "OPERATOR", WfSeq: 0, Name: "ASEP SAEPUDIN"},
		{Nik: "9595569", LineCode: "08AY010706", WfLevel: 1, WfName: "SUPERVISOR", WfSeq: 0, Name: "SALMAN"},
		{Nik: "9595113", LineCode: "08AY010706", WfLevel: 2, WfName: "MANAGER", WfSeq: 0, Name: "TUTUN CARYA ILMAYUDANDA"},
		{Nik: "10000126", LineCode: "08AY010706", WfLevel: 3, WfName: "GM", WfSeq: 0, Name: "BENING ADI JAYA"},
	}
	for _, approval := range stagingApproval {
		exists, err := approvalExists(db, approval)
		if err != nil {
			return fmt.Errorf("error checking master existence: %w", err)
		}
		if !exists {
			if err := createApproval(db, &approval); err != nil {
				return fmt.Errorf("error creating master: %w", err)
			}
		}
	}
	return nil
}

func approvalExists(db *gorm.DB, approval OrganizationApprovalRoute.OrganizationApprovalRoute) (bool, error) {
	var count int64
	if err := db.Model(&OrganizationApprovalRoute.OrganizationApprovalRoute{}).Where("nik = ?", approval.Nik).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}
func createApproval(db *gorm.DB, approval *OrganizationApprovalRoute.OrganizationApprovalRoute) error {
	if err := db.Create(&approval).Error; err != nil {
		return err
	}
	return nil
}
