package part

import (
	ApproverPivot "BE-Epson/domains/master/entities"

	"gorm.io/gorm"
)

func ApproverPivotSeeder(db *gorm.DB) error {

	approver := []ApproverPivot.ApproverPivot{
		{
			ID:              1,
			IdApproverGroup: 5, // karna DB
			IdApprover:      1,
		},
		{
			ID:              2,
			IdApproverGroup: 5, // karna DB
			IdApprover:      2,
		},
		{
			ID:              3,
			IdApproverGroup: 6, // karna DB
			IdApprover:      3,
		},
	}
	for _, approver := range approver {
		exists, err := approverPivotExists(db, approver)
		if err != nil {
			return err
		}
		if !exists {
			if err := createPivotApprover(db, &approver); err != nil {
				return err
			}
		}
	}
	return nil
}

func approverPivotExists(db *gorm.DB, approver ApproverPivot.ApproverPivot) (bool, error) {
	var count int64
	if err := db.Model(&ApproverPivot.ApproverPivot{}).Where("id = ?", approver.ID).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}

func createPivotApprover(db *gorm.DB, approver *ApproverPivot.ApproverPivot) error {
	if err := db.Create(&approver).Error; err != nil {
		return err
	}
	return nil
}
