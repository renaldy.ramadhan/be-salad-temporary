package part

import (
	Approver "BE-Epson/domains/master/entities"
	"BE-Epson/shared/helpers"

	"gorm.io/gorm"
)

func ApproverSeeder(db *gorm.DB) error {
	approvalStage := helpers.ParseToUint8(1)
	approver := []Approver.Approver{
		{
			ID:            1,
			Nik:           "9595569", // NIK
			Sequence:      1,
			DateApproval:  nil,
			EmployeeName:  "Rizky",
			ApprovalStage: &approvalStage,
		},
		{
			ID:            2,
			Nik:           "9595113", // NIK
			Sequence:      2,
			DateApproval:  nil,
			EmployeeName:  "Itano Yosuke 2",
			ApprovalStage: &approvalStage,
		},
		{
			ID:            3,
			Nik:           "9999630", // NIK
			Sequence:      1,
			DateApproval:  nil,
			EmployeeName:  "Teguh",
			ApprovalStage: &approvalStage,
		},
		{
			ID:            3,
			Nik:           "9595113", // NIK
			Sequence:      2,
			DateApproval:  nil,
			EmployeeName:  "Cecep",
			ApprovalStage: &approvalStage,
		},
	}
	for _, approver := range approver {
		exists, err := approverExists(db, approver)
		if err != nil {
			return err
		}
		if !exists {
			if err := createApprover(db, &approver); err != nil {
				return err
			}
		}
	}
	return nil
}

func approverExists(db *gorm.DB, approver Approver.Approver) (bool, error) {
	var count int64
	if err := db.Model(&Approver.Approver{}).Where("id = ?", approver.ID).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}

func createApprover(db *gorm.DB, approver *Approver.Approver) error {
	if err := db.Create(&approver).Error; err != nil {
		return err
	}
	return nil
}
