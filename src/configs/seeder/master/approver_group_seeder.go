package part

import (
	ApproverGroup "BE-Epson/domains/master/entities"
	StagingPurchasingGroup "BE-Epson/domains/staging/entities"
	"gorm.io/gorm"
)

func ApproverGroupSeeder(db *gorm.DB) error {
	purchasingGroupCode := "D13"
	approvalgroup := []ApproverGroup.ApproverGroup{
		{
			Model:                  gorm.Model{ID: 5},
			IdDepartment:           2, // Request
			PurchasingGroupCodeFk:  &purchasingGroupCode,
			ApproverGroupName:      "PPC Approval Group",
			GroupType:              "Request",
			Department:             ApproverGroup.Department{},
			StagingPurchasingGroup: StagingPurchasingGroup.StagingPurchasingGroup{},
		},
		{
			Model:                  gorm.Model{ID: 6},
			IdDepartment:           3, // Request
			PurchasingGroupCodeFk:  &purchasingGroupCode,
			ApproverGroupName:      "WHC",
			GroupType:              "Request",
			Department:             ApproverGroup.Department{},
			StagingPurchasingGroup: StagingPurchasingGroup.StagingPurchasingGroup{},
		},
	}
	for _, appprovalGroup := range approvalgroup {
		exists, err := approverGroupExists(db, appprovalGroup)
		if err != nil {
			return err
		}
		if !exists {
			if err := createApproverGroup(db, &appprovalGroup); err != nil {
				return err
			}
		}
	}
	return nil
}

func approverGroupExists(db *gorm.DB, appprovalGroup ApproverGroup.ApproverGroup) (bool, error) {
	var count int64
	if err := db.Model(&ApproverGroup.ApproverGroup{}).Where("approver_group_name = ?", appprovalGroup.ApproverGroupName).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}

func createApproverGroup(db *gorm.DB, appprovalGroup *ApproverGroup.ApproverGroup) error {
	if err := db.Create(&appprovalGroup).Error; err != nil {
		return err
	}
	return nil
}
