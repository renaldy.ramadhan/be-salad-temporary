package part

import (
	"BE-Epson/domains/master/entities"
	"gorm.io/gorm"
)

func FlowStepSeeder(db *gorm.DB) error {
	flowSteps := []entities.FlowStep{
		{
			Model:        gorm.Model{},
			IdWorkflow:   1, // Request
			IdDepartment: &[]uint{1}[0],
			FlowSequence: 1,
			StepName:     "Requestor",
			Type:         1,
			Department:   entities.Department{},
			Workflow:     entities.Workflow{},
		},
		{
			Model:        gorm.Model{},
			IdWorkflow:   1, // Request
			IdDepartment: &[]uint{2}[0],
			FlowSequence: 2,
			StepName:     "PPC",
			Type:         3,
			Department:   entities.Department{},
			Workflow:     entities.Workflow{},
		},
		{
			Model:        gorm.Model{},
			IdWorkflow:   1, // Request
			IdDepartment: &[]uint{3}[0],
			FlowSequence: 3,
			StepName:     "WHC",
			Type:         2,
			Department:   entities.Department{},
			Workflow:     entities.Workflow{},
		},
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   3, // Disposal
		// 	IdDepartment: nil,
		// 	FlowSequence: 1,
		// 	StepName:     "Destroy (WHC)",
		// 	Type:         2,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   3, // Disposal
		// 	IdDepartment: nil,
		// 	FlowSequence: 2,
		// 	StepName:     "Requestor",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   3, // Disposal
		// 	IdDepartment: nil,
		// 	FlowSequence: 3,
		// 	StepName:     "GNS",
		// 	Type:         2,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   3, // Disposal
		// 	IdDepartment: nil,
		// 	FlowSequence: 4,
		// 	StepName:     "GA",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   3, // Disposal
		// 	IdDepartment: nil,
		// 	FlowSequence: 5,
		// 	StepName:     "Accounting",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   3, // Disposal
		// 	IdDepartment: nil,
		// 	FlowSequence: 6,
		// 	StepName:     "BC 25",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   2,
		// 	IdDepartment: nil,
		// 	FlowSequence: 1,
		// 	StepName:     "Requestor",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   2,
		// 	IdDepartment: nil,
		// 	FlowSequence: 2,
		// 	StepName:     "GNS",
		// 	Type:         2,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   4,
		// 	IdDepartment: nil,
		// 	FlowSequence: 1,
		// 	StepName:     "Purchasing",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   4,
		// 	IdDepartment: nil,
		// 	FlowSequence: 2,
		// 	StepName:     "GNS",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   4,
		// 	IdDepartment: nil,
		// 	FlowSequence: 3,
		// 	StepName:     "Related Dept",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   4,
		// 	IdDepartment: nil,
		// 	FlowSequence: 4,
		// 	StepName:     "GA",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   4,
		// 	IdDepartment: nil,
		// 	FlowSequence: 5,
		// 	StepName:     "Accounting",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
		// {
		// 	Model:        gorm.Model{},
		// 	IdWorkflow:   4,
		// 	IdDepartment: nil,
		// 	FlowSequence: 6,
		// 	StepName:     "BC 25",
		// 	Type:         1,
		// 	Department:   Department.Department{},
		// 	Workflow:     FlowStep.Workflow{},
		// },
	}
	for _, flowStep := range flowSteps {
		exists, err := flowStepExists(db, flowStep)
		if err != nil {
			return err
		}
		if !exists {
			if err := createFlowStep(db, &flowStep); err != nil {
				return err
			}
		}
	}
	return nil
}

func flowStepExists(db *gorm.DB, flowStep entities.FlowStep) (bool, error) {
	var count int64
	if err := db.Model(&entities.FlowStep{}).Where("step_name = ?", flowStep.StepName).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}

func createFlowStep(db *gorm.DB, flowStep *entities.FlowStep) error {
	if err := db.Create(&flowStep).Error; err != nil {
		return err
	}
	return nil
}
