package part

import (
	Sequence "BE-Epson/domains/part/entities"
	"errors"
	"gorm.io/gorm"
	"time"
)

func SequenceSeeder(db *gorm.DB) error {
	sequences := []Sequence.Sequence{
		{
			Model:         gorm.Model{},
			SequenceLevel: 1,
			Type:          "Request",
			NextResetDate: time.Time{},
		},
		{
			Model:         gorm.Model{},
			SequenceLevel: 1,
			Type:          "Ringi",
			NextResetDate: time.Time{},
		},
		{
			Model:         gorm.Model{},
			SequenceLevel: 1,
			Type:          "Disposal",
			NextResetDate: time.Time{},
		},
		{
			Model:         gorm.Model{},
			SequenceLevel: 1,
			Type:          "Discrepancy",
			NextResetDate: time.Time{},
		},
	}
	for _, sequence := range sequences {
		exists, err := sequenceExists(db, sequence)
		if err != nil {
			return errors.New("error checking sequence existence")
		}
		if !exists {
			if err := createSequence(db, &sequence); err != nil {
				return errors.New("error creating sequence")
			}
		}
	}
	return nil
}

func sequenceExists(db *gorm.DB, sequence Sequence.Sequence) (bool, error) {
	var count int64
	if err := db.Model(&Sequence.Sequence{}).Where("type = ?", sequence.Type).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}

func createSequence(db *gorm.DB, sequence *Sequence.Sequence) error {
	if err := db.Create(&sequence).Error; err != nil {
		return err
	}
	return nil
}
