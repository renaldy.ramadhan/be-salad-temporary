package part

import (
	FlowStep "BE-Epson/domains/master/entities"
	"gorm.io/gorm"
)

func WorkFlowSeeder(db *gorm.DB) error {
	workFlows := []FlowStep.Workflow{
		{
			Model: gorm.Model{},
			Type:  "Request",
			Notes: "Part Request",
		},
		{
			Model: gorm.Model{},
			Type:  "Discrepancy",
			Notes: "Discrepancy 1",
		},
		{
			Model: gorm.Model{},
			Type:  "Disposal",
			Notes: "Disposal 1",
		},
		{
			Model: gorm.Model{},
			Type:  "Ringi",
			Notes: "Ringi",
		},
	}
	for _, workFlow := range workFlows {
		exists, err := workFlowExists(db, workFlow)
		if err != nil {
			return err
		}
		if !exists {
			if err := createWorkFlow(db, &workFlow); err != nil {
				return err
			}
		}
	}
	return nil
}

func workFlowExists(db *gorm.DB, workFlow FlowStep.Workflow) (bool, error) {
	var count int64
	if err := db.Model(&FlowStep.Workflow{}).Where("type = ?", workFlow.Type).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}

func createWorkFlow(db *gorm.DB, workFlow *FlowStep.Workflow) error {
	if err := db.Create(&workFlow).Error; err != nil {
		return err
	}
	return nil
}
