package part

import (
	Request "BE-Epson/domains/part/entities"
	"errors"
	"time"

	"gorm.io/gorm"
)

func RequestSeeder(db *gorm.DB) error {
	requests := []Request.Request{
		{
			Model:                 gorm.Model{ID: 19},
			UserName:              "Angga",
			IdReason:              1,
			PurchasingGroupCodeFk: "D101",
			//IdParent:              nil,
			Type:            "Request",
			RequestDate:     time.Now(),
			DueDate:         time.Now().Add(time.Hour * 24 * 7),
			PlantName:       "Jakarta",
			RequestLocation: "D101",
			DestroyLocation: "D101",
			Status:          "In Progress",
			Notes:           "Apa Aja Bole",
			DisposalType:    "Non - System",
			Parent:          nil,
		},
		{
			Model:                 gorm.Model{ID: 20},
			UserName:              "Rendy",
			IdReason:              1,
			PurchasingGroupCodeFk: "D101",
			//IdParent:              nil,
			Type:            "Request",
			RequestDate:     time.Now(),
			DueDate:         time.Now().Add(time.Hour * 24 * 5),
			PlantName:       "Bandung",
			RequestLocation: "D101",
			DestroyLocation: "D101",
			Status:          "In Progress",
			Notes:           "Apa Aja Bole",
			DisposalType:    "System",
			Parent:          nil,
		},
	}
	for _, request := range requests {
		exists, err := requestExists(db, request)
		if err != nil {
			return errors.New("error checking request existence")
		}
		if !exists {
			if err := createRequest(db, &request); err != nil {
				return errors.New("error creating request")
			}
		}
	}
	return nil
}

func requestExists(db *gorm.DB, request Request.Request) (bool, error) {
	var count int64
	if err := db.Model(&Request.Request{}).Where("id = ?", request.ID).Count(&count).Error; err != nil {
		return false, err
	}
	return count > 0, nil
}

func createRequest(db *gorm.DB, request *Request.Request) error {
	if err := db.Create(&request).Error; err != nil {
		return err
	}
	return nil
}
